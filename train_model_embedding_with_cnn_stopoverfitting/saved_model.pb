��
�&�%
D
AddV2
x"T
y"T
z"T"
Ttype:
2	��
^
AssignVariableOp
resource
value"dtype"
dtypetype"
validate_shapebool( �
~
BiasAdd

value"T	
bias"T
output"T" 
Ttype:
2	"-
data_formatstringNHWC:
NHWCNCHW
K
Bincount
arr
size
weights"T	
bins"T"
Ttype:
2	
N
Cast	
x"SrcT	
y"DstT"
SrcTtype"
DstTtype"
Truncatebool( 
h
ConcatV2
values"T*N
axis"Tidx
output"T"
Nint(0"	
Ttype"
Tidxtype0:
2	
8
Const
output"dtype"
valuetensor"
dtypetype
�
Conv2D

input"T
filter"T
output"T"
Ttype:	
2"
strides	list(int)"
use_cudnn_on_gpubool(",
paddingstring:
SAMEVALIDEXPLICIT""
explicit_paddings	list(int)
 "-
data_formatstringNHWC:
NHWCNCHW" 
	dilations	list(int)

�
Cumsum
x"T
axis"Tidx
out"T"
	exclusivebool( "
reversebool( " 
Ttype:
2	"
Tidxtype0:
2	
R
Equal
x"T
y"T
z
"	
Ttype"$
incompatible_shape_errorbool(�
W

ExpandDims

input"T
dim"Tdim
output"T"	
Ttype"
Tdimtype0:
2	
=
Greater
x"T
y"T
z
"
Ttype:
2	
�
HashTableV2
table_handle"
	containerstring "
shared_namestring "!
use_node_name_sharingbool( "
	key_dtypetype"
value_dtypetype�
.
Identity

input"T
output"T"	
Ttype
w
LookupTableFindV2
table_handle
keys"Tin
default_value"Tout
values"Tout"
Tintype"
Touttype�
b
LookupTableImportV2
table_handle
keys"Tin
values"Tout"
Tintype"
Touttype�
q
MatMul
a"T
b"T
product"T"
transpose_abool( "
transpose_bbool( "
Ttype:

2	
�
Max

input"T
reduction_indices"Tidx
output"T"
	keep_dimsbool( " 
Ttype:
2	"
Tidxtype0:
2	
>
Maximum
x"T
y"T
z"T"
Ttype:
2	
e
MergeV2Checkpoints
checkpoint_prefixes
destination_prefix"
delete_old_dirsbool(�
>
Minimum
x"T
y"T
z"T"
Ttype:
2	
?
Mul
x"T
y"T
z"T"
Ttype:
2	�

NoOp
M
Pack
values"T*N
output"T"
Nint(0"	
Ttype"
axisint 
C
Placeholder
output"dtype"
dtypetype"
shapeshape:
�
Prod

input"T
reduction_indices"Tidx
output"T"
	keep_dimsbool( " 
Ttype:
2	"
Tidxtype0:
2	
�
RaggedTensorToTensor
shape"Tshape
values"T
default_value"T:
row_partition_tensors"Tindex*num_row_partition_tensors
result"T"	
Ttype"
Tindextype:
2	"
Tshapetype:
2	"$
num_row_partition_tensorsint(0"#
row_partition_typeslist(string)
@
ReadVariableOp
resource
value"dtype"
dtypetype�
E
Relu
features"T
activations"T"
Ttype:
2	
�
ResourceGather
resource
indices"Tindices
output"dtype"

batch_dimsint "
validate_indicesbool("
dtypetype"
Tindicestype:
2	�
o
	RestoreV2

prefix
tensor_names
shape_and_slices
tensors2dtypes"
dtypes
list(type)(0�
l
SaveV2

prefix
tensor_names
shape_and_slices
tensors2dtypes"
dtypes
list(type)(0�
?
Select
	condition

t"T
e"T
output"T"	
Ttype
A
SelectV2
	condition

t"T
e"T
output"T"	
Ttype
P
Shape

input"T
output"out_type"	
Ttype"
out_typetype0:
2	
H
ShardedFilename
basename	
shard

num_shards
filename
9
Softmax
logits"T
softmax"T"
Ttype:
2
N
Squeeze

input"T
output"T"	
Ttype"
squeeze_dims	list(int)
 (
�
StatefulPartitionedCall
args2Tin
output2Tout"
Tin
list(type)("
Tout
list(type)("	
ffunc"
configstring "
config_protostring "
executor_typestring ��
@
StaticRegexFullMatch	
input

output
"
patternstring
m
StaticRegexReplace	
input

output"
patternstring"
rewritestring"
replace_globalbool(
�
StridedSlice

input"T
begin"Index
end"Index
strides"Index
output"T"	
Ttype"
Indextype:
2	"

begin_maskint "
end_maskint "
ellipsis_maskint "
new_axis_maskint "
shrink_axis_maskint 
N

StringJoin
inputs*N

output"
Nint(0"
	separatorstring 
<
StringLower	
input

output"
encodingstring 
e
StringSplitV2	
input
sep
indices	

values	
shape	"
maxsplitint���������
�
VarHandleOp
resource"
	containerstring "
shared_namestring "
dtypetype"
shapeshape"#
allowed_deviceslist(string)
 �"serve*2.8.02v2.8.0-0-g3f878cff5b68��
�
embedding_6/embeddingsVarHandleOp*
_output_shapes
: *
dtype0*
shape:	�Nd*'
shared_nameembedding_6/embeddings
�
*embedding_6/embeddings/Read/ReadVariableOpReadVariableOpembedding_6/embeddings*
_output_shapes
:	�Nd*
dtype0

conv1d_1/kernelVarHandleOp*
_output_shapes
: *
dtype0*
shape:d�* 
shared_nameconv1d_1/kernel
x
#conv1d_1/kernel/Read/ReadVariableOpReadVariableOpconv1d_1/kernel*#
_output_shapes
:d�*
dtype0
s
conv1d_1/biasVarHandleOp*
_output_shapes
: *
dtype0*
shape:�*
shared_nameconv1d_1/bias
l
!conv1d_1/bias/Read/ReadVariableOpReadVariableOpconv1d_1/bias*
_output_shapes	
:�*
dtype0
{
dense_36/kernelVarHandleOp*
_output_shapes
: *
dtype0*
shape:	�
* 
shared_namedense_36/kernel
t
#dense_36/kernel/Read/ReadVariableOpReadVariableOpdense_36/kernel*
_output_shapes
:	�
*
dtype0
r
dense_36/biasVarHandleOp*
_output_shapes
: *
dtype0*
shape:
*
shared_namedense_36/bias
k
!dense_36/bias/Read/ReadVariableOpReadVariableOpdense_36/bias*
_output_shapes
:
*
dtype0
f
	Adam/iterVarHandleOp*
_output_shapes
: *
dtype0	*
shape: *
shared_name	Adam/iter
_
Adam/iter/Read/ReadVariableOpReadVariableOp	Adam/iter*
_output_shapes
: *
dtype0	
j
Adam/beta_1VarHandleOp*
_output_shapes
: *
dtype0*
shape: *
shared_nameAdam/beta_1
c
Adam/beta_1/Read/ReadVariableOpReadVariableOpAdam/beta_1*
_output_shapes
: *
dtype0
j
Adam/beta_2VarHandleOp*
_output_shapes
: *
dtype0*
shape: *
shared_nameAdam/beta_2
c
Adam/beta_2/Read/ReadVariableOpReadVariableOpAdam/beta_2*
_output_shapes
: *
dtype0
h

Adam/decayVarHandleOp*
_output_shapes
: *
dtype0*
shape: *
shared_name
Adam/decay
a
Adam/decay/Read/ReadVariableOpReadVariableOp
Adam/decay*
_output_shapes
: *
dtype0
x
Adam/learning_rateVarHandleOp*
_output_shapes
: *
dtype0*
shape: *#
shared_nameAdam/learning_rate
q
&Adam/learning_rate/Read/ReadVariableOpReadVariableOpAdam/learning_rate*
_output_shapes
: *
dtype0
o

hash_tableHashTableV2*
_output_shapes
: *
	key_dtype0*
shared_name	1706869*
value_dtype0	
^
totalVarHandleOp*
_output_shapes
: *
dtype0*
shape: *
shared_nametotal
W
total/Read/ReadVariableOpReadVariableOptotal*
_output_shapes
: *
dtype0
^
countVarHandleOp*
_output_shapes
: *
dtype0*
shape: *
shared_namecount
W
count/Read/ReadVariableOpReadVariableOpcount*
_output_shapes
: *
dtype0
b
total_1VarHandleOp*
_output_shapes
: *
dtype0*
shape: *
shared_name	total_1
[
total_1/Read/ReadVariableOpReadVariableOptotal_1*
_output_shapes
: *
dtype0
b
count_1VarHandleOp*
_output_shapes
: *
dtype0*
shape: *
shared_name	count_1
[
count_1/Read/ReadVariableOpReadVariableOpcount_1*
_output_shapes
: *
dtype0
�
Adam/embedding_6/embeddings/mVarHandleOp*
_output_shapes
: *
dtype0*
shape:	�Nd*.
shared_nameAdam/embedding_6/embeddings/m
�
1Adam/embedding_6/embeddings/m/Read/ReadVariableOpReadVariableOpAdam/embedding_6/embeddings/m*
_output_shapes
:	�Nd*
dtype0
�
Adam/conv1d_1/kernel/mVarHandleOp*
_output_shapes
: *
dtype0*
shape:d�*'
shared_nameAdam/conv1d_1/kernel/m
�
*Adam/conv1d_1/kernel/m/Read/ReadVariableOpReadVariableOpAdam/conv1d_1/kernel/m*#
_output_shapes
:d�*
dtype0
�
Adam/conv1d_1/bias/mVarHandleOp*
_output_shapes
: *
dtype0*
shape:�*%
shared_nameAdam/conv1d_1/bias/m
z
(Adam/conv1d_1/bias/m/Read/ReadVariableOpReadVariableOpAdam/conv1d_1/bias/m*
_output_shapes	
:�*
dtype0
�
Adam/dense_36/kernel/mVarHandleOp*
_output_shapes
: *
dtype0*
shape:	�
*'
shared_nameAdam/dense_36/kernel/m
�
*Adam/dense_36/kernel/m/Read/ReadVariableOpReadVariableOpAdam/dense_36/kernel/m*
_output_shapes
:	�
*
dtype0
�
Adam/dense_36/bias/mVarHandleOp*
_output_shapes
: *
dtype0*
shape:
*%
shared_nameAdam/dense_36/bias/m
y
(Adam/dense_36/bias/m/Read/ReadVariableOpReadVariableOpAdam/dense_36/bias/m*
_output_shapes
:
*
dtype0
�
Adam/embedding_6/embeddings/vVarHandleOp*
_output_shapes
: *
dtype0*
shape:	�Nd*.
shared_nameAdam/embedding_6/embeddings/v
�
1Adam/embedding_6/embeddings/v/Read/ReadVariableOpReadVariableOpAdam/embedding_6/embeddings/v*
_output_shapes
:	�Nd*
dtype0
�
Adam/conv1d_1/kernel/vVarHandleOp*
_output_shapes
: *
dtype0*
shape:d�*'
shared_nameAdam/conv1d_1/kernel/v
�
*Adam/conv1d_1/kernel/v/Read/ReadVariableOpReadVariableOpAdam/conv1d_1/kernel/v*#
_output_shapes
:d�*
dtype0
�
Adam/conv1d_1/bias/vVarHandleOp*
_output_shapes
: *
dtype0*
shape:�*%
shared_nameAdam/conv1d_1/bias/v
z
(Adam/conv1d_1/bias/v/Read/ReadVariableOpReadVariableOpAdam/conv1d_1/bias/v*
_output_shapes	
:�*
dtype0
�
Adam/dense_36/kernel/vVarHandleOp*
_output_shapes
: *
dtype0*
shape:	�
*'
shared_nameAdam/dense_36/kernel/v
�
*Adam/dense_36/kernel/v/Read/ReadVariableOpReadVariableOpAdam/dense_36/kernel/v*
_output_shapes
:	�
*
dtype0
�
Adam/dense_36/bias/vVarHandleOp*
_output_shapes
: *
dtype0*
shape:
*%
shared_nameAdam/dense_36/bias/v
y
(Adam/dense_36/bias/v/Read/ReadVariableOpReadVariableOpAdam/dense_36/bias/v*
_output_shapes
:
*
dtype0
G
ConstConst*
_output_shapes
: *
dtype0	*
value	B	 R
H
Const_1Const*
_output_shapes
: *
dtype0*
valueB B 
I
Const_2Const*
_output_shapes
: *
dtype0	*
value	B	 R 
��
Const_3Const*
_output_shapes	
:�N*
dtype0*��
value��B���NBtheByouBiBtoBandBaBmeBmyBitBinBofBthatByourBonBimBallBbeBisBforBweBdontBloveBsoBknowBlikeBitsBbutBwithBjustBupBthisBnoBwhenBwhatBohBdoBgotBcanBnowBgetBifBoutBgoBdownByoureBoneBtimeBseeBbabyBneverBareBwasBtheyByeahBcantBwantBsheBnotBcomeBwillBhaveBsayBfromBletBcauseBwayBmakeBbackBtakeBatBillBherBwereBhowBasBgonnaBfeelBheBrightBhereBthereBawayBaintBlifeBneedBwannaBiveBtellBwellBbeenBgirlBheartBcouldBnightBwhereBmoreBdayBbyBthatsBgiveBmanBtooBaboutBworldBonlyBtheresBthinkBwhyBgoodBthroughBorBkeepBthenBsomeBwhoBourBlittleBwontBagainBstillBaroundBsaidBoverBusBeveryBlookBeyesBhadBthemBlongBwouldBmindBfindBalwaysBhisBhomeBeverBamBchorusBheyBnothingByaBgottaBbetterBoffBthingsBanBgoneBholdBputBhearBtheseBreallyBintoBtonightBmuchB	somethingB
everythingBshesBthanBbeforeBturnBleaveBliveBaloneBlaBstopByoullBtryBhimBthingBbelieveBcallBrunBstayBheadBevenBlightBnewBgoingBletsByouveBhardBniggaBdidBshowBfaceBmadeBanotherBboyBidBinsideBleftBmoneyBfallBlastBshitBpeopleBoldBshouldBemBrealBplaceBsameBbadBwithoutBendBoohBmineBpleaseBwrongBlostBhasBsunBplayBfuckBhandBhighBbreakBwalkBtheirBnameBdreamBchangeBfreeBfeelingBtrueBbigBownBtwoBthoughtBdieBgodBhesBdoneBhandsBcryBwhileBmyselfBwhatsBfireBenoughBdanceBsoulBsongBcareByesBmaybeBcomingBfarBlookingBsweetBotherBsomeoneBwaitingBforeverBtalkBbitchBsideBwaitBdaysBrememberBbringBnaBrockBtogetherBstandBskyBbestBthoseBstartBmustBtoldBmightBcrazyBcameBdidntBniggasBwatchBwishBwordsBtillBbodyBroundBkissBpainBfoundBcomesBsingBhopeBmoveB	everybodyBcoldBdreamsBbecauseBblueBusedBfirstBrideBhitBrainBworkBknewBtimesBgirlsBhellBcloseBblackBmanyBhelpBflyByoBreadyBonceBmeanByoungBseenBtownBalrightBtouchBfriendsBloseBdoorBlivingBuhBlonelyBgettingBnobodyBgameBuntilBtryingBboutBgoesBahBopenByourselfBfightBmakesBmayBfriendBrollBtookBbeatBforgetBthoughB	sometimesBdeadBafterBtheyreBmorningBsureBbehindBlieBanyBlineBsleepBtodayBlordBheardBsomebodyBsmileBelseBknowsBmatterBdarkBdeepBwholeBhappyBhateBhotBanythingB
understandBrunningBtearsBgoodbyeBdaBwomanBmissBeasyBlateBmusicBunderBroadBfallingB	beautifulBsoundBcityBtruthBlightsBgonBguessBlistenBkindBaskBaliveBprettyBbrokenBweveBpartyBassBfeelsBsawBarmsBstarsByearsBhurtBtilBfineBshakeBgroundBnextBchanceByallBliesBsetBwentBheavenBeachBhideBwantedBcouldntBalongBsaveBtopBroomBhouseBseemsBbloodBshineBwhiteBwakeBthinkingBstreetBfloorBairBverseBsoonBsinceBgaveBburnBlotBtriedBlowBfeetBboysBlayBfoolBslowBwonderBbedBreasonBstrongBgoinBwindBredBdoesByoudBmoonBdamnBpartBkillBsuchBwordBlovesBmamaBmakingBfunBbornBfearBfullBstarBcontrolBdoesntBheartsBveryBmeetBpastBhoneyBtomorrowBwhoaBdoingBfastBthrowBwildBaboveBwaterBlovingBwhosBapartBbabeB2BblowBstepBmomentBdropBgetsBanymoreBtalkingBpullBcoolBbeingBonesBwouldntBlovedBstandingBcarBbetweenBeyeBtiredBhairBfollowBseaBpayBwitBthreeBeveryoneBstraightBtakesBwalkingBuponBsadBwarBsaysBafraidBsorryBbothBrestBdeathBseemBlandBcatchB	christmasBtakingBchildBfeltBcutBsitBcallingBcuzBkingBburningBdrinkBwantsBtightBbitBladyBuseBlearnBclubBnothinBimaBgettinBcaughtBstreetsBlookinBstartedBgunBtrustBlipsBsayingBsummerB	somewhereBmotherBbreatheBcominBholdingBdearBwhateverBgoldBturnedBbitchesBvoiceBwallBbreathByetByoursBtryinBphoneB	differentBnightsBdriveBsickBearthBmetBspeakBpowerBriverBperfectBrepeatBpromiseBshotBblindBbrightBreachBpeaceBhaBsingingBwasntB1BskinBspendBfeelinBmostBniceBclearBuBsonBlivesBoutsideBacrossBloverBalreadyBdirtyByeaBmadBworthBfrontBdancingBpassBprayBfuckingBriseBjumpBpickBgreatBmouthBisntBkeepsBmenBcarryBcheckBwatchingBemptyBblameBhalfBtalkinBsmokeBkidsBfellBgrowBmeantBplayingBknownBleavingBkneesBoBstoryBwinBdarlingBcalledBangelBcryingBtasteBimmaBgivingBlovinBnearBdaddyBchildrenBmillionBbrokeBringBnowhereBpopBsendBtrainBbuyBfinallyByearBokayBfuckinBlilBscaredBwaysBnumberBleadBloudBtroubleBhuhBswearBmovingBcannotBbetBbluesBlooksBhigherBminuteBfaithBfutureBtearBbangBworryBi’mBneedsBdogBfadeBbelongB
everywhereBniggazBactBsecondBgreenBreadBscreamBwindowBlaughBhelloBmemoriesBjesusBdoinBfourBcloserBnBeverydayBtellingBguyBbreakingBspaceBeatBhangBwhichBhookBtrynaBpushBagainstBsomedayBthousandBthankBlivinBshellBturningBpictureBlookedBanyoneBwarmBcrossBdreamingBlosingBchangedBgimmeBbrotherBshameBwallsBshareBdarknessBsightBwalkedBdyingBfillBwasteBshoesBstrangeBiceBradioBsignBtheyllBmilesBx2BwideBschoolBmagicBfunnyBhoesBstuckBrunninBfeelingsBratherBdoubtBwingsBwearBclothesBwineBflowBshiningBstoneBohhBkickBshutBfiveBbyeBmemoryBfatherBtreatBheatBhoodBboomBanswerBwriteBalmostB3BthoughtsBbecomeBfamilyBloversBwithinBmiddleBballBangelsBsomethinBsongsBbrainBmessBshoutBjoyBmeansBaheadBdevilBpointBlongerBsomehowBanywayBsafeBhoBcmonBtenBhurtsBpoorBsexBpretendBsingleBladiesBrichBcloudsBmissingBoceanBmrBpussyBsimpleBkidBworkingBshootBprideBheresBlessB	yesterdayBhundredBayBsnowBstyleBblockBcountBhappenBthinkinBtreeBouttaBfewBpaperBholyBqueenBbridgeBsecretBlyingBcornerBsittingBsilenceBwomenB
everybodysBfreedomBcosBglassBnahBbeginBshadowsBgladBwoahBsenseBspecialBkindaBskiesBsmallBholeBpieceBmirrorBchooseBshallBheldBeBcrowdBcashBstormBturnsBdawnBcleanBokBsunshineBdiedBhourBagoBstickBknockBneededBhangingBdryBwedBdickBgBdeBsixBbeyondBplayedBrealizeBbrandBheavyBwelcomeBrollingBsexyBhaventBluckyBsayinBsleepingBtrackBehBhavingBkeptBslowlyBfakeBfightingBtreesBkillingBdemBcriedBmidnightBdon’tBdealBhumanBlearnedBcountryBlooseBmakinBanybodyBraiseBrapBshawtyBinsaneBlinesBleavesBmountainBbroughtBflameBnoneBkeyBluckBawakeBdustBevilBbonesBweakBbesideBinsteadBquiteBsupposedB	searchingBdesireBtakenBplanBbrownBnewsBstupidBbirdBstartsBendsBsceneBsinBgamesBplacesBdressBhappenedBsurviveBcrackBbuildBwifeBhoursBsBpriceBsilverBforgiveBknowingBsandBgoldenBdrunkBbottleBedgeBpiecesBstealBwinterBchainBwheneverBspentBboneBpaidBprobablyBshadowBquickBfateBtvBaskedB	screamingBgloryBcoupleBdooBlifesByorkBbeautyBstateBbusinessBfallsBflyingBfacesBghostBshedBothersBbandBwaveBrollinBwestBfatBmindsBfreshBfingersBgrabBproblemBparadiseBleastBgivenBcoverBcarsBsugarBescapeBbarBboughtBproveBlaterBitllBjobBdrivingBfilledBstrongerBworldsBrulesBtwiceBwhereverBsellBmiBwalkinBanywhereB
hallelujahBsisterBproudBdiamondsBsouthBexplainBdjBbottomBooohBtypeBslipBmaBfreakBbleedBfactBreturnBrockinBharderBbagBdatBmovinB	wonderfulBcaseBliftBdBdumbBgraceBfitBspotBviewBtongueBrhythmBboundBdoubleBaskingBhopingBbellsBbeneathBsevenBroseBwetBlovelyBspiritBfuckedBlaughingBstrangerBstareBbounceB2xBplayinBsoftBtripBtoughBsorrowBridingBranBguitarBsundayBbringsBraceBdiBmommaBneckBdiamondBit’sBbookBstrengthBsoundsBmaryBmercyBgunsBdollarBsmellBwastedBbirdsBdirtBhillBdenyBsweatBfavoriteBbaBspitBbelowBlockedBbowBlockBnineBtakinBquietBgrownB	wonderingBfameBweekBearlyBwhisperBfigureBbuiltBchainsBstuffBwooBdigBdeserveBnothingsBquestionBmistakeBcrimeBclimbBgodsBtallBcupBfairBsilentBheadsBstoodBeveningBfeedBshortByellowBbustBmachineBchestBvoicesBweatherBlivedBseeingBwhipBchangingBwokeBflowersBdareBdarlinBquitB	surrenderBruleBgrowingBmansBgraveBregretBmovesBshotsBkeepingBchaseBcaresBchickBtheyveBmurderBlyricsBpackB	happinessBwashBswingBlatelyBhandleBsentB
underneathBageBwaitinB4B	breathingBweightBbBprayerBeverythingsBspinningBmistakesBbeatingBshouldntBguysBdopeBspinBplansBfearsBspringBromanceBrespectBfixBthunderBnakedBcallsBrushBselfBridinBchoiceBdrownBlettingBasleepBclockBlaidBkissedBshoulderBimagineB	disappearBthaBrisingBdoorsB	attentionBwavesBcandyBpaintBfallenBbiggerBhistoryBthrillBmotherfuckerB	everytimeBstaringBhighwayBhurryBfallinBfoolsBsoulsBwroteBbeatsBfleshBbattleBhidingBmoBghettoBweedBsittinBhungryBsecretsBboxBfasterBjackBsurpriseBspeedBsuperBbossBwheelBmovieBletterBsantaBmiseryBclapBsoldBgivesBtuneBearBproblemsBbreaksBthangBfingerBfinalBworseBbiteBspellBjoeBsuddenlyBdrinkingBvisionB	listeningBbendBhappensBremindBdistanceBrealityBtaughtBsavedBmessageBdeeperBrecordBspreadBteachBfeverBhoeBfolksBpassedBrosesB	moonlightBkissesBstageBpassingBleanBforgotBworstBchasingBburnedBlawBbrothersBwaitedBolderBmerryBflamesBwrittenBnoiseBpathBfootBpocketBpicturesBmelodyBteethBbombBmissedBlegsBshortyBpulledBburnsBwishingBbreezeBshipBendingBdoctorBjohnBsteadyBcatBsailB
girlfriendBseatBcolorBpleasureBheavensB	beginningBreasonsBcryinBpromisesBbegBpreciousBdressedBbootyBsignsBrocksBslideBroughBknifeBenemyBshinesBtableBglowB50BwonBsearchBparkBbassBtellinBhopB
californiaBcrownBcriesBactingBcloudBmysteryBhealBcolorsBooBgrindBstartingBdestinyBclosedBtherellBtestBeastBrhymeBcallinBseemedBbegunBbusyBnobodysBcrewBdangerBshouldveBendlessBrainbowBkeysBfantasyBtBcakeBstoppedBsmilingBpromisedBchristB	forgottenBcrashBmightyBkilledBhomieB5BpassionBgrassBunlessBhatersBcrawlBbitterBgangstaBsaturdayBgrooveBxBblowingBayeBbearBswitchBsteelB
somethingsBfadingBwatchedBhedBbeachBamericaBchangesBdancinBpourBtroublesBwheresBeitherBremainBseesBbeginsB
differenceB	questionsByou’reBtwistBchillBanswersBmeaningBlifetimeBshowsBcBtornBtwentyBpoppinBeaseBlickBtenderBjeansBthinBamericanBsuckBdragBbellB	ourselvesBtiedBjoinBwiseBlikesBstoreBmorninBteamBoooBtellsBshoreBdecideBflipBohohBgreyBwhatchaBpureBthrewBstoleBexactlyB	satisfiedBcastBrunsByuhBcalmBamazingBlonesomeBheroBplaneBtieBmomentsBsoloBgivinBblessBsunnyBdrugsBringsBchurchBworkinBarmBwantingBcomfortBcruelBbeganBwheelsBdogsBnoseBgardenBbuildingBpoisonBearsBadmitBroofBolBfaultBmattersBpressureBbulletBbleedingBtruckBtheeBshakingBsinginBcostBwastingBtideBgrowsBbraveBgreatestBhatBgangBdimeBwearingBdadBdesertBnamesBmmmBleavinBcoastBbottlesBblowsBminutesBflightBbooBmoodBplusBclassBsoldierBzoneBveinsBjudgeBguideBswimB	mountainsBscarsB	heartbeatBthugB	dangerousBbreadBcourseBbumpBthatllBgrewBboatBpraiseBalthoughBfoolishBstayedB	strangersBcompanyBhorseBdreamedBthinksByouthBwrappedBhonestBbusBfridayBdaughterBwindowsBmomBmasterBattackBbringingBhipB	goodnightBfoodBliedBripB	hollywoodBconfusedBcheapBhollaBhillsBcertainBgrandBadoreBdrugBbillBjohnnyBswallowBhitsBcan’tBreachingBfieldsBsettleBraisedBbankBshowedBpersonBwindsBx4BmovedBsillyBtrickBgalBdiesB	telephoneBdisguiseBlaughterBhustleBqueBblessedBmarkBgrayBmonkeyBnoticeBactionBartBrhymesBfailBbeerBahhBbeggingBdoggBsmokingBeightBpageBforceBshyBpumBdoughBhotelBbodiesBwalksBsurelyBrockingBpoliceBtouchedBburyBstoriesBvainBwowBpullingBlaneBnastyBnumbBmileBhanginBcountingBsatBwayneBkissingBplaysBwickedBpantsBfadedBhallBtrulyBforthBbarelyBguiltyBarentBmotionBtrapBuhhuhBkitchenBsmilesBdrowningBgasBweekendBdollarsBuglyBmetalBmisterBrageBdudeBnatureBinnocentBmarriedBlouderBfamousBdrumBcircleBsmokinBgiftB	directionBholdinBnationBembraceB	happeningBmicBprayingBtravelBx3BchancesBwatchinBmothersBontoBbelievedBanimalBstepsBliarBburiedBpushingBmadnessBhopesBlotsB	heartacheB	lightningBwhetherBheadedBexcuseBtwistedBbirthdayBbabysBloadBsparkBslaveBnorthBbeastBpoundBanytimeBopenedB	situationBreleaseBintroBdivineBtipBtaBplainBsinkBholdsBthanksBwarningBcompleteBpaBbopBbayBsomeonesBstandsBkillerBmiracleB	believingB	sacrificeBkingsBhardlyBbootsBcurseB	somebodysBislandBwrapBwhoseB
revolutionBfunkyBblowinBfieldBthickBlistBdarkestBdonBstonesBcribBlessonBdemonsBashesB
lonelinessBgucciBcopsBjokeBdestroyBlevelBclownBseasonBpimpBfloatingBbagsBjamBfishBburninBfurtherBcirclesBexpectBlitBcoffeeBundergroundBstrikeBrecallBchairBstopsBohhhBwoodBtrippinBlondonBhungBseekBfBblewBspokenBuniverseBhurtingBcoatBhushBflashBsangBticketBjealousBsighBchaBholidayBdateBconversationBstationBmissionBforwardBdowntownBhooB
impossibleB
generationBsnoopBoftenBsinkingBdrawBshootingBmondayBexceptBorderBdramaBsheetsBgripBdevilsByeBringingBfortuneBdistantBundoneBsingsBjailBtrappedBsuicideBvalleyBmmBstandinBmBsipBflowerB	celebrateBbrighterBignoreBthroatBoursBclickBpapaBsufferBsirBmeltBcreamBuhhBlustBdiseaseBgentleBfindingBworriedBfrozenBsinsBcardsB	champagneBjBwerentBbotherBwillingBmatchBfoBfancyBmotherfuckinBsystemBbullshitBplentyBhipsBshirtBwipeBfinishBresistBangerBsadnessBcureBableBwakingBeasilyBsimplyBi’llBhollowBloadedBelectricBbumBbloodyBshelterBpumpBceilingBlimitBjungleBbecameBsqueezeBtheydBrowBwireBlearningBclosingBputtingBbarsBsuitBhavinB	recognizeBcopBstripBpurpleB6BwatersBshapeBsplitBshopBremainsBcrushBvictimBlazyBlBunknownBsoftlyBroadsBsavingBcatsBenjoyBclaimBbreakinBgatherBcentBheelsBcouldveBasideBcoveredBcarefulBangryBkingdomBdroveBtracksBwristB10B	innocenceBrainingBfliesB	everyonesBprivateBthyBrefuseBproofBoweBnoteBwitnessBstayingBcrossedBleadsBshadeBplanetBpinkBnooneBtoesBpaintedBledBdaylightBscreenBcommonBpityBharmBshadesBnaturalBpBwhiskeyBcherryBrelaxBcageBblastBrawBeternityBneonBbedroomBpillowBformBactinBwritingBfreakyBthrowingBgatesBboogieBdreaminBdipBworksBhiBscoreBbecomesBsoldiersBsmartBshakinB
pretendingBstruggleBseriousBemotionBkicksBdaddysBemotionsB	afternoonBcardBadviceBjingleBbillsBleatherBawhileBnervousBfunkBenergyBrestlessBoughtBfadesBlongingBcrashingBknockingBspokeBchicksBprisonBwearyBfistB	boyfriendBbuckBoooohBtearingBgayBdrinksBrecordsBattitudeBsunlightBmissinBrescueBpocketsBpillsBlaughedBdroppedBwashedBsunriseBviolenceBlemmeBjourneyBflatBpressB	confusionBhmmBriversBchoseBslippingBplasticBbrickBamongBflowsBflagBshowingBashamedBexistBcozBstackBcandleBmixBlettersB9BfloatBdrinkinBballinBheadingBenemiesBclausBblownBagreeBworkedBmarryBhearingBeraseBlayingBtouchingBhiddenB	nightmareBliquorBappleBstaysBcellBtradeBheavenlyBcokeBideaBweddingBechoBrainyBfedBofferBrainsBjetBseasBseasonsBconfessBzeroBsizeBsweeterBeasierBaddictedBframeBbeefBhammerBcrawlingBaimBstringsB	shouldersBsunsetBfashionBdancerBaffairBtalkedBmainBhungerBwaitsBdripBbulletsBthouBpickedBtickBpairBtrunkBregretsBfiBdamnedBowBdancedBsistersBmotherfuckersBsmoothBreligionBbirthBplannedBlyinBaddBtexasBhaloBbenzBtricksBlottaBtraceBhorsesBimaginationBspeakingBbodysBupsideBtuBpouringBplayerB
summertimeBlungsByBillusionBdriftingBserveBmonsterBpoolBnearlyBsaintBmikeBspendingBwreckBrolledBquarterBordinaryBcreepBfoughtB	followingBdiceBarmyBkillinBeatingBdinnerBsakeBrunawayBbrainsBsweetestBteaBthruBsaviorB	cigaretteBpenBtaleBpaleBsippinB	shatteredBhimselfBdrankBbeinBcheekBfreezeBhopelessBchrisBthroneBtinyBthrownBcuteB	hurricaneBbubbleBhahaBclueBgateB	salvationBheroesBbillyBprinceBropeBreachedBdriftBalbumBmendBstressBprotectBteaseBnumbersBgainBrollsB	desperateBboredBtriggerBforeignBtrashB
completelyBswimmingBturninBironBextraBroamBdollBcarriedBtowardsBthirdBbooksBsetsBnamedBjimmyBjuiceBbabiesBjumpinBfaithfulBevryBpieBseedBswagBwinnerBpushedBmentionBsupposeBsharedBgentlyBcrystalBleeBlikedBhomiesBwanderBshouldaB
reflectionBstreamBsquareBbareBlossBdropsBscreaminBrappersBcheeseBdiscoBpistolBfanBstBholesBghostsByardBrebelBdisBwoBtoeBstringBcadillacBjaneBelBspareBcutsBfalseBharmonyBeternalBprayersBdespairBpatienceBscreamsBvisionsB	sufferingBwoundsBsellingBdrivinBdeeByehBgsBscratchBchickenBpillBhardestBmamasBsmashBsmiledBfiguredBmarchBacceptBfathersBumBsacredBlakeB	surprisedBoclockBguardB
understoodBsouthernBwaBrBtrailBstairsBappearBkeepinBfocusBriderBbelongsBsometimeBpalmBwisdomBwinningBkneeBbiggestB
temptationBsnapB
conscienceBwhollBwhistleBorleansBmonthsBflowingBdamageBseparateBendedBbricksBspineB100BparadeBtreasureBtrafficBleadingBcowboyBworeBlameBhauntBsettingBherselfBblazeBcolderBtoyBdrivesBuptownBawBpunkBmoviesBakonBtoastBrealizedBshookBthugsBpackedBachingBniBmapBcheatBjumpingBcameraBbewareBcaredBstacksBsixteenBsatisfyBriskBseanBrangeBmilkBfamiliarBbeeBsonsBlipBcomplainBhorizonBfillsBaffordBhotterBgravityBskipBweeksBvibeBplayaBwhooBfiftyBbomBspiritsBnaughtyB8BexplodeBchokeBdrumsByoungerBstoppingBrentBi’veBflyinBgrantedBweepBsecondsBmessedBwhydBfiresBmudBhugBrevengeBteBblindedBchosenBlackBsailingBnailsBmodernBmaskBloserB20BwaistBjumpedB	emptinessB
sweetheartBsummersBfarewellBchromeBrocketBlastsBancientB4xByahBsteppedBdecidedBdyinBcharmBsidesBlanguageBforgivenessBalsoBsleighBknockedBhatingBpurposeBbunchBtalksBhailBruinBnickiBhatinBusingBwhoreBsmackBshockBgreedBduBcaptainBsaltBdrawnBtoysBrewindBfollowedBdiveBmichaelBkillsBsolidBbuddyBsendingBhelplessBfellasBlullabyBsquadBvictoryBsouljaBmodelBmagazineBknockinBflewBparisBjonesBcastleBdumBselfishBhesitateBbuttBbanksBriotBmixedBwolfBwhispersBsnakeBmillionsBmedicineBroleBpositionBsanBprizeBunderstandingBstolenBhealingBgypsyBpaulBpagesBblissBjamesBitdBtragedyBsworeBshininBcookB
themselvesBratB	affectionBwouldveBsearchinBvideoBsuckerB
heartbreakBdeckBcracksBtapeBsleeveBmiamiBbellyBswayBawfulBthat’sBautumnBracingBpresentBteacherBshowerBpoleBpimpinBmallBconcreteBwishesB	invisibleBrayBelsesBballsBquicklyBchillinBnorBcomplicatedB7BsaleB	importantBexcitedBswordBclipBsympathyBpreacherBmamiBplatinumBdawgBbridgesBplateBtourBdemonBshoeBputsB	tomorrowsBkickedBjayzBslapBpotBpanicBsleepinBwoundBforeBfoldBsoberBmonthBwarsBthiefBlosinBswingingBinchBdriverBslippedBsensesBrapperB	butterflyBlouisBfactsBegoBtroubledBkissinBcreatedBrobBpapersB3xBloBfloodBblinkBnerveBcommandBmotherfuckingBcockBcocaineB
yesterdaysBpartsBstormyBhollerBneighborhoodBhighestBforestBkickingBitselfBtablesBpaceBputtinBmajorBallowBmackBpennyBhazeBfailedBuncleBpublicBfloBgoddamnBflashingBecstasyBmarchingBdeniedBoleBlegBavenueBpickingBcourtBstereoBseedsBain’tBridBweaponBsentimentalBpsychoBcreditBwornBoceansBimageBboardBeveBthrowinBenterBhookedBterrorBthirtyBrollerBchargeBbandsBrubberBhauntingBbladeBshinyBtiBdecemberBtailBeagleBdueBcollideBdiggingBslippinBgeorgiaBdeliverBcodeBtwelveBbobBmeatBblahBteenageBcoversBcheerBwoodsBmexicoBmarsBjointBfinishedBengineBworriesBstumbleBsharpBcompareB	superstarBsungBparentsBhauntedB	seventeenBdailyBclearlyBbombsBjusticeBeverlastingBsitsBpullsBneitherBqueensBvegasBshelfBpunchBleBtigerBohohohBhonestlyBayyBwearsBfortyBbibleBsackBpissB	apologizeBweaknessB	there’sB	guaranteeBpreparedBbrooklynB	tennesseeBpostBexpressBcandlesBhabitBtwilightBlionB
intentionsBcenterBjuneBkickinBreplaceBmollyBjealousyBiiBboldBaahBshadyBviolentBdreamerBshownBdingBnuhBdimBdelightBtoreB	neighborsBmommyBdudesBcottonBpersonalB	automaticBlapBcurtainBneedleBfindsBfansBclimbingBanimalsBtriesBsleptBsticksBsteppinBhopinBsoonerBborderBsunsBbeepBprayedBpassesBwingBrefrainBcrowdedBbroadBdiscoverBalleyBridesBpianoBguiltBcountyBbluntBcouchBcharmsBtrippingBmemphisBcreateB	breakfastByousB
whisperingBscienceBconstantBbacksBoutroBknowinBjacksonBgazeBtowerBkBbelieverBechoesBrightsBginBdecisionBcourageBbringinBvelvetBrumB	backwardsBsteppingBnutsBlaughsBuselessBdestructionBleaderBbahBstealingBjaBstomachBlingerBbuzzBpullinB	sensationBdisasterBplugBsinnerBseBoriginalBchipsBrecklessBjayB	whisperedBurBsuccessBroomsBdeadlyBshaBsatisfactionBrubBduckBcolourBthighsBtoneBhonorBfruitBmeetsBshoBlambBfiredBspinninBshipsBmacBneedingBgoodnessBdivideBcreationBspanishBcreepingB	presidentBmoanBfenceBfooledBstretchBawareB	footstepsBponBlaceBstonedBperfumeBrevealBdrainBpardonBmakerBdoveBrudeBfuckerBbuttonBhittinBbeholdBparkingBroarBperhapsBcenturyBsurfaceBeyedB	septemberBdefeatB
compromiseBhasntBdizzyBlisaBexBcoupeBvisitBsaintsBtalesBlipstickBlendBgreaterBfreaksBshotgunBborrowB	representBspeaksBindeedBhatredBdeyBragBarriveBtiesBmentalB	wanderingBdoubtsBfrownBeyB
tendernessBglockBdeepestBpoppingBgoalBsleepsB
connectionB
governmentBcruiseBbegginBamenBnoticedB‘causeBtuesdayBbettaBmillBpoundsByupBvanBsurroundBsheepBpresenceBlegendBinspirationBbloomBsoreBvoidBusuallyBsocietyBweezyBoilB
cigarettesB40BrainbowsBmailBwishedBremindsBgutsBburntBwon’tBsymphonyBaskinBstrikesBreceiveB	gentlemenBhahBcrookedBcoreBbentBwintersBoughtaB	breakdownBspillBpipeBhearsBdesignBastrayBinnerBfarmBcircusBspiteBrodeBwarmthBpossibleB30BsuddenBtossB
surroundedBshoveBreignB	knowledgeBhowsBdialBdestinationBbobbyBvoodooBuntoB
forgettingBfairyBmistBheartedBnanaBinnaBtextBshiverBstrayBpatientBgoodbyesBalarmBpayingBprincessBfragileBshowinBobviousBchopBfifteenBfaBburdenBvowBhadntBgrillBcurtainsBbuyingBweeBooooBcitiesByepB
microphoneBwhoeverB12BswellBworshipBunitedBtortureBpuffBtattooBvBgasolineBburstBfrenchBnevaBmakeupBfightsBhatedBdotBbutterfliesBtumblingBstruckBtreatedBboreBacheBhealthBdreBcollegeBseatsBlayinBreadingBboringBaceBvipBslickBporchB
photographBjulyB
constantlyBbrideBmaneBscarBphysicalB	spotlightBsortB
especiallyB	chocolateBpinBwondersBthirstyBrootsBrareBlistenedBinformationBenBenglishBtangledBwishinB	travelingBtapBglanceBchaosBvirginBterribleBsprayBsatanBhandsomeBallowedBscaryBpeoplesBfriendlyBmammaBjewelsBdoeBwonderinBtastesBslipsBdahBalcoholByellBpeacefulBhelpingBflexBbaddestBwomansBvacationBsleepyBlessonsBcatchingBblessingBmeasureBhumbleBstompBmaterialBdrippingBskinnyBpoundingBpearlsBhalfwayBsteamB	perfectlyBblankBshiftBlocalBchicagoBchasinBbutterB
appreciateBrimsBunBsignedBpeepBhelpsBrichesB
nightmaresBexcusesBtotalBmississippiBfoxBcleverBfrontinBateBrefrãoBhelpedBfinestBrippedB
confidenceBthangsBsellinBdevotionBstrappedBspeakersB
hypnotizedBnotesBchoirBgrinB	prechorusBmrsBkillersBhousesBbrushBabuseBupsBhomesBclosetBscentBhuntBcoloursB	sleeplessBcuttingBbangingBmirrorsBliftedBcomfortableBbustinBweirdBscrewBpartnerBlabelBforgivenBunkindBsuitcaseBshallowBsingerBfillingBdroppingBahaBmessingBclingBagesBpushinBphantomBwhomBstrapBslamBprepareBglassesBnellyBlawsBhaterBcarelessBsippingBdrBdoomBstuntinBrazorBjimBjiveBviciousBrottenB
wonderlandBmassBchewBupsetBstainBserviceBfavorBtickingBnawBmiraclesBlunchBhatesBgotsBusaBleafBfogBwiggleBsweptBsirensBprisonerBfacingB
perfectionBmisunderstoodBdependBsuitsBlandsBcausedBlaughinBfussBcaressBblingBbeesBstayinBsealBseeinBglowingBcornBayoByearningBusherB	satelliteBfckBcloudyBshoppingBmessinBthankfulBtensionBsharingBcradleBovercomeB
incredibleBpartysBbowlBstingBdescribeBchampionBsupermanBjusB
heartachesBwavingBjarBdegreeBnormalBcriminalBchevyBcheckingBbetrayedBlighterBdeafB
iâ€™mBpowersBhurtinBunderstandsBmeetingBfighterB	daughtersB	boulevardBbentleyBtrainsBslimBsiBprayinBshootinBduesBwe’reBreplyBdavidBtollBniggerBmansionBcornersBchoicesBsamBbeamBtragicBluvB	decisionsBmotorBmomsB	existenceBwealthB	starlightBgeeBdynamiteBcharlieBscareBorangeBlowerBbathroomBtinBbathBrushingBfuneralB
frightenedBrattleBinstrumentalBcrankBsallyBrepBfightinBfifthBenvyBdrewBcoalB
reputationBmuscleBchinBthreadBsolutionBrappinBcrackedBjokesB	teardropsBunhappyB
televisionBkindnessBcouldaBtonguesBkittyBhornBgloomBfeedingBbelievesBwebBsocialBposeBpitBhopedBdecayBgriefBcursedBrememberingBrailroadBhBreactionBfoolingB	blessingsBbucksBconquerBcapByayBwonderedBweaponsBtanBsparksBohhhhBfitsBdewBstoppinBnuttinBlatestBgutterBfilthyBdisgraceBlosBbeliefBragingBpissedBmovementBfedsBelvisBcrumbleBcontinueBahhhBstuntBhonestyBforcedBpracticeBcarpetBtopsBoddsBdealingBbamBstrainBporBpeterBjeBhidesBgratefulBstickyBromanticBpressedBnationsBworthyBuntrueB	emotionalBblazingBwarriorBsolveBsecurityBbabylonBphaseBnailBchinaBwhodBsightsBrockyBfilmByonderBsaneB24BwohBdesiresBcabBsicknessB
eventuallyBcollarBwillowB	patientlyBhypeBblindingBtonightsBrapeBpreachBnaeB	ignoranceBfixedBfeatherBcousinBbumpinBariseBwarnBtripleBtoothBindianBstaticBratsBsinnersBcumBblocksB
backgroundBgaBgreetB
disrespectBspoonBplayersBbikeBtankBpantiesBmagicalBjerkBflavorBdespiteBloneBhiphopBfacedBchillsBbustaBmusicsBlibertyBinvolvedBgottenBroundsBhoweverBcherishBwolvesBsanityBhayBgladlyBfuturesBskirtBgatBbasementBinsanityBempireBbindBtomBouterBfailureBsafetyBrealiseBfuseBshe’sBhidBwoundedBreverseBmillionaireBkushBaverageBtagBjahBhonkyB
guaranteedB	creaturesBcraveB	obsessionBdrownedBbanginBtumbleBplantBlargeBbullBbecomingBamazedBstormsBrelateBpreyBmariaBchokingBgyalBexitBdrivenB	connectedBcamerasBbailBafricaBwitchBkarmaBclassicBthoBgloriousBfollowsBdarkerBbeltBvineBtameBnickBharlemBgloveBdefendBconsiderBknoBhallsBdisappearedBstakeBmeltingBkneelBcountinBfuelBdroB	childhoodBpauseBkindsBgearBthirstBdigginBcompetitionBclayBbroBbastardBwearinBboBstudioBunionBtonyBrabbitBmoneysBbustedBargueBbarrelBjudgmentBminesBinsecureBcrunkBremedyB
protectionBmiaBideasBhustlerBgimmieB	raindropsBpointsBpartiesBlouieBfictionBdragonBdenBconnectBaprilBvictimsBshitsBhappeninBstrungBromeoBreBjacketBcookinBbalanceBwoeBtotallyB	swallowedBmoldBminajBbehaveBgrainBboothBactuallyBwhipsB	magazinesBdebtBbaseBrelyBmcBmankindBgrabbedBreliefBwindingBflownBeatinBcelebrationBweepingBsourBlettinBhittingBdatsBwhineBsweepBimpressBbeatenBvenusBrustBfourthBpaintingBneBunfoldBmellowBmateBdrakeB
atmosphereBwanBooooohBtunnelBswaggerBslayBfinnaB	concernedBcaliBsueBsandsBgreedyBfartherBdemandBcheatingBdenialBarrivedBambitionBpickinBlet’sBdreamersBchecksB	apartmentBsolitudeBmealBdunBdashBgiantBgirlfriendsBesBarrowBwoohBshoutingB	mistletoeBjustifyBcheckinBbondBbattlesBtockBschemeBoohhBhustlinBcreatureBalBtastedBshackBbackseatBstashBsorrowsBpileBhumBearnBsumBclownsBbillionBstrutBmornBgeorgeBceaseB–BtrustedBstartinBiiiBdustyBbatBsourceBrickBwahBcarryingBstrandedBsocksBclearerBbroadsBi´mBfullyBeminemBpavementBhusbandBwarnedBcultureBgetchaBcliqueBavoidBtaxiBremixBneathBequalBelevatorBshakesBcrushedB	worthlessBthrillsBroverBrihannaBrejoiceBgiftsBstabBrisesBcreepinB	valentineB	righteousBmcsBgangsterBfactoryBwackB
rememberedBprimeBmonaBmarieBwillieBsendsBkellyBdownsBbeamsBladderBkillaB
invincibleB
experienceBevidenceBtempleBseekingBdroppinBsavageBrouteB
fingertipsBtpainBtargetBsneakBscenesBfeastBchainedBwidBtraveledBtranceBjonBmsBcheeksB	scatteredBplotBnervesBcoffinBblanketBlampBjeanBfocusedBclimbedBblondeBgooseBglimpseBdefineBponyBnanBglitterBmodelsB	fantasiesB	eternallyB	spreadingBreplacedBplanesBmuthafuckinBmachinesBfoxyBdrippinBwheredBpositiveB	emergencyBdoorwayBfakingBtripsBpearlBxzibitBindustryBfellowBtossedBjuicyBeffectBrotBbruisedBsmokedBofficeBhoustonBgrowinBglueBbreedBasksB
throughoutBsidewalkBmarketBfountainBclubsBnorthernBlosersB	destroyedB
undercoverBsmellsBsealedBpopsB	endlesslyBdividedBdeceiveBbushBwBregularBpigBgradeBannieBsidewaysBsexualBmazeBlucyBlloydB	introduceBdullBstrokeBsailedBlouBhowdB	buildingsBsmilinBpulseB	favouriteBromeB
resistanceBhowlingBcrowdsBmouthsBjunkBfiguresBweaveBtrialBcancerBshieldBprojectsBlalalaB
friendshipBsowBguidingBgarbageBproperBpanBovertimeBlisteninB	gentlemanBcastlesBzoomBzBtwistingBpleasedBnoonBknivesBcrimesBwashingB	timbalandBradarBpleadBmorningsBhumpBdreamtBsnakesBforcesBapplauseBplacedBsuckingBfucksBdwellBdegreesBbugBzooBtittiesBpoetryBmercedesBjukeboxBelevenB	rocknrollBicyBfrozeBbucketB	blackbirdBounceBmumBjennyB	addictionBrubyBpumpingBgatheredBfemaleBbananaBamongstB45BtowardBstainsBracksBnarrowBfatalBsuckersBpleaBhangsBdamBsweetlyBraisingBmistyBchopperBbeatinBraysBmobBdugBboatsBpeelB
incompleteBveilBthreatBlocoB	dedicatedB	returningBnoelBfiendBdraggingBwinsBtendBlawnBcowBcarriesBtenderlyBsilkBguitarsBgrindinBdozenBcrashedBtownsBflirtBdawningBcontentBappearsBsailsB	conditionBhornsBcuriousBvestBsearchedBobeyBneedlesBleapBcrossingBbreathinBbotheredBguessingBalibiBobsessedBenglandBearnedBdreadBbittersweetBappetiteB	advantageBpaysBdiddyBsucksBmistakenBheedBgroupBchillingByou’llB	tremblingBstainedBskillsBalienBagonyB	worldwideBsupportBpuppetBpoppedB
permissionBpainfulBknotBjordanBhomeyBfreezingBfrankBcupsBtighterBsnatchBllBdomeBluxuryBincomprehensibleBglobeBborrowedB
attractionBwhoaohBmayneBmarksBcaveB21BwilliamBumbrellaBwrathBtipsB	sweetnessBhazyBthumbBrearBrealestBproBlightingBatlantaBwesternBwaltzBpitbullBogBloyalB	illusionsBhumanityBhellaBpitchBinkBcruisinBcollectBballoonBwouldaBcopeBthievesBfolkB	wednesdayBparanoidBopinionBmekBinternationalBhorrorBstaresBpatBetBeighteenBcopyBunwindBrodeoBhottestBjamaicaB
headlightsBdetroitBteachersBshtBbroadwayBswinginB
redemptionBrahBmustveB	abandonedBsincereBlionsBryanBcaneBsootheBprogressBnestB	christianBbarkBslangBforsakenBwatchesBstrollBsodaBoohoohBmmmmBbesidesBe40BdancesBsaluteBlemonBdocBcloverBtemperatureBbyebyeBalmightyBsurvivalBspiderBqBheightsBdriedBuptightB
invitationBgorgeousBbreastB	apologiesBpreferBpedalBohohohohBimagesBfoolinBdrawsByellingBupstairsBpumpinB	permanentB	paralyzedBgangstasBcrowBcravingBcheatinBsectionB	heartlessBmonstersBgrandmaBfeedsBdefBwrongsBsentenceBpowderB	naturallyBflagsBappealBscaleBreapBfellaBdesperationBwildestBuhuhBstudyBrumorsBreleasedBissuesBfreewayBdefenseB15BtaylorBseamsBhighsBflickBblossomBactionsBraBmortalBmaintainBjewelryBjapanBhunterBellaBcoasterB	temporaryBquietlyBfareB	difficultBarmorB	slaughterBpowerfulBleadersBhenryBstarinBsailorBragsBpavedBpainsBciaraBlandedBerasedBdaydreamBbidBbadlyBbackyardB	vengeanceB	surroundsBkongBgoddessBchimneyBthornsBservedBphotoBflippinBcupidBstrippedBshellsBrootBlanesBinviteBfrightBnaiveB	judgementBhoundBcasketBroyalBnickelBfearlessBdimesBbonnieBstarvingBrobinBnateB	forbiddenBwinkBthirteenBtestifyBspittinB	fantasticB	expensiveBdealerBdazeBcoloredBdivaBcusBclipsBmeadowBfootballBtreatsBpercentBmoonsBlordsBfeathersB
definitelyBcrimsonBboyzBaccountByou’veBrobbedBpoursBmouseBhummingBdisagreeBbledBtingB
silhouetteBpoBindependentBwombBsilentlyBgeniusBdrawingBwakesBtouchinBnutBopensBgravesBsliceBscreamedBsaviourBsavesB	reminisceBopeningBkeeperBjellyBhospitalB
dancefloorBvillageBscrewedBhandedBtonkBsiteB	confusingBsoilBshatterBribbonsBnotionBfranceB	entertainBconBalabamaBadByerBliquidBdannyBafarByayoBtoolBstrifeBmotelBgunnaBdenyingBsighsBrangBpurseBpriestBphonesBditchBacidBwastinBtokyoBrunwayBpacBhersBdignityBdespiseBquickerBdependsBcontactBrelationshipB	rearrangeBprovideBpowBmuddyBlinkBdollaBdepthsBlacBbulletproofBblindsBamorBwoodenBtuckBstickingBspendinBsatinBlimitsBdumpBdarkenedBleaningBjesseBivyBgetawayBcitysBchanginBaboardBwassupBtshirtBhornyB
crossroadsBconvinceBartistBwhatchuBversionBstirBsaddleBcookingBtonBskullB	deliciousBtypicalBmaidBlawyerBigniteBfirmBscarredBchemicalBaddressBslavesBruinedB	educationBcycleBnonBcomoBbeansB
absolutelyB
situationsBpatronBgloriaBfuryBdonâ€™tBcooBcapturedB25B	waterfallBstatusBhometownBhealsBhahahaBfordBcareerBbuttonsBbarbieBsurfinBstarryBpassinBkhaledBinvitedBhowlBcookieBchatBbetchaBtiresBofferingBmassesBmashBgroovyBgrindingBunrealBruinsBpuzzleBovBmelodiesBfascinationBpresentsBleagueBfamBsucceedBnooseB
muthafuckaBdemiseBconversationsBawesomeBtidesBsupplyBspeechBreturnedBpoetBjawBdigitalBcdBwiserBthursdayBsmithBofficialBlasBkinBgrantB	convincedBwaxB	thousandsBsparkleBrisinBgunitBcarterBnegativeBfaintBteeBpalaceBnewbornBlongestBcheckedBcarvedB	bethlehemB	repeatingBnowsBneatBknocksBjazzBflawlessBwantinBwalletBveinBtunesBspeedingBplanningBoutlawB	louisianaBfurBchuckB	challengeByeahhBsurvivedBobieBhabitsBbruiseBakBstrideBfloorsBbitsBbetrayBashBwinnersBwarriorsBtouchesBdraggedBrocB	overnightBcowardBchasedBbluntsBashantiBtrucksBrossBpopularBfrustrationB
discoveredBblurBamountBalotBnetB	graveyardBdon´tBawakenBrestingBhundredsB
disappearsBcountedBcheatedBsoakBsehBliftingBdongBdissBbirdmanBbedsBtattoosBrackB	passengerBi’dBfabulousBachesBtatBstylesBsauceBrepeatsBreindeerBeagerB	deceivingBtracesBstockB	brilliantBspotsBsosB	meanwhileBgroovinBspoilBpokeBplanetsBguestBwhaBusualBtreadBoerBmeekBfrostBcrapBconcentrateBclosestBbattlefieldBtailsBspainBrapsBmurdaBmartinBignoredBbitesB…BpetB	impressedB
hesitationBfeatBawwBanthemBtombBsurvivorBsupremeBstaredBspinsBremorseBlyricalBlingBjadedBhmBduskBcurbBbouncingBzipBundoBstripesB	pleasuresB	nighttimeBheeBbuyinBwanderedBvolumeBspyBslumberBmediaBdestinedB
breathlessBbelievinBaightByeahyeahBnearerBflossBstatesBprojectBgooB
confessionBchipBstealsBphonyBnuffBmadonnaBexpectedBangelesB99BthemeBsoarBsettledBlaysBguruBfistsBeggsBdesignerBdemandsBtrembleBridersBpayinBparkedBanyhowBwiB	terrifiedBswingsBrappingBproductBphillyBlingersBharvestBenginesBwagonBthugginBpharrellBpathsBjuryBjulietB	chemistryBtimingB
strugglingBhollyBrewardBopportunityBliningBinterestBevaBedenBcruisingBbrokenheartedB17B
straightenBquieroBquestBdoctorsB16BturntBsuckedBskysBoakBneighborBinstantBdeedsBchronicBbehaviorB
wildernessBsteerBsprungBoperatorBjobsBunspokenB	stretchedBrugBpumpedBnopeBgreenerBglovesBbtchB	stumblingBrateBhardcoreBgownBgitBfiendsBattachedBtwistaBpoliticsBpaneBjeffBfoeBcutieBconcernBcomparesBcommitBconcealBcautionBaveBwatBtroyBtravelinBjerseyBexploreBdivingBviewsBticketsBpigsBinfinityBcoughB
mysteriousBloopBfailsBboozeBsoundingBscarletBringinBnggaBhuntingBdameBcarolinaBansweredBit´sBfkBcomplainingBbunBbubblesBtrillBtodaysBschemesBriddleB
revelationBpineBmilkyBidiotBdoseBcausingBbrowBbrightlyBwhoahBvalleysBrevealedBpoutBpouredBhugginBhidinBchickensBbearsBactsBvibesBtorchBtheredBpourinBlocksBjiggaBwhippinBsnowmanBpawnBoxygenBmissesB
masqueradeBwhysBunholyBspiceBpossiblyBdealsBcaptureBadamBwiresBspittingBpartnersBnuthinBnowadaysBfoulBdutyBdisplayBcharmingBbragBawaitsBappearedB1stBsteveBslugsBphotographsBnaturesBliarsBinchesBexcitingBdixieBcatchinBundressBswiftBservingBrustyBrippingB	evolutionBcafeBbastardsBsantasBrockedBknightBhauntsBfreakingBduttyBdeeplyBdealinB2000BughBtowersBgambleBfulfillBchapterBbladesBunlockBunfairBsweatingBoppositeBfinerBemilyBdodoBcoolerBapproachBabsurdBvirginiaBvanityBthrowsBseventhBeuBbeggedB18BspeakinBnovemberB	marvelousBlordyBjaiBgroupieBfiddleB22BwhoresBtwinBkimBhitchBhellsBforsakeBchiefBthornBsubwayBstackedBrebornBdealtBbrassBstealinBpackingBmystikalBgiddyBentertainmentB‘emBuniteBreflectionsBlalaBhappilyBdrummerBcloselyBunbreakableBtwinkleBsignalBscriptBriffBpossessBdishesBuhohBriceBdangBcompeteBbrewB
travellingBreactB	limousineBdrunkenB	crumblingBbizarreBnananaB	carefullyBcampBabusedBwoohooB	southsideBrequestBminorBgoodsBeveninBdefeatedBwiredBvodkaBvacantBurgeBunseenBsofaBplainsBmotionsBmostlyBdeathsBcomptonBalertBshoresBridaBbrakesB911B
springtimeBplayboyBplatesBmurderedB
depressionBcombBtemptBtempoBstreamsBreplayBmessiahBgeneralBdanBdaisyBbronxBwitchaBtwerkBtaxBmathBmajestyBhehBdrillBronBproceedBpointingBkiteBhearinBdesignedBcamBahahBwristsBstankBshhBserenadeBislandsBgraspBeatsBwigBruledBnasB
directionsBcountsBprofitBpenniesBheartbreakerBeeB	christinaB	brightestBsundownB
playgroundBpilotBpersonalityBlowsBlimeBglideBeggBsugaBsockBrumbleBmournBdyouB
worthwhileBteddyBslowerBpleasantBofferedBkentuckyBhallwayBgospelBcrackinBcorrectBrunnerBreelingBmonBlimbBbootBballerBsurfB	strangestBsophisticatedBshakenBpadBnurseBhealthyBheadinBessenceBcarnivalBballingBtoolsBstunnaBsleevesBplagueBpartedBpalmsBnodBgarageBbayouBunbelievableBexchangeBdedicateB
contagiousBtipsyBskateBplantedBoverloadBmessagesBinspiredBforevermoreBexposedBchannelBcaringBbleedsBwoofBstressedBshuffleBoverseasBisraelBgossipBdressesBdeceivedBdadaBcommunicationBbizBsoakedBshimmyBretreatBremindedBnineteenBledgeBjustinBfreakinBbannerBbakeBpackinBchimeBbruhB
possessionBcomputerB	whistlingBreppinBpimpsBpettyBmastersBloosenBconsumeBanybodysBslightlyBlollipopBlimbsBlifelineBhomeboyB
footprintsBdubsBdoggoneBclocksBchairsBsweaterBschoolsBropesBritualBmusicalBlinedBheelBdressingBdollsBdeclareBanchorBvowsBvenomBtuckedBswornBstoveBsnitchBsharpayBsecureBroutineB	pricelessBmommasBdonnaBaccidentBvuBsofterBseveralBrodBmojoBmilB	intentionBflamingBdjsBweighBpostedBpoisonedBhovBgumBgoshBbinBx8BwillinBversaceB
rendezvousBannaBmaybachBmaxBinfiniteBfredBflawsBerBcollapseBaltarBachieveBtwasBtalentB	prettiestBmuthaBissueB
expressionBtireBsportBsadlyBordersBmistressBdelayBcentralBwindyBtightlyBlicenseBlesBlemonadeBgalaxyBcriticalBcoldestB	adventureBsneakingBpeasBnonstopBfrankieBexpectationsBdaytimeBcarolineBblazinBareaBliftsB	fairytaleBdeoBclawsBawaitingByellinBviceBupperBtommyBraininBniteBflowinBchiBbitingBwizBsoakingBsixtyBreelBphraseBcowboysBchamillionaireBcarouselBbouncinBarrowsBstallBsharkBmickeyBdidn’tBdaveBchooBbendingBassumeBzombieBwormBunawareBsoapBslackBribbonBpledgeB	manhattanBdubB
strawberryBrichardBlegacyBdeuceB
dedicationBcuttinBconfuseBwestsideBraggedBpoloBfreightBdrawersBconfessionsBchineseBbaitBamendsBtitleB	spiritualBsmarterBshapesBpeakBlogBimmortalBhoppedBhmmmBbeyonceBwhackB
ridiculousBremoveBextremeBescapingBeastsideBdippinBwe’llB	sanctuaryBlangBgutBetcB	committedBclingingBwaanB
speechlessB	remindingBjokerBhustlersBfloridaBflopBbenBwhyallBtherapyBsaltyBpraisesBmildBitchBgoonsBgainedBfryB
foundationBflickerB
compassionB	centuriesBunsaidBsuckinBspilledBslidingBskeetBitâ€™sBconsequenceBwhat’sBmoiBlootB	lifestyleBkanyeBhomicideBdsBcorpseBbudBwarmerBultimateBsurroundingBstripperBoopsB	necessaryBlethalBhoppinBdoggyBclutchBadvanceBstrictlyBshoneBsaBlistensBkansasBjsBhostBg5BfishingBferrariBfatesBfakinBvoteBtrustingBspacesBshirtsBrowdyBovaBlickingB
frustratedBflippedBdesperatelyBcreepsB
comprehendBbranchesBassholeB
adrenalineBwhatdBtattooedBsmallerBownedBoverdoseBnecklaceB
motivationBlightlyB	gatheringB	dimensionBdefyB	certainlyBapeBaliceBtallerBpeteBmodeBfeeBvanishBshowersBjerryBdaleBcheersBbraBrepentBraptureBoverdueBohoB	miserableBfileBbumpingBbumperBbrakeBwhoopBummBstorysBpipesBoblivionBkiddingBcircumstanceBwidowBtitsBthongBstiffBrockstarBmaleBhamBflockBcrawledBthumpBsufferedB	sensitiveBosBmysticBlogicBfleeB	depressedBswollenBspiralBraveBpinsBpinesBmariahBjoinedBgreaseBflashinB	childrensBbittyBunstoppableBsuggestBmadlyBinternetBtorturedBreflectBgottiBbostonBsuicidalBstumbledBreadsBprophetsBpapiBlandingBjuvenileB	headlinesBfriedBelevateB	crucifiedBcontainBbasicBtrynnaBsweatinBsirenB	shepherdsBreaperBjoyfulB	isolationBhosBdinahBcowardsB	celebrityBcapitalBcannonBwifeyBromanBpicksBphoenixBdancersBcockedBbettyBaiBretireBjadaBfoggyBcurveB	chemicalsBweakerBuniformBundertowBpatheticBoddBobsceneBentireB	enchantedBcosmicBbruisesB	blackenedBtribeBtormentBsmashedBshooterBshiversBpoliticiansBmasterpieceBkingstonBevermoreBcomplexBaweBanxietyBtrophyBswayingBprophetBpatchB	landslideBlalalalaBhaulB	criticizeBcoinBbluebirdBbenchBaddictBwehBsampleBrussianBomarionBlikelyBhairsBfailingBdootBchildsBabandonBwalaBtemptedBmobileBjunkieBjuniorBhumansBgravyBdearlyBarmedB2ndB
scratchingBrottingBreachesB	halloweenBgaveaB	forgivingB	expectingBdummyB	underwearBseizeBrioBposterBmatildaBliBlevelsBinsidesBhangoverB	franciscoBfergieBcestB
borderlineBwriterBpovertyBheightBhatsBendureBcrackingB	availableBtearinBstrawBspillingBscaresBoopBmoralBharryBexplanationBcrushingBboxesB	whateversBvalueBthickerBtemperBsolitaryB	overratedBmattressBlicksBgoalsBcrossesBconsumedB
cinderellaBbonBbeardBtisBsneakersBrandomBmurdererBjoeyBeltonBcandlelightBbgBakaBwinninBweedsBunusualBtootB	sparklingBrickyBporscheBpackageB	obviouslyBjeepBbrianByiBslutBpacksBjackieB	inbetweenBchuBcharlesBblushBwhaleBtowelB	suffocateBpradaBmugBiggyBfoesBdineB	deliveredB
definitionBconceiveBaglowBtradedBspunB
philosophyB	paparazziBmourningB
headphonesB	deceptionBboiBaxeBaudienceB1000BwoahohBtrialsB	someplaceBoooooBmissyB	fireworksB	fashionedBelectricityBdisturbBcurlBawaitBairplaneBabyssBvulturesBuniqueBsubjectB	spaceshipBmowBleaninBfloatinBblackoutBwoooBvivaBtreatingBrecoverBninetyBinfectedB	evrythingBdiaryBcrisisBchamberBaimingBthusBprollyBooohhhBlearntBfittedBcurrentBcreekBboltBsonnyBsavinBreignsBrecipeBkindlyBjewelBjawsBdotsBdarnBahhhhBtubBstickinBskillBpropertyB
particularBnuclearB	moonshineBmanageBinhaleBillestBgrieveBcrawlinBchBbrinkBaisleBx6Bx5BwageBunityBturkeyB	stockingsBjoysB	interludeBhireBdeclineBconsequencesBbananasBarrestBadmireB11BwhippingBversesBuuhBunsureBsoundedBsheetBrifleBpunksBpointedBchalkBcaravanBbuysBbluffBbangaBweekendsBtoxicBtoniteB	mysteriesBjbBbreakerBblondieBalibisBsuBslowingBsailorsBmarriageBhomelessBfrailBcriticsBchordsBaliBwreckingB
underwaterBtubeBstardustB44BvillainBturtleBstubbornBstitchBshoppinBshiftingBreachinBpunishBpotionBlimpBlacedBfrighteningBdriftedBdelicateBcontrollingBbuffaloBbuckleBbloodsBstoresBrockerBpoetsBownsBnonoBlobbyBeightyBdoomedBdallasBcoatsBwopBstairwayBpokerBmeltsBeraBdosB	countdownB	bartenderBanticipatingBtruthsBtimelessBswordsBspeakerBsoupBsafelyBrobotBlamborghiniBjudyBjokingBirishBflyerBbarefootBniggersBholidaysBdeservesBvidaB	treasuresBtaintedBsteakBicedBdukeBcustomBcolaBtentBricherBoperaBnggasBlightninB	influenceBhealedBdiscussBdamagedBbiggieBwilliamsBtidingsBrumourBrapedBfencesBdicksBcuddleBbunnyBbritneyBwakinBsyrupBshockedBservinBscrapeBgatsBexoticByouâ€™reByearnBwhippedBslappedBroyceBnooBlaserBcoachBtriceB
regardlessBooohhBmonkeysBidentityBcounterBcockyBburdensBapplyBslugBskylineBpumpsBpeachesBlilyB
bitternessBbbBalikeBwreckedB
vibrationsBskinsBrowsBhikeBdomBcostsBsupernaturalBrosieBreminderBperBoooooohBnicoleBmothafuckinBmannersBleashB	intuitionBinspireB
impressionBharborBfrogBfishinBdroughtBcurvesBrippinBrainedBninaBmatteredBleakBguardianB
earthquakeB	deliriousBawfullyBadoredBsundaysBspringsBshoutsBsellsBracesB	possessedBheroinBconsolationBcapeBcanvasBanewBsupperBsuedeBsublimeBsignalsBshapedBscatterBravenBoyeBloyaltyBjudasBflingBeaglesBdutchBcheddarBbowsBbiB	alligatorBwhoveBtrampBshovelBsermonBmosesBmethodBknotsBgramsBfunctionBchoosingBcabinB	universalBunableBstreakBshooB
sacrificedBnalBmothaB
hopelesslyBdistractionBdirectBaloudBwaitressBtrumpetsBtrainedBsecretlyBrulerBmahBlearninBknucklesBgroundsBfrostyBditBdehBdeedBcozyBwhirlBretardedB	remembersBnobleBinvitingBhappierBclosesBbriefBaugustBvogueBsuckaBstabbedBsheriffBrefusedBpossibilitiesBmarbleBcableB
reflectingBpitifulBmarysBkhalifaBhummerB	hopefullyB	gabriellaB	conqueredBcattleB	boomerangByeahhhBwatchaBsarahBladysBignorantBhypnoticBgleamBbreezyBtwitterBsystemsB
restaurantBprocessBofficerBjanetBfaultsBbelongedBbeachesBbaseballBbaeBawkwardBannBtastyB	strongestBsplashBpotatoBoptionsBidleBhoorayB
determinedBcurlyBballersBapproachingB80BreminiscingB
neighboursBmemberBmafiaBlegalBhennyBdontchaBcasesBbustingByachtBwivesBsweetieBpossibilityBnonsenseBfloatsBdevourB	consciousBbatheBawokeBanxiousBstationsBperformBmerelyBguessedBgreasyBformsBbambooBtroughBtaskBruggedBqualityBpeachBmereBisleBinstinctBimaginedBhandfulBgymBglistenBcrucifyBcentsB
armageddonBaccusedBtygaB
satellitesB	riversideB	jerusalemBflightsBdollyBdockBdammitBcurlsBbeliefsBbasketBsortaBprogramBpoliteBpeepsBmaydayB	marijuanaBglowsBgiantsBfamiliesBdadsBbussBbackingB21stB	wastelandBtrendBtermsBtagsBspellsBresponsibleBpinchBlovesickBfourteenBfortressBfireballB	dependingBcynicalBcleaningBbatteredBandyB13BvirtueBtrickyBsaddestB	revealingB	protectedBnationalBforkBclawBboundsBbongBbetrayalBarizonaBamnesiaBunpredictableBtvsBtechBstakesBsplendorBsniffBskrrtBsighingBscarfaceBreturnsB	prisonersBpmBpalBovenBlimboBinventedB	hypnotizeBhustlaBexplodesBdoopBdejaBdefensesBchapelBcardiBbathingB808BtrayBtikBswerveBsuffocatingBspoiledBseldomBschemingBpoppaBpassionsBparanoiaBnativeBlucilleBlegendsBdungeonBdeceitBcripBchristmasesBbuzzingB	authorityBapologyBabideBtabB	switchingBsweatyBstutterBsoleBslitBrooftopBrailsBopinionsBjollyBjagBgleamingBfleetingBcarveBbalconyBwipedBvaBstableBslidinBshinedBscreensBprovedB	politicalBparaBmaniacBmakebelieveBhennessyBguapBgazingBdovesBchordByuletideBskippingBrepeatedB	quicksandBpatternB	otherwiseBhighwaysBgrimBfavorsBdrivewayBcheBbroomBlongedBlastingBivoryBhugsBhooksBenteredBearthlyBdisappearingBdepthBbottomsBbaggageBbaconB	backstageBwailBuntouchableBthrillerBstatueBremoteBpleasingBoathBmidstBmentallyBmarilynBlickinBillegalBframesBcookedBcanyonBbrightenBsuiteBslyBrelieveB
punishmentB	pressuresB	preachersBlifelessBjazzyBinnBhustlingBhearseBgoodiesBgamblerBframedB	fantasizeBdragginBdeanB	countlessBchargedBbullyBbleekBvertigoBtwinsBtidalBshoopBpistolsBladBgearsBedBchillyBcaddyBberryBbendedB	attractedBronnieBreportBprintBoptionBmtvBminkBjaggerBdifferentlyBcomparedBchokedBangieB
suspiciousB
presidentsBoklahomaB
ohohohohohBobjectBnecksB
motorcycleBlamboBjetsBintentBglareBcomBbreathesBaiyyoBunbrokenBstuntingBshortsBquoteBquestioningBpicketB
medicationBjunkiesBjoanB	honeymoonBfinBdrainedBdianaBdealersBcausesBarrestedBvampireBstackinBoverheadBjointsBjoBflauntBexistsBdoorstepBbugsBbruceBsharksBrimBpunB	pretenderBpiesBoctoberB	nashvilleBjoleneBexampleBdistressBclarityBbraceBbeggarB	vibrationBrooftopsBpinkyBpetalsBperspectiveBnormallyBmaidenBhoneysBdishBbearingBteardropBstuffedBscoopBrollieB
passionateBownerBgorillaBfreelyBeatenBcousinsBcommunicateBcansB	bloodshotBairportBtryinaB	travelledBsolarBsmokesB	potentialBlolaBfilthBbelovedBwillyBvastBstressinBprevailBninesBmultiplyBmeantimeBlauraB	jailhouseBglocksBextraordinaryBelephantBcrowsBbsBatticBanticipationBteenBstampBslainBsceneryBroaringBrepairBmoonlitBmagnetBlawyersBcreatorBcharityB
weightlessBtenseBsendinBrudolphBrealmBplayasBpartingBohhhhhBminusBlegitBfeBdragsBdonkeyBchingBchelseaBchartsBbetsB3rdBuntoldBsubtleBstriveBsmellingB	preachingBnamelessBmythBmeaninglessBmasBlabelsBhe’sBflexinBcondoBblamedBamberBxoBversoBrenegadeBracinBpsBmarkedBindigoB	imaginaryBhawkBfondBfacelessBdeskBdaggerBcottageBclackBchariotB	bluebirdsBblendB500BwizardB
twentyfiveB
substituteBrhondaBportraitBmottoBjulianBheadacheBeffortBswitchedBsoothingBslightBnailedBlumpBkevinBincBfuBdodgeByknowBrobertBprofessionalBpleadingBpearlyBjenniferBhoodieB	damnationBcueB
collectingB	championsBbrothaBbreezesB	boardwalkBassesB	amsterdamBwavedBviolinsBsurgeryBstanceBscottBpeepinBneedinBmeterBidolBfreedBburstingB	bubblegumBbowedBboughBafricanBtunedBtoiletBstackingBseemonBsandyBreliveBramblinBpropsBfrontingBcainB	believersB	suspicionBsodBportBphatBmatchesBhawaiiBgroveBgotchaB	gangstersBdisappointedBcrippledBcreatingBsportsBgoatBeddieBechoingBdabBchadBbelleBbatterBadiosBwitchesBwildinB
technologyBruffBnãoBnightingaleBmenaceBmartyrBmangerBjulieBhoopBhasteBchoppersBchanelBatlBapplesBtoteB
tambourineBsinkinBransomBpirateBpickupB
persuasionB	penthouseBpencilBoffersBhollerinB	glamorousBdolphinB	clevelandB	certifiedBcemeteryB	ambulanceBvieBtrailsBteachingBstevieBstarveBsoaringBshepherdBnanananaBlightersBinsistB	impatientBherbBfuckersBfarmerBexposeB	exceptionBeclipseBbeliveBbasedB	accordingB123BworryingBwormsB	thereforeBsewBsettinBneolBmoonglowB	hourglassBhealerBfreedomsBdcBcussB	confidentBcancelBbarnBallrightB	aftermathBactressBvocêBunlikeBspreeBproclaimBomBnotchBlaborBjapaneseBjadakissBirresistibleBencoreB
commitmentBcomaBcliffBclassyBsparrowBpressingBnothinsBhorizonsBhaffiBgamblingBformerB	explodingBclydeBcellsB80sBtintedBthBresurrectionBrejectBpiperBparBneoBmothBindependenceBflippingBeveningsBdescendB
cannonballB23B14Bx7BswanBskipsB	skeletonsBrailBludacrisBlocaBevrybodyB
controlledBcirclingBcan´tBanneBactorByapBwreckageBwashesBvanillaBserpentBmesBmercuryBhymnBhopefulBcuckooBbordersBblindlyByou´reBticktockBtemptingBsingersBricoBrazorsBpyramidBpreludeBpeekBfearedBedgesBdohBdetailsB
collectionBclaimedBbackboneBéBtroopsBsessionBpatternsBmuseB
mistreatedBjumpsBinclinedBilBglobalBembersBdylanB
valentinesBtangoBsunkBskiB	senselessBrambleBneverendingBmeowBmassiveBlindaBlengthBjockB	greatnessBgrannyBasianBapathyBwarrenB	uncertainBtekBsomewayBshootsBseraBroosterB	nevermindBintenseBduringBdisorderBcombinationBblastinB
apocalypseB
twentyfourBtennisBtastingB	seriouslyBpuertoBmuleBgrievingBformedB	dreamlandB
downstairsB	demandingBcastingBbarrenB	afterlifeBwalkerB	tightropeBsuburbanB	separatedBquartersBpuppyBluciferB
introducedBfarmersB
destroyingBdenimBcontrolsBwhiskyBwardB	they’reBswineB	sidewalksBribsBrelationshipsBquandoBpizzaBpimpingBnaaB
mesmerizedBgenuineBgenieBflashesBdriversBdepartedB95BsymbolB	suspendedBspidersBservantB	overdriveB	legendaryBhammersBflipmodeBfeaturesBfeatureBcordBclaimingB	blueberryBbellaBbashBballoonsBbadgeBbackedBactedBabsenceB101ByankeeBunconditionalBthineBswimminBsoughtBsatansBpregnantBpornB	loneliestBloanBkillasBjamminBhoundsBgenocideBdigitsB	condemnedB	afterglowBslaveryBscumB	recogniseBreceivedBignoringBforeheadBfishesBcontractBcigarBblondB4thBtheirsBtextingBshittyBresolveBremovedB	pollutionBpeggyBmhmB
melancholyB
boundariesBwritinBsnowsBripeBresponsibilityBorderedBlaundryBjanuaryBhavanaBeuropeanB	crocodileBbugginBbendsBtequilaBsimmerBmountBmilitaryBguardsBgogoBglamourBdeerBcrewsBcolouredBchainzBauldByeaahBreverieBoomBmopBmontanaBgroupiesBgloomyBdominoBcompassBclearsBbuzzinBbernieB	australiaBaidBunafraidB
superstarsBsownBrubbingB	rejectionBrejectedB	reflectedBpamBmirageBkgBholBdrawerBcorruptBclearedBchoirsB	basicallyBarrivesBwaltzingBwaiterBteasingBsantanaBratchetBooooooBjubileeBgoodyBfoursBfledBfeedbackBcuffBcircumstancesBcapsBboilingBbillieBauntBamazeByungBwenBunaBtreasonBrollercoasterB	relationsBlamesBhomeworkBhillsideBformingBestateBdietB	blacknessB	appealingBweptBthighBslashBsimonBshacklesBprowlBpaintsBoiBlexusB	justifiedBfortunesBdecentBcementBbodiedBbleakBartistsBtimBspikeBskeletonB	scratchedBrocketsBpotsBdriftinBcookiesBchoppedBbrandyBbeastsBattemptBaddingB19B
yourselvesBtunechiB	touchdownBstitchesBruthlessB
rearrangedBmiceBkneelingBfieryBdesertedBcatchesB
artificialBwitheredB
voulezvousBvietnamBtimerBsneakinBohioBlocBfetishBerrorB	corporateBcongratulationsBbraidsBbittenBbeersBwelfareBtamedBrarelyBmaamB
lighthouseBjasonBgunshotBgreensBcoupB
conclusionBcloneBcitizenB	cardboardBberlinBassuredBagreedBvanishedB	religiousBprophecyBoldenBminiBlearnsBlargerBjudeBhallowedB
faithfullyB
excitementB	disguisedBdeltaBcrutchBblaBalightBvirusBtypesB
submissionBmarvinBlastedBhomewardB
corruptionBchildishBbuildinB	breakawayBbleedinBbarkingBaaahB
woahohohohBvocalsBvacancyB
supersonicBsuckasBpasB
outrageousBoutfitB	operationBfriscoB	fortunateBestaBcoolinBasapB
ammunitionByumBtsBtickinBservesBmilliBexileBdefinedBcoppedBcanadaBversusBtestingBolaBmileyB	faithlessBdreamyBdelBconsciousnessBclaimsB	blindnessB
washingtonBtwistinBskinnedBshaveBramBpoolsBmusclesBmidwestBmexicanBjoiningBjohnsonBi´veBgaugeBflockaBflirtingBeightiesB
disgustingB
disconnectBdieselBdepartBcubanBbicycleBbailaB	avalancheBvocalB
unexpectedB	stillnessBshoutedBparumBmicsBgraciousB
glisteningBfaggotBdevaB
convictionBcaptiveBbugattiBbiaBatomicB	womanizerBunitBtrippedBtradingB	tombstoneBposseB	oohoohoohBjakeBhintB
helicopterBfckingBdissolveBcologneBcavingBcagedBwretchedBwandererBtwosBtrinaBsteepleBroamingBnighBmatBjourneysBcivilBblurryB	autographBarmiesBalterBumaBthriveBtarBscrubB
populationB
hesitatingBfilterBconfideBclBbrazilBahhaB	undressedBtriumphBticBthumbsB
snowflakesBsilhouettesBrolexBrisenBpuffinBonwardBmeltedBlyBlunaticBlatinBjrBgrandpaBglittersBfrontierBfluidB	explosionBcorrinaBcodesBcindyBcelebratingBbaldBamyBtweetBtossingBthrottleBstompinBsteepBsmokeyB	sincerelyBradioactiveB	privilegeBpreBpaybackBooooooohBonewayBmightveBjiggyBhunBhogBgurlBgrudgeBgraffitiBgermanBfortBfabolousBeyelidsB	elephantsBcrookBcartoonB8xBwoolBwoogieBwiderBwhitesBwe’veBsusieBsuperstitiousBstrikingBrosyBrevolverBlotteryBloosingBkrayzieBgrabbingBdippedBblamingBadriftBacresBwritesBtokenBtapesB	strollingBskirtsBscopeBrushedBnappyBmarleyBjudgingBguidanceBdatesBdampBcoveringBceptBavecBundeadBtemptationsBsurrealB	strippersBstingsBrhythmsBpepperBmessyBliberateB
interstateB	daydreamsBcrisBclothBchumpBcallerBbritishBanarchyBwellsBunfoldsBsilveryBscornBridgeBmindedBmaisBinfatuationBhutBgapBdanielBcribsBchewingBchantBallsB
allegianceB200BwraithBtickleB	seductionBryeBrumoursBpredictableBpotatoesB	positionsBpillowsBmaeBluckiestBlongsBjocBgollyBflushBfloodsB
disappointBdeucesBdearestBcomplicationsBcluesBblanketsBunoBtrumpetBtrousersBswitchinBsonicBsigningBreeferBqueueBoutrunBonehorseBmoaningBharB	frequencyB	communityBbottledBblacksBbeastieBbathedBbabaBaaaahBvideosBsassyBrouletteBripsB
relentlessBrailwayBpredictB	penetrateB	parachuteBobamaB	imaginingBidealBgrenadeBdismayBdecidesBdaybreakBchartBchampBbrutalBafloatBwealthyBtravelsBswamBstirringBstinkBpromBpoemBnikeBneglectBmuthafuckasBlutherB
interestedBinfantBforestsBescapedBeffectsBcontemplateB
conspiracyBupgradeB
swallowingB	stressingBspreadsBsadieB
resolutionBraidBlidBjdB	infectionBexciteBdragonsBdnaBdismissBcrashesBachinBzionBtowBtossinBsuperiorBrearviewBraahBparallelBoutlawsBmixtapeBmindlessBmilianBmichelleBmaggieBloungeBleoBkiB
indecisionBhookerBgardensBfluBcoconutBclimbinBbouquetBbatteryBbanditBvousBtellyB	substanceBserenityBroyaltyBpossessionsBoutaBnearestBlooBhugeBharkBgramBflawBfillinBduckinB	distortedBdisappointmentB	companionBsweepingBrubbinBrescuedBpostcardBnyBmealsBlightenBkitBjackedBhuBhowlinB
complicateBcanâ€™tBburnerBbeingsBalleluiaB	aeroplaneBurbanBsuperficialB
soundtrackBnoisesBmatesB	landscapeBdebateBcrownedBavoidingBwadeBvroomBvaluesBsickerBrefugeB	rebellionB	overboardBflashyBfebruaryBchefBcharadeB
basketballBallahBtrickinBtattedBslanginBsimilarBscandalBsaferBpunchesBprescriptionBpeculiarBpatrolBoccasionBjacksBintendB	imitationB	disbeliefBdandyB	concealedBclumsyBcarefreeBbuddiesBboilBbmBblastingB	baltimoreBalasByrBwannabeB	surprisesB
stretchingBskelterBseverBplowBhelterBfixingB	democracyBbreastsBangleBactorsBablazeB3dBwhoaohohBswangBrewriteBnumberedBkeenBimpalaB	galvestonBg6BfraudB	fireplaceBelasticBeasternBderangedB	chevroletBcagesBbloomingBagentB24sB	traditionBtemperaturesBstungBsavorBpursuitBmaximumB	lullabiesBlovelessBitalianBinsecuritiesBhottieBhoboBhareB	grapevineBfriesBearthsB	curiosityBcropBbondsBbeyoncéBamericasB60BwakaBtropicalBtobaccoBthreesBtellemBshowdownBshootersBpierceBpacingBhastaBgolfB	firefliesBdeservedBclashBbloodedB
bewilderedBbettingBbakerBvictoriaBturfBstarringBrobberyBrebelsBlewisBlarryBkittenBeventBcuntBcreptBconflictBbeggarsBanguishBwetterBthreadsBstandardBsinksBrealerBraisinBpursueBpricesBpomBplantsBmummyBlouiseBlockingBjuliaBheatherBhappiestB	awakeningBarcadeBwheatBweepsBvileBtougherBtonsBthreatsBstrapsBspadesB	shiveringBshakyB
perceptionBmendingBkonBjerichoBhighlyBhankBfruitsBfellowsBdripsB	dashboardBcasualBcapableBbargainBbadoomBworkersBwendyBweathersBtaupinBshorterBsemiBsandwichBsailinBrosemaryBreboundBrankBquitsBoldestBminimumBmerrygoroundBlinenBlincolnB	limelightB
liberationBflossinB
descendingBcurledBbrotherhoodBarrivalBantoneByoreBwritingsBuziBtodoBtatteredBswirlingBsunsetsBspendsBrehabBpurpBpintBladenBkobeBkeithBjohnnysBhorribleBhippyBgoodieB	embracingBdivorceB	collisionBbeanByowBweighingB
vulnerableBtrailerBstrobeBslingBsalesBrevealsBremainedBpolishBoverwhelmingBoverwhelmedB
mothafuckaBlossesBjacobBherdBgenerationsBexhaleBembarrassedBdesertsBdadadaBcowsB
camouflageBattractBabilityBtwitchBteasinB	survivingBstudentBstadiumBsmackedBrumorBreverendBreckonBorbitBneighbourhoodBhotelsBgulfBequalsBearlBconceptB	clutchingBboastBanothersBaffairsByuBwaywardBunbornBtimberBswitchesBsurvivesBsomedaysBslumsBseduceBscalesBredeemBquiBparkinBmintBlobsterBlensBlavaBgravelBfetchBeroticBemmanuelBdebtsBcubeBbeadsBauB64BvioletBunleashBuhhhBtrappinBshockingBrushinBreloadB	recordingBpresidentialBpondBphotosBkrazyBjudgesB
illuminateBhymnsBhashBglossBeducatedBdownfallBcrooksBcloakBbredBbranchBblizzardBbaptizedBallenBalarmsByawningBwutB	thrillingBsmashingBshhhBrespondBqueerBquakeBpsychicB
protectingBpopcornBmagneticBlosesBjukeBhenBgrittyBgrabsBgiftedBfroBdougieBcommaBanalyzeBagonnaBa1BtellerBsleazyBshrinkBshinningBshackledBsewnB	respectedBpurityB
productionBprivacyBoffendBnavyBlousyBjingBjamieBfearingBeasterB	crossfireBconfettiBtwirlBtumblinBswizzB
successfulBreinsBpopeBphilBpartyingB	mentalityBlegionsB
lalalalalaBintactBheatedBgripsBcreoleBchaserBbrookB
birminghamBbandanaBamazinBverbalBunheardBradiosBpolkaB	pointlessBowwBmansionsBmagnificentBkardinalBjeezyBdangersBconcertBclingsBclearingBboredomBarcB90ByieldBwitherBwarmsB	visualizeBtigersB	superheroBsposedBsaraBrougeBrhyminBrepayBouBjessicaB
individualBhandyBgullyBguidedB	gratitudeBfretBforgetsBfiringBdisconnectedBblurredBblackestBwilderBwhistlesBwavinBwarmingB	untouchedBspitsBrustedBroBramonaB	primitiveBpedestalBonlineBnotherBnagBmeadowsBmannieBkimiBintoxicatedBfinesseBeventsBduhBdresserBdaisiesB	consumingB	caressingBbikiniBbeaconB
aggressionBwhensBvergeBtresBsuspectBstrollinBrungBrobeBrestoreBreggaeBproudlyBpahBoverflowBoficialB	notoriousBnearnessBmixingBlickedBidiotsB
hurricanesBgingerBgiBfonteBfollowinBfmB	executionBdodgingBdevotedBdawnsBcréditoB	christineBbizzyBbanjoBbakingBatlanticBarchBalignBveBunderestimateBswapBswallowsB	survivorsB	supernovaB
separationBpornoBpoemsBpinnedBoverflowingBnosBnickelsBmoralsB	moonbeamsBmonicaBmatchingBlowlyBlackingBinfernoBhonB	greyhoundBgladnessBgiverBfridgeBfoldedBfiresideBfalterB	everythinB
disciplineBdesBcopperBchessBassassinBahhhhhByummyBwoesBvividB
undefeatedBtakerBsmelledBslipperyB	porcelainBmustaBmissouriBmerrilyBmedalBmalibuB	freestyleBfemalesBendingsBcolorfulBcleanedBchatterBcasualtyBbuildsBbolderBblinkingB	airplanesB	whirlwindBswampBsurferB	shelteredBsaviorsBrewearBreupBpiningBnunBnikesBmonyBlowdownBkoB	integrityBimproveBhumminBheadlineBgrandmasBdododoB	disregardB
discussionBdemoBcrumbsBbuddhaBbowlingBbarbaraBahahahB5thBuphillBunhBsunkenB
scandalousBsaladBracistBpiercingBpeeBoutcastBoffendedBmustardBmurdersBlurkingB	instinctsBganjaBembracedBdaredBcreedBcrashinBchoppinBbourbonBacesB69ByoyoBwilsonBukBtransmissionBtchuBtaxesBswearsBsuburbsB	strugglesBrushesBrbBperoBpacificB	newspaperBmonroeBlorraineBlimoBlambsBfresherBdrowninBdreadsB	doesn’tBcreepyB	continuesBcoleBchimesBbushesBabsoluteBweighsBviolinBquiverBleveeBlabBitalyBindiansBhostageBhomemadeBfingerprintsB
explainingBemberBdeleteBcultBcombineBcloutBchoppaB
chandelierBbrawlBblockedBbacardiBtrenchesBsteeringBservantsBrobbinBreelinBpriestsB
phenomenonBnightlyBmadmanBleaseBkooBjelloBhuntedBdetourBcontemplatingB
consideredBconfrontationB	collectedBchicaBbadderBabcBwhoooBvitalBtrimBtoilBtissueBspliffBshBrubbleBriB	pretendedBmurphyB	movementsBichBhelpinB	graduatedB	fragmentsBforgaveBfelizBfamineBepisodeBenjoyedBdeviceBbrimB
boyfriendsBbingBbatsB	astronautB
victoriousBtheoryBsummonBstewBprovesBoprahBnovaBmorgueBishBclothingBcinnamonBchicoBcheaperBangelinaBwrestleBvsBthrowedBsweetheartsBsmellinBskrrBseveredBscrapBreservationBreflectsBpunchedBmeanestBkonvictB	josephineBjamsB	instagramBinaBimpactBhustlasBhonourB
heartbeatsBhallwaysBfoamBducksBdrapedB
delightfulBcrawlsBcoincidenceBcatastropheBcasinoBbubbaBashoreB2pacBwondrousBtappingBstatuesBstalkingBstaleBsolaceBslammedBshredsBsenoritaBsambaBproperlyB	powerlessBpeersBpapasBnunnaBmillerBmensBlizaBlavishBkeroseneBironyBheaterBgroceryBgeuBfingernailsBfendiBfavourB	characterBcakesBbeverlyBballroomBainBvagueBusinB	twentyoneBtrebleBsuzyBstoolBstairBsprayedB	solitaireBsloppyBramblingBnicerBlonerBidealsBhumorBhobbyBhiredBgoonBfightersBentwinedBcrueltyBcombatBcarolsBboppinBanglesBaddedBwhatllBwaterlooBvacuumBtriggersBtreatinB	travellinBtauntBsinisterBripplesBradiantBpraysBpassportB	neglectedB
motherfuckBmayorBlyricBliverBknuckleBknightsBhoverBhideawayBhatefulBglenBgangsBfindinB
desolationBcinemaBceBbratB	blasphemyBbikesBbenefitBzevonBtopicBtingleBtikiB
sunglassesBstreetlightsBspillsBsinnedBshittinBquenchBpenisBparoleBninjaBmustangBkateBjiggleBintelligenceBguidesBgloriesBdetailBcutestBconfinedBclosinBclicheBcleanseBclappingBcellarBbwoyBblowedBbasicsBaliciaBaffectBwildfireBtalkerBswarmBsquaresBslateBsereneBredeemedBposedBplatformBnudeBmundoBmanicBleanedBlakesBignitionBhysteriaBhomeboysBhavocBhaeBflavorsB	daffodilsBcoochieBcometBcoloradoBchurchesBcdsBbitchinBairwavesBwhitneyBtugB	speciallyBshrineBrighteousnessB	receivingBpeanutBnittyBnadaBlarkBhuggingBhoppingBharshBgigBgazedBgagaBfierceBcontestBattacksBantidoteBuntillBtrainingBtestedBterriblyBstenchBslowsBskullsBskippedBshabbaB	scratchesBredneckBrattlesnakeBprairieBpilgrimB	perpetualBpatterBorganBmulaBmondaysBmanagerBkaneBjillBjadeB	hypocrisyBheckBglancesBghostlyBgdBfrictionB	flashbackBdelusionB
complimentBcoinsB
christiansBchokinBbrushedBbragginBadjustBwhilstBvoyageBviolateB	vanishingBthomasBsuccumbBstoopB	standardsB	solutionsBsmallestBslidesBporkB	performedBnicotineBnavidadBnancyBmoaninBmaineBladsBkaratsB	handcuffsBgroovingBforeversB
flickeringBericBelusiveBdiddleyBcrazeB	commotionBcheetahBbuhBauntieBaimedBwishfulB
unfaithfulBtranslationBshroudBresponseBresetBrecognitionBphreshBmagnoliaBláB
locomotionBlayersBinheritBgluedBfundsBentertainingBehhBdevastationBdescendsB
couldn’tBboardsBbeatlesBawakenedBalbumsBaddsB	accidentsB20sBtightenB
sympathizeBsuzieBsuperstitionBspreadinBsillBrolesBprovenB
propagandaB
politicianBooooooooBneedyBmarthaBmamboBlockerBkingdomsBinterventionBinterestingBintelligentBheraldBhahahahaBgrabbinBfloatedBfarawayBesteemBescaladeBdrearyBdaresB	bewitchedBbeguineBbeckyBadieuByouseBwitchuBspeciesBsooBsheddingBsankBsadderBreadinBratedBprickBnoooBmutualBmascaraBincenseBhoseBholaBhackBgongBglidingBfeebleBdolphinsBdiggaBdelilahBcraftBconsoleBcocoaBclassesBcaptainsBblossomsB
attractiveBatomBapesBalfieBtrapsBtonesBswishBrariB	poisonousBorphanBoasisBmoscowBmoodsBmidBlandlordB	interruptBgroundedBgrailBgirlieBforcingBfailuresBemceesB
distractedBconeBcaineBbreathedBacceptedBvinesBtwistsBtrendsBtntBtippinBthatdB	territoryBsequelB	petrifiedBinvadeBhoodsBheapBgayeBfernandoBdramaticBcursingBcorpsesBconnectionsB
conditionsB
commercialBceilingsBcassidyBbookedBbeanieBbarbedB88BvoltageBvanessaBtummyBtroublemakerBsettlingBroyBprotestBplottinBowlB	outskirtsBouncesBnothingnessB
ninetynineBlimitedBlayzieB
kryptoniteBintendedBidolsBflicksBfbiB	exhaustedBexactBdooooBdeceasedBcocaBcloudedBchargesBcarolBbudgetB	answeringBaidsB0B	unleashedBtumbledBteensBslammingBskatingBshaftBpolesBperishBpeelingB
occupationBnaegaBmareBlungBkuruptBkidnapBjupiterBizBirelandBhecticBfloodedBflirtinBextremesB	evergreenBescapesB	eliminateBdrenchedBcursesBcrackerBcongregationBcigarsB	cadillacsBbreakthroughBbabesBasylumBaccentBvomitBtuesdaysBsteadilyBstaggerBsinfulBricochetBmossBmembersBimmuneBhulaBgrippinBcoalsBcentreBbipolarB
beginningsBamusedBaliensBa5BzzByelledBweighedBvetBtriggaBthisllBsyneBsupaB	staircaseBshrimpBrogerBritaB	policemanBneverthelessBmuteBmoeB	misplacedBmingleBjugBivB	hypocriteBgrinningBgrapesBgorillasBdivisionBdinnersBdawgsBbumpyB
backstreetBautoBanywaysBaffectedB
wouldn’tBunravelB	strugglinBstagesB	squeezingB	splittingBsmotherBskylarkBrabbitsBpopoBpassageBnoonesBmarchinBlocationBkeepersBjuanBintimateBheaveBhanBgroomBegyptBcannibalBbumsBbrosB	billboardBairsByou’dBvillainsBurgencyBtupacBsumthinBslimeBroadsideBrequireBremyBpostmanBoysterBmichiganBlooraB	furnitureBfreddieBfastestBentranceBdinosaurBcounterfeitBclanBbarroomBbabygirlByeaaahBweavingBwanaBtornadoBsusanBporterBoohhhB
millenniumBhippieBgrammyBfeelinsBfannyBduranBdublinBcrumblesBcourtesyB	classroomBcherriesBbreakoutBbraceletBarabBveteranBuncertaintyBtuffBtreyBthrilledBthinnerBpollyBpleasesBnurseryB	messengerBmediocreBmashedBladdersBindifferenceBfizzBelbowBdoubtedBcuffsBcleanerBbossesBbegsB	anonymousB6xByonByakBwussupBswervingB	slightestBsharpenB
regrettingBrebirthBpolicyBparasiteBnecessarilyBnapBmemorizeBmagikBjoesBi’maB	inventionBglamB
flashlightBfittingBdunnoBdaeBdadeBwhiningBvirginsBvintageBuhnBtyrantBsweatsB
subliminalBsteadBpurestBmagnumBgalsB	exclusiveBdomainBdidditBdebrisBcreatesB	composureBchequeBcarvingBbatmanBawardBarrogantB	arroganceBanyplaceBalleysBxxxoBunlockedB
triumphantBsymptomsBswellingB	statementBsipsBshaltBserpentsBseamBroastBprovingBpiercedBparkerB	obliviousBmisleadBmeditateBmatrixBmasksBhathBelbowsB	deafeningBcrunchBcommasBcomedyB	cathedralBcarriageBburrBbumpedBbluerBazBanyonesBallergicB1999BvirtualB	tangerineB	struggledBstashedBsouvenirBsocalledBshirleyBsandmanBroachesBradiateBpumpkinBpissinBpabloBnayBmjBlotionBlikingBleisureBindianaBicingBhoveringBheyoBfickleBeskimoBenslavedBdreadfulBdoodleB
disturbingBcodeineBclaiminBbrandedBbindsBarguingBadrockB5xB26ByardsBwritersBwattsBwaeBtruckinB	transformBstewartBsixthBscheduleB
recognizedB
oppressionBmotherfuckaBmmmmmBkiddinBgamblinBforeplayBfireflyBfiestaBexerciseBengineerB	conceivedBcavesBcautiousBcartBbutcherBboominBbmiBbideBarrangeBaaB	uninvitedBtippyBtangleBscheminBrecordedBpubBpolishedBoutcomeBlingerieBkaleidoscopeBkaBitchingBiconBgainingBfrankensteinBflossyBehehBdissinBdeliveryBdanglingBcontradictionBcomebackBbaileyBargumentB305BtruBtipdrillBtiltBspiesBsolemnBsayonaraBrhymingBproducerBpigeonBperformanceBoutsBnoahB
meditationBmeasuredBjamaicanBironicBinvestBinsightBinhibitionsBinfamousBhtownBheeyBfranklinBfistfulBfalloutBeliteBdyeBdraftBdefenselessBdaydreamingBcrippleBconspireBcocoBcanyonsBcaBboostBblouseBarmourBarkB247BzimboBzerosByeeBxrayBwhamBvuittonBuneasyBtingsBsweetsBswaggBsolvedBshoutinBseventyBredsBplainlyBpeppersBpendulumB
nananananaBlexBjudgedBjoyousBharpBgrandeBgraduateBfumbleBfilesBemeraldBelementsBdoubtingBdispositionBchoursBcashedB	beethovenBbachelorBapproveBammoB38BzipperBtraitorBsteamingBslumBsharesBrippleBpusherB	promisingBpinballBpierB
physicallyBpartyinBmoansBmagicianBlukeBlestBlanternBhydeBguttaBfagBevilsBdelayedBdavisBcreativeB	corridorsBcoolestB
complaininBcoBbubBbitinBbentleysB
automobileBunstableBunclearBtreadingBtravisB	streamingBsnitchesBsnareBshopsBsageBrichestBrelatedBrefugeeBpreachinBphiladelphiaBpasturesBojBmaseratiBmanifestBlizardB	lifetimesBlamaBjoseBiâ€™llBharleyBgrapeBfutileB	disciplesB	countriesBcalvaryBboxingBarthurBamoBacB
windshieldBtailorBswirlBstunnedBshuttingBrallyBpissingBpicassoBogsBmuseumBkisBjaguarB	insuranceB
insecurityBgeminiBgeBforlornBfabricBeuropeBdolceB	defendingBdansBcontinentalBconstellationBcaramelB	brutalityByounginBvaletBticksBsprinkleBsighedBrutBrotationBrobotsBrangerBplasterBperryBoperateBoooooooBodBnoticingBmorphineBmischiefBmaliceBinsectsBinfraredB
hypocritesBhannahBhaircutBgwaanBgeeseBfranticBforbidBflareB	featuringBfacinBexcessBeazyBbungBaggravationByoungestByangBwandBwailingBtinaBsyncBsuffragetteBsqueezinBshutsBrevB	realizingBpunchingBprintsB	precisionBoohahBmockingB
misfortuneBmetaphorBmapsB	limitlessBiiiiBflipsBdryingBdecadeBcurrencyBchoosinB	chauffeurBcajunBbonfireBalaskaBagingB
afternoonsBwobbleBvinylBtoiBstudiedBslidBshaneBsagB
rhinestoneBreviveBrevelationsBrelationBquarrelBpeepingBmysticalBmotiveBmillionairesBmcaBmachoBloadsB
lighteningB	imperfectBheirBhairedBfattyBdrownsBdominoesBdemonstrateB
demolitionBdaringBcuredBcoatedBclappinBcarbonBcalendarBbulldogBbucketsBblockingBbligeBbackdoorBavalonBamazesB
aggressiveBx1Bwho’sBtrenchBsurfingB	souvenirsBsnuckB
shimmeringBsheerBretributionBpussycatBpraBmillsBmassacreBmarathonBmamBjosephBguessinBgallowsBfumesBflyestBferrisBfangsBdifferencesB	criminalsBclonesBcampaignBbeezBapolloBanacondaBtysonB	treatmentBthoroughBterminalBtechnicolorBtbirdBtangBtabooBstockingBstemBslabBsisBscaringBsavoyB	romancingBrestsBpigeonsBpaymentBoscarBnocheBmisfitsBmaidsBludaBlowkeyBlacesBkneltBkiloBjigBjacketsBinvasionBincreaseBhoochieBgirliesBgaloreB	firelightBfckinB
electronicBdazBdahmBcrucialBcouplesBcomicBcaviarBbrrBbienBbeamingBbarriersB	barcelonaB	backwoodsBbackpackBwittyBwarninBtouristBtolerateBthrustBstunningBstinkingBspatBsolesBscrewsBrobbingBrevivalBrealisedBpicBperceiveBpawsBparksBotisB	obstaclesBminimalBmakersBlureBlowestBhowardBhondaBheavyweightBhangmanBfuriousBfrenzyBemailB
departmentBconfrontBcondemnB	charlotteBblackedBbadaBamourBalignedB22sBwomensBwipingBwhassupBvultureBvisualBveniceBvaiBtokB
threatenedB	submarineBspoonsBsoyBsammyBrulingBrinseB	revolvingBrentedBmortalsBmockBmaynB	lyricallyBitchyBhostileBheatsBequalityBecstacyBdriftsBdrifterBdippingBdatingBclosureBchilledBcabbageB	blueprintBbloomsBarBanklesB	witnessedBvolcanoBunforgettableBunderdogBuncoverB	strangelyBstaminaBrookieBrockawayBresultsB	persuadedBneoreulBmeltdownBmarchedBmagBkurtB	interviewBindiaBhomesickBfaggotsBexpandBelegantBchainsawBcartierBbreakupBbankerBballetBweyB
unfamiliarBtúBtzenaBtrumpBtiminBthreateningBslowedBsleetBrosaBrepresentingBrepresentinBrepliedBprodigalBprincesBnicelyBlesserBleakingBkennedyB	hillbillyB	hennesseyBgroovesBgiggleBgatorsBfloyBflashedBempiresBemperorBdmB	dancehallBcrackersB	continuedBcanaryBc5BbeamerBbasslineBawardsBarrangedBantBalphabetB
accustomedBtwentiesBtemplesBswungB
starstruckBspankB
skateboardBscornedBragesBrachelBqueroBpiggyBpajamasB
motherlessBmissilesBmarBmannerBltdBjordansBhiveBharbourBglowinB	gibraltarBgabrielB	fulfilledBfilmsBfamilysBexistedBequippedBentirelyBclitBclimaxBcitizensBcannonsBbundleBbothersBbarryBvesselBtribesBthreatenBsuzanneBstompingBsniffingBsewerBsettlesBsergeantBschooledBscanBrivalBreedB
publishingBprofitsBpitterBpattyBpainlessBouiBnapoleonB	mystifiedB	mistakingBlibsBfearfulBeighthB	dragonflyBdonchaBdistractionsBdillyBcrestBcherokeeBcaesarBbumpsB
bottomlessBblastedBassureBadlibsB36BwhirlingB	undecidedBtyBtwisterBtanksBsupermarketBsplendidBsneakyBsistaB	scarecrowB	saturdaysB	rocafellaBrebuildBreappearBpiledBpenitentiaryBnsBmistreatBmartyrsBknackBjackpotBfurnaceBfuckaBferryBemmaBelementBdesiredBdasB	copyrightB
collapsingBclimbsBcasanovaB	braceletsBbeautysB	ballerinaBworkerBunluckyBundyingB
undeniableBtrickedBtreetopsBthumpinBthroatsBstarvedBsohoBsoftenBsmokyB	sickeningBseekerB	psychoticBpitsBpicnicBnormBnicestB
negativityBmisledBkunivaBkatyBjanieBgrainsB	fragranceBfooBflutterBfluteBflawedBdrasticBcongoBchoppingBchoBchewedBcellularBbullsBblockinBashtrayB1234BwingedBwarningsBvisaBunwantedBtintB
timberlakeBthymeBthankingB
swallalalaBsustainBspookyBslippersBshawtysB	shamelessB
sensationsBprofoundBperiodBpansBobserveBlassieBkungBgreekBgonnB
glitteringBenoughsBduckingBdottedBdoodooBcrustBcrispBcountrysideBconvertibleBcomplimentsBcalicoBalarmedBweakestBwagesBuncleanBtupeloBtruestBthere´sB	techniqueB	surfboardBstruttinBstarvinBsplitsBshawnBseasideBregainBradicalBpoundinB
officiallyBnycBmustntBmixinBmandyBmadamBlulaBloonBlaurenBintroductionBinteriorBindoBheathenBglitchB	fracturedBearringsB	dandelionBd5BcubaBcorkBbudgeBaustinBanothaBamarilloBwoopBweirdoB
waterfrontBwardenBuntieBtuttiBstallingBsnackBsabotageBrocknBquitterB	postcardsBplightBpiratesBnosesBmammyB	lingeringBjacuzziBitchinBireneBinsectBguardedBgassedBflexingBferalBfelonyBenteringBeditionBdownloadBdoubtfulBdouB
dictionaryBdefyingBcolumbusB	clockworkB	cleopatraBcarryinBbustasBzootedBworryinBwitsBwhimBweepinBvowedBvampiresB
undertakerBtoughestBtkoBstylinBsoullessBsharperB	selectionBsawedBresideBpaganBnareulBmoodyBmilfBmetroBmemryBmackinBloudlyB	literallyBjockinBinsomniaB
imprisonedB
immaculateBgleeB	forgettinB	explainedBexcelsisBdominateBdisarrayBdiddleBdesolateBcoppinBchristopherBbrigadeBblistersBbleachBbangsBaquariusB	ambitionsBwrapsBwarmedBtooraBthereâ€™sBtermBsurgeonBsnatchedB
skyscraperBsecrecyB	recommendBpurchaseB	professorBpleasinB	mentionedBmalBjerkinBiphoneB	injectionBinfernalB
inevitableBhuntersBhughB
homecomingB	handshakeBhandingBhalleBgatorBfleetBeyeballsBeleanorBehohBdonaldBdankBcliffsBauthorBassaultBasphaltBaccuseB
waterfallsBtrippyBtrillionB
trampolineBspottedBspeedinBslopeBsigelB	scientistB	saxophoneBriteB	resurrectBrelievedB	rehearsedBpuckerB	probationBmarioBlinksBknockoutBjaggedBinvadedBimperfectionsBhydroB	holocaustBgaryBgalleryBfleasBempathyBdittyBdistractBdiningBdazedBdaddyoBcobraBcheyenneBcelloBcausinBcasaBbreedsBbatonBamidstBwokenBvaporBtengoBtecBswissB	spaghettiBshamBscarfBrumpBrevolvesBresortBresignB	reckoningBmallsB	lovelightBloudestB
limousinesBgritsB
extinctionBdividingBdeceiverBcreditsBcoolingB	celestialBcarrieBbannedBawakesBaccidentallyB92B48B3000B
wintertimeBvillaBvehicleBuntiedBtchaB	structureBstrainsBstalkBstabbingBsomewhatBskunkBsidekickB	shorelineBscissorsBsandalsBrockiesBrichardsBreservationsBpussiesBpluckBpilingBpermanentlyBmissusBmeaninB	mannequinBkatieBjezebelBjaysB
hystericalBhungoverBhuffBhoopsBgunninBgrippingBfoldingBfakesBfabBexpertBdrummersBdisrespectfulBdelightsBcontemptBconcentrationBchrousBceilinBburgerBbrittleBbreedingBblessinBbelBbaggyBapBamandaBainâ€™tBaccessBy’allBwindinBvioletsBverdictB	twentytwoBtunBtotinBsowingBsealsBscoresB	radiationBpicturedBpainterBmp3BmayhemBlsBkenBincludeBhardenedB
gracefullyBfussinB	fortyfiveBenjoyingBeasiestBdrainingBdazzleBcornyBcommeBchambersBbugleBblitzBbeautifullyBbakedBacominB	weatheredBunsungBtoylandBthatâ€™sBstrivingBstarterBselfcontrolBringtoneBowedBneomuB
moneymakerBmeasuresBlennyBkodakBjokersB
irrelevantBinvitationsBinfoBindestructibleBgroanBforfeitBfacadeBexodusBdoobyBdataBcrusadeBcraziestBconsumesBconstitutionBcommandsBcivilizationBcheeringBcatholicBburgundyBblamBbaleBapparentBangelicB85ByippieByalaB	westcoastBuprightB	undividedBtinselBthroughtBtappedBsoakinBskippinBshavedB	sexualityB
selfesteemBreciteBpushesB	nocturnalBmuchoBmobbinB	misbehaveBmaggotsBlucidBluBibizaBhavenBhallucinatingBgoooBgirlyBfridaysB	formationBflowedBfkinBfiremanB	exquisiteB
dominationB	disgustedBdisgustBdiegoBcrushinBcocktailBbreathsBbountyB	benjaminsB	behaviourBascendBarrivingByouyouBwardrobeBvita
��
Const_4Const*
_output_shapes	
:�N*
dtype0	*��
value��B��	�N"��                                                 	       
                                                                                                                                                                  !       "       #       $       %       &       '       (       )       *       +       ,       -       .       /       0       1       2       3       4       5       6       7       8       9       :       ;       <       =       >       ?       @       A       B       C       D       E       F       G       H       I       J       K       L       M       N       O       P       Q       R       S       T       U       V       W       X       Y       Z       [       \       ]       ^       _       `       a       b       c       d       e       f       g       h       i       j       k       l       m       n       o       p       q       r       s       t       u       v       w       x       y       z       {       |       }       ~              �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �                                                              	      
                                                                                                                                           !      "      #      $      %      &      '      (      )      *      +      ,      -      .      /      0      1      2      3      4      5      6      7      8      9      :      ;      <      =      >      ?      @      A      B      C      D      E      F      G      H      I      J      K      L      M      N      O      P      Q      R      S      T      U      V      W      X      Y      Z      [      \      ]      ^      _      `      a      b      c      d      e      f      g      h      i      j      k      l      m      n      o      p      q      r      s      t      u      v      w      x      y      z      {      |      }      ~            �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �                                                             	      
                                                                                                                                           !      "      #      $      %      &      '      (      )      *      +      ,      -      .      /      0      1      2      3      4      5      6      7      8      9      :      ;      <      =      >      ?      @      A      B      C      D      E      F      G      H      I      J      K      L      M      N      O      P      Q      R      S      T      U      V      W      X      Y      Z      [      \      ]      ^      _      `      a      b      c      d      e      f      g      h      i      j      k      l      m      n      o      p      q      r      s      t      u      v      w      x      y      z      {      |      }      ~            �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �                                                             	      
                                                                                                                                           !      "      #      $      %      &      '      (      )      *      +      ,      -      .      /      0      1      2      3      4      5      6      7      8      9      :      ;      <      =      >      ?      @      A      B      C      D      E      F      G      H      I      J      K      L      M      N      O      P      Q      R      S      T      U      V      W      X      Y      Z      [      \      ]      ^      _      `      a      b      c      d      e      f      g      h      i      j      k      l      m      n      o      p      q      r      s      t      u      v      w      x      y      z      {      |      }      ~            �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �                                                             	      
                                                                                                                                           !      "      #      $      %      &      '      (      )      *      +      ,      -      .      /      0      1      2      3      4      5      6      7      8      9      :      ;      <      =      >      ?      @      A      B      C      D      E      F      G      H      I      J      K      L      M      N      O      P      Q      R      S      T      U      V      W      X      Y      Z      [      \      ]      ^      _      `      a      b      c      d      e      f      g      h      i      j      k      l      m      n      o      p      q      r      s      t      u      v      w      x      y      z      {      |      }      ~            �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �                                                             	      
                                                                                                                                           !      "      #      $      %      &      '      (      )      *      +      ,      -      .      /      0      1      2      3      4      5      6      7      8      9      :      ;      <      =      >      ?      @      A      B      C      D      E      F      G      H      I      J      K      L      M      N      O      P      Q      R      S      T      U      V      W      X      Y      Z      [      \      ]      ^      _      `      a      b      c      d      e      f      g      h      i      j      k      l      m      n      o      p      q      r      s      t      u      v      w      x      y      z      {      |      }      ~            �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �                                                             	      
                                                                                                                                           !      "      #      $      %      &      '      (      )      *      +      ,      -      .      /      0      1      2      3      4      5      6      7      8      9      :      ;      <      =      >      ?      @      A      B      C      D      E      F      G      H      I      J      K      L      M      N      O      P      Q      R      S      T      U      V      W      X      Y      Z      [      \      ]      ^      _      `      a      b      c      d      e      f      g      h      i      j      k      l      m      n      o      p      q      r      s      t      u      v      w      x      y      z      {      |      }      ~            �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �                                                             	      
                                                                                                                                           !      "      #      $      %      &      '      (      )      *      +      ,      -      .      /      0      1      2      3      4      5      6      7      8      9      :      ;      <      =      >      ?      @      A      B      C      D      E      F      G      H      I      J      K      L      M      N      O      P      Q      R      S      T      U      V      W      X      Y      Z      [      \      ]      ^      _      `      a      b      c      d      e      f      g      h      i      j      k      l      m      n      o      p      q      r      s      t      u      v      w      x      y      z      {      |      }      ~            �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �                                                             	      
                                                                                                                                           !      "      #      $      %      &      '      (      )      *      +      ,      -      .      /      0      1      2      3      4      5      6      7      8      9      :      ;      <      =      >      ?      @      A      B      C      D      E      F      G      H      I      J      K      L      M      N      O      P      Q      R      S      T      U      V      W      X      Y      Z      [      \      ]      ^      _      `      a      b      c      d      e      f      g      h      i      j      k      l      m      n      o      p      q      r      s      t      u      v      w      x      y      z      {      |      }      ~            �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �       	      	      	      	      	      	      	      	      	      		      
	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	       	      !	      "	      #	      $	      %	      &	      '	      (	      )	      *	      +	      ,	      -	      .	      /	      0	      1	      2	      3	      4	      5	      6	      7	      8	      9	      :	      ;	      <	      =	      >	      ?	      @	      A	      B	      C	      D	      E	      F	      G	      H	      I	      J	      K	      L	      M	      N	      O	      P	      Q	      R	      S	      T	      U	      V	      W	      X	      Y	      Z	      [	      \	      ]	      ^	      _	      `	      a	      b	      c	      d	      e	      f	      g	      h	      i	      j	      k	      l	      m	      n	      o	      p	      q	      r	      s	      t	      u	      v	      w	      x	      y	      z	      {	      |	      }	      ~	      	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	       
      
      
      
      
      
      
      
      
      	
      

      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
       
      !
      "
      #
      $
      %
      &
      '
      (
      )
      *
      +
      ,
      -
      .
      /
      0
      1
      2
      3
      4
      5
      6
      7
      8
      9
      :
      ;
      <
      =
      >
      ?
      @
      A
      B
      C
      D
      E
      F
      G
      H
      I
      J
      K
      L
      M
      N
      O
      P
      Q
      R
      S
      T
      U
      V
      W
      X
      Y
      Z
      [
      \
      ]
      ^
      _
      `
      a
      b
      c
      d
      e
      f
      g
      h
      i
      j
      k
      l
      m
      n
      o
      p
      q
      r
      s
      t
      u
      v
      w
      x
      y
      z
      {
      |
      }
      ~
      
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
                                                             	      
                                                                                                                                           !      "      #      $      %      &      '      (      )      *      +      ,      -      .      /      0      1      2      3      4      5      6      7      8      9      :      ;      <      =      >      ?      @      A      B      C      D      E      F      G      H      I      J      K      L      M      N      O      P      Q      R      S      T      U      V      W      X      Y      Z      [      \      ]      ^      _      `      a      b      c      d      e      f      g      h      i      j      k      l      m      n      o      p      q      r      s      t      u      v      w      x      y      z      {      |      }      ~            �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �                                                             	      
                                                                                                                                           !      "      #      $      %      &      '      (      )      *      +      ,      -      .      /      0      1      2      3      4      5      6      7      8      9      :      ;      <      =      >      ?      @      A      B      C      D      E      F      G      H      I      J      K      L      M      N      O      P      Q      R      S      T      U      V      W      X      Y      Z      [      \      ]      ^      _      `      a      b      c      d      e      f      g      h      i      j      k      l      m      n      o      p      q      r      s      t      u      v      w      x      y      z      {      |      }      ~            �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �                                                             	      
                                                                                                                                           !      "      #      $      %      &      '      (      )      *      +      ,      -      .      /      0      1      2      3      4      5      6      7      8      9      :      ;      <      =      >      ?      @      A      B      C      D      E      F      G      H      I      J      K      L      M      N      O      P      Q      R      S      T      U      V      W      X      Y      Z      [      \      ]      ^      _      `      a      b      c      d      e      f      g      h      i      j      k      l      m      n      o      p      q      r      s      t      u      v      w      x      y      z      {      |      }      ~            �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �                                                             	      
                                                                                                                                           !      "      #      $      %      &      '      (      )      *      +      ,      -      .      /      0      1      2      3      4      5      6      7      8      9      :      ;      <      =      >      ?      @      A      B      C      D      E      F      G      H      I      J      K      L      M      N      O      P      Q      R      S      T      U      V      W      X      Y      Z      [      \      ]      ^      _      `      a      b      c      d      e      f      g      h      i      j      k      l      m      n      o      p      q      r      s      t      u      v      w      x      y      z      {      |      }      ~            �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �                                                             	      
                                                                                                                                           !      "      #      $      %      &      '      (      )      *      +      ,      -      .      /      0      1      2      3      4      5      6      7      8      9      :      ;      <      =      >      ?      @      A      B      C      D      E      F      G      H      I      J      K      L      M      N      O      P      Q      R      S      T      U      V      W      X      Y      Z      [      \      ]      ^      _      `      a      b      c      d      e      f      g      h      i      j      k      l      m      n      o      p      q      r      s      t      u      v      w      x      y      z      {      |      }      ~            �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �                                                             	      
                                                                                                                                           !      "      #      $      %      &      '      (      )      *      +      ,      -      .      /      0      1      2      3      4      5      6      7      8      9      :      ;      <      =      >      ?      @      A      B      C      D      E      F      G      H      I      J      K      L      M      N      O      P      Q      R      S      T      U      V      W      X      Y      Z      [      \      ]      ^      _      `      a      b      c      d      e      f      g      h      i      j      k      l      m      n      o      p      q      r      s      t      u      v      w      x      y      z      {      |      }      ~            �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �                                                             	      
                                                                                                                                           !      "      #      $      %      &      '      (      )      *      +      ,      -      .      /      0      1      2      3      4      5      6      7      8      9      :      ;      <      =      >      ?      @      A      B      C      D      E      F      G      H      I      J      K      L      M      N      O      P      Q      R      S      T      U      V      W      X      Y      Z      [      \      ]      ^      _      `      a      b      c      d      e      f      g      h      i      j      k      l      m      n      o      p      q      r      s      t      u      v      w      x      y      z      {      |      }      ~            �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �                                                             	      
                                                                                                                                           !      "      #      $      %      &      '      (      )      *      +      ,      -      .      /      0      1      2      3      4      5      6      7      8      9      :      ;      <      =      >      ?      @      A      B      C      D      E      F      G      H      I      J      K      L      M      N      O      P      Q      R      S      T      U      V      W      X      Y      Z      [      \      ]      ^      _      `      a      b      c      d      e      f      g      h      i      j      k      l      m      n      o      p      q      r      s      t      u      v      w      x      y      z      {      |      }      ~            �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �                                                             	      
                                                                                                                                           !      "      #      $      %      &      '      (      )      *      +      ,      -      .      /      0      1      2      3      4      5      6      7      8      9      :      ;      <      =      >      ?      @      A      B      C      D      E      F      G      H      I      J      K      L      M      N      O      P      Q      R      S      T      U      V      W      X      Y      Z      [      \      ]      ^      _      `      a      b      c      d      e      f      g      h      i      j      k      l      m      n      o      p      q      r      s      t      u      v      w      x      y      z      {      |      }      ~            �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �                                                             	      
                                                                                                                                           !      "      #      $      %      &      '      (      )      *      +      ,      -      .      /      0      1      2      3      4      5      6      7      8      9      :      ;      <      =      >      ?      @      A      B      C      D      E      F      G      H      I      J      K      L      M      N      O      P      Q      R      S      T      U      V      W      X      Y      Z      [      \      ]      ^      _      `      a      b      c      d      e      f      g      h      i      j      k      l      m      n      o      p      q      r      s      t      u      v      w      x      y      z      {      |      }      ~            �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �                                                             	      
                                                                                                                                           !      "      #      $      %      &      '      (      )      *      +      ,      -      .      /      0      1      2      3      4      5      6      7      8      9      :      ;      <      =      >      ?      @      A      B      C      D      E      F      G      H      I      J      K      L      M      N      O      P      Q      R      S      T      U      V      W      X      Y      Z      [      \      ]      ^      _      `      a      b      c      d      e      f      g      h      i      j      k      l      m      n      o      p      q      r      s      t      u      v      w      x      y      z      {      |      }      ~            �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �                                                             	      
                                                                                                                                           !      "      #      $      %      &      '      (      )      *      +      ,      -      .      /      0      1      2      3      4      5      6      7      8      9      :      ;      <      =      >      ?      @      A      B      C      D      E      F      G      H      I      J      K      L      M      N      O      P      Q      R      S      T      U      V      W      X      Y      Z      [      \      ]      ^      _      `      a      b      c      d      e      f      g      h      i      j      k      l      m      n      o      p      q      r      s      t      u      v      w      x      y      z      {      |      }      ~            �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �                                                             	      
                                                                                                                                           !      "      #      $      %      &      '      (      )      *      +      ,      -      .      /      0      1      2      3      4      5      6      7      8      9      :      ;      <      =      >      ?      @      A      B      C      D      E      F      G      H      I      J      K      L      M      N      O      P      Q      R      S      T      U      V      W      X      Y      Z      [      \      ]      ^      _      `      a      b      c      d      e      f      g      h      i      j      k      l      m      n      o      p      q      r      s      t      u      v      w      x      y      z      {      |      }      ~            �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �                                                             	      
                                                                                                                                           !      "      #      $      %      &      '      (      )      *      +      ,      -      .      /      0      1      2      3      4      5      6      7      8      9      :      ;      <      =      >      ?      @      A      B      C      D      E      F      G      H      I      J      K      L      M      N      O      P      Q      R      S      T      U      V      W      X      Y      Z      [      \      ]      ^      _      `      a      b      c      d      e      f      g      h      i      j      k      l      m      n      o      p      q      r      s      t      u      v      w      x      y      z      {      |      }      ~            �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �                                                             	      
                                                                                                                                           !      "      #      $      %      &      '      (      )      *      +      ,      -      .      /      0      1      2      3      4      5      6      7      8      9      :      ;      <      =      >      ?      @      A      B      C      D      E      F      G      H      I      J      K      L      M      N      O      P      Q      R      S      T      U      V      W      X      Y      Z      [      \      ]      ^      _      `      a      b      c      d      e      f      g      h      i      j      k      l      m      n      o      p      q      r      s      t      u      v      w      x      y      z      {      |      }      ~            �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �                                                             	      
                                                                                                                                           !      "      #      $      %      &      '      (      )      *      +      ,      -      .      /      0      1      2      3      4      5      6      7      8      9      :      ;      <      =      >      ?      @      A      B      C      D      E      F      G      H      I      J      K      L      M      N      O      P      Q      R      S      T      U      V      W      X      Y      Z      [      \      ]      ^      _      `      a      b      c      d      e      f      g      h      i      j      k      l      m      n      o      p      q      r      s      t      u      v      w      x      y      z      {      |      }      ~            �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �                                                             	      
                                                                                                                                           !      "      #      $      %      &      '      (      )      *      +      ,      -      .      /      0      1      2      3      4      5      6      7      8      9      :      ;      <      =      >      ?      @      A      B      C      D      E      F      G      H      I      J      K      L      M      N      O      P      Q      R      S      T      U      V      W      X      Y      Z      [      \      ]      ^      _      `      a      b      c      d      e      f      g      h      i      j      k      l      m      n      o      p      q      r      s      t      u      v      w      x      y      z      {      |      }      ~            �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �                                                             	      
                                                                                                                                           !      "      #      $      %      &      '      (      )      *      +      ,      -      .      /      0      1      2      3      4      5      6      7      8      9      :      ;      <      =      >      ?      @      A      B      C      D      E      F      G      H      I      J      K      L      M      N      O      P      Q      R      S      T      U      V      W      X      Y      Z      [      \      ]      ^      _      `      a      b      c      d      e      f      g      h      i      j      k      l      m      n      o      p      q      r      s      t      u      v      w      x      y      z      {      |      }      ~            �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �                                                             	      
                                                                                                                                           !      "      #      $      %      &      '      (      )      *      +      ,      -      .      /      0      1      2      3      4      5      6      7      8      9      :      ;      <      =      >      ?      @      A      B      C      D      E      F      G      H      I      J      K      L      M      N      O      P      Q      R      S      T      U      V      W      X      Y      Z      [      \      ]      ^      _      `      a      b      c      d      e      f      g      h      i      j      k      l      m      n      o      p      q      r      s      t      u      v      w      x      y      z      {      |      }      ~            �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �                                                             	      
                                                                                                                                           !      "      #      $      %      &      '      (      )      *      +      ,      -      .      /      0      1      2      3      4      5      6      7      8      9      :      ;      <      =      >      ?      @      A      B      C      D      E      F      G      H      I      J      K      L      M      N      O      P      Q      R      S      T      U      V      W      X      Y      Z      [      \      ]      ^      _      `      a      b      c      d      e      f      g      h      i      j      k      l      m      n      o      p      q      r      s      t      u      v      w      x      y      z      {      |      }      ~            �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �                                                             	      
                                                                                                                                           !      "      #      $      %      &      '      (      )      *      +      ,      -      .      /      0      1      2      3      4      5      6      7      8      9      :      ;      <      =      >      ?      @      A      B      C      D      E      F      G      H      I      J      K      L      M      N      O      P      Q      R      S      T      U      V      W      X      Y      Z      [      \      ]      ^      _      `      a      b      c      d      e      f      g      h      i      j      k      l      m      n      o      p      q      r      s      t      u      v      w      x      y      z      {      |      }      ~            �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �                                                                      	       
                                                                                                                                                                  !       "       #       $       %       &       '       (       )       *       +       ,       -       .       /       0       1       2       3       4       5       6       7       8       9       :       ;       <       =       >       ?       @       A       B       C       D       E       F       G       H       I       J       K       L       M       N       O       P       Q       R       S       T       U       V       W       X       Y       Z       [       \       ]       ^       _       `       a       b       c       d       e       f       g       h       i       j       k       l       m       n       o       p       q       r       s       t       u       v       w       x       y       z       {       |       }       ~              �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �        !      !      !      !      !      !      !      !      !      	!      
!      !      !      !      !      !      !      !      !      !      !      !      !      !      !      !      !      !      !      !      !      !       !      !!      "!      #!      $!      %!      &!      '!      (!      )!      *!      +!      ,!      -!      .!      /!      0!      1!      2!      3!      4!      5!      6!      7!      8!      9!      :!      ;!      <!      =!      >!      ?!      @!      A!      B!      C!      D!      E!      F!      G!      H!      I!      J!      K!      L!      M!      N!      O!      P!      Q!      R!      S!      T!      U!      V!      W!      X!      Y!      Z!      [!      \!      ]!      ^!      _!      `!      a!      b!      c!      d!      e!      f!      g!      h!      i!      j!      k!      l!      m!      n!      o!      p!      q!      r!      s!      t!      u!      v!      w!      x!      y!      z!      {!      |!      }!      ~!      !      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!       "      "      "      "      "      "      "      "      "      	"      
"      "      "      "      "      "      "      "      "      "      "      "      "      "      "      "      "      "      "      "      "      "       "      !"      ""      #"      $"      %"      &"      '"      ("      )"      *"      +"      ,"      -"      ."      /"      0"      1"      2"      3"      4"      5"      6"      7"      8"      9"      :"      ;"      <"      ="      >"      ?"      @"      A"      B"      C"      D"      E"      F"      G"      H"      I"      J"      K"      L"      M"      N"      O"      P"      Q"      R"      S"      T"      U"      V"      W"      X"      Y"      Z"      ["      \"      ]"      ^"      _"      `"      a"      b"      c"      d"      e"      f"      g"      h"      i"      j"      k"      l"      m"      n"      o"      p"      q"      r"      s"      t"      u"      v"      w"      x"      y"      z"      {"      |"      }"      ~"      "      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"       #      #      #      #      #      #      #      #      #      	#      
#      #      #      #      #      #      #      #      #      #      #      #      #      #      #      #      #      #      #      #      #      #       #      !#      "#      ##      $#      %#      &#      '#      (#      )#      *#      +#      ,#      -#      .#      /#      0#      1#      2#      3#      4#      5#      6#      7#      8#      9#      :#      ;#      <#      =#      >#      ?#      @#      A#      B#      C#      D#      E#      F#      G#      H#      I#      J#      K#      L#      M#      N#      O#      P#      Q#      R#      S#      T#      U#      V#      W#      X#      Y#      Z#      [#      \#      ]#      ^#      _#      `#      a#      b#      c#      d#      e#      f#      g#      h#      i#      j#      k#      l#      m#      n#      o#      p#      q#      r#      s#      t#      u#      v#      w#      x#      y#      z#      {#      |#      }#      ~#      #      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#       $      $      $      $      $      $      $      $      $      	$      
$      $      $      $      $      $      $      $      $      $      $      $      $      $      $      $      $      $      $      $      $      $       $      !$      "$      #$      $$      %$      &$      '$      ($      )$      *$      +$      ,$      -$      .$      /$      0$      1$      2$      3$      4$      5$      6$      7$      8$      9$      :$      ;$      <$      =$      >$      ?$      @$      A$      B$      C$      D$      E$      F$      G$      H$      I$      J$      K$      L$      M$      N$      O$      P$      Q$      R$      S$      T$      U$      V$      W$      X$      Y$      Z$      [$      \$      ]$      ^$      _$      `$      a$      b$      c$      d$      e$      f$      g$      h$      i$      j$      k$      l$      m$      n$      o$      p$      q$      r$      s$      t$      u$      v$      w$      x$      y$      z$      {$      |$      }$      ~$      $      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$       %      %      %      %      %      %      %      %      %      	%      
%      %      %      %      %      %      %      %      %      %      %      %      %      %      %      %      %      %      %      %      %      %       %      !%      "%      #%      $%      %%      &%      '%      (%      )%      *%      +%      ,%      -%      .%      /%      0%      1%      2%      3%      4%      5%      6%      7%      8%      9%      :%      ;%      <%      =%      >%      ?%      @%      A%      B%      C%      D%      E%      F%      G%      H%      I%      J%      K%      L%      M%      N%      O%      P%      Q%      R%      S%      T%      U%      V%      W%      X%      Y%      Z%      [%      \%      ]%      ^%      _%      `%      a%      b%      c%      d%      e%      f%      g%      h%      i%      j%      k%      l%      m%      n%      o%      p%      q%      r%      s%      t%      u%      v%      w%      x%      y%      z%      {%      |%      }%      ~%      %      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%       &      &      &      &      &      &      &      &      &      	&      
&      &      &      &      &      &      &      &      &      &      &      &      &      &      &      &      &      &      &      &      &      &       &      !&      "&      #&      $&      %&      &&      '&      (&      )&      *&      +&      ,&      -&      .&      /&      0&      1&      2&      3&      4&      5&      6&      7&      8&      9&      :&      ;&      <&      =&      >&      ?&      @&      A&      B&      C&      D&      E&      F&      G&      H&      I&      J&      K&      L&      M&      N&      O&      P&      Q&      R&      S&      T&      U&      V&      W&      X&      Y&      Z&      [&      \&      ]&      ^&      _&      `&      a&      b&      c&      d&      e&      f&      g&      h&      i&      j&      k&      l&      m&      n&      o&      p&      q&      r&      s&      t&      u&      v&      w&      x&      y&      z&      {&      |&      }&      ~&      &      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&       '      '      '      '      '      '      '      '      '      	'      
'      '      '      '      '      '      
�
StatefulPartitionedCallStatefulPartitionedCall
hash_tableConst_3Const_4*
Tin
2	*
Tout
2*
_collective_manager_ids
 *
_output_shapes
: * 
_read_only_resource_inputs
 *0
config_proto 

CPU

GPU2*0J 8� *%
f R
__inference_<lambda>_1740592
&
NoOpNoOp^StatefulPartitionedCall
�3
Const_5Const"/device:CPU:0*
_output_shapes
: *
dtype0*�3
value�3B�3 B�3
�
layer-0
layer_with_weights-0
layer-1
layer_with_weights-1
layer-2
layer-3
layer-4
layer_with_weights-2
layer-5
	optimizer
	variables
	trainable_variables

regularization_losses
	keras_api
__call__
*&call_and_return_all_conditional_losses
_default_save_signature

signatures*
$
_lookup_layer
	keras_api* 
�

embeddings
	variables
trainable_variables
regularization_losses
	keras_api
__call__
*&call_and_return_all_conditional_losses*
�

kernel
bias
	variables
trainable_variables
regularization_losses
	keras_api
__call__
* &call_and_return_all_conditional_losses*
�
!	variables
"trainable_variables
#regularization_losses
$	keras_api
%__call__
*&&call_and_return_all_conditional_losses* 
�
'	variables
(trainable_variables
)regularization_losses
*	keras_api
+_random_generator
,__call__
*-&call_and_return_all_conditional_losses* 
�

.kernel
/bias
0	variables
1trainable_variables
2regularization_losses
3	keras_api
4__call__
*5&call_and_return_all_conditional_losses*
�
6iter

7beta_1

8beta_2
	9decay
:learning_ratemlmmmn.mo/mpvqvrvs.vt/vu*
'
0
1
2
.3
/4*
'
0
1
2
.3
/4*
* 
�
;non_trainable_variables

<layers
=metrics
>layer_regularization_losses
?layer_metrics
	variables
	trainable_variables

regularization_losses
__call__
_default_save_signature
*&call_and_return_all_conditional_losses
&"call_and_return_conditional_losses*
* 
* 
* 

@serving_default* 
9
Ainput_vocabulary
Blookup_table
C	keras_api* 
* 
jd
VARIABLE_VALUEembedding_6/embeddings:layer_with_weights-0/embeddings/.ATTRIBUTES/VARIABLE_VALUE*

0*

0*
* 
�
Dnon_trainable_variables

Elayers
Fmetrics
Glayer_regularization_losses
Hlayer_metrics
	variables
trainable_variables
regularization_losses
__call__
*&call_and_return_all_conditional_losses
&"call_and_return_conditional_losses*
* 
* 
_Y
VARIABLE_VALUEconv1d_1/kernel6layer_with_weights-1/kernel/.ATTRIBUTES/VARIABLE_VALUE*
[U
VARIABLE_VALUEconv1d_1/bias4layer_with_weights-1/bias/.ATTRIBUTES/VARIABLE_VALUE*

0
1*

0
1*
* 
�
Inon_trainable_variables

Jlayers
Kmetrics
Llayer_regularization_losses
Mlayer_metrics
	variables
trainable_variables
regularization_losses
__call__
* &call_and_return_all_conditional_losses
& "call_and_return_conditional_losses*
* 
* 
* 
* 
* 
�
Nnon_trainable_variables

Olayers
Pmetrics
Qlayer_regularization_losses
Rlayer_metrics
!	variables
"trainable_variables
#regularization_losses
%__call__
*&&call_and_return_all_conditional_losses
&&"call_and_return_conditional_losses* 
* 
* 
* 
* 
* 
�
Snon_trainable_variables

Tlayers
Umetrics
Vlayer_regularization_losses
Wlayer_metrics
'	variables
(trainable_variables
)regularization_losses
,__call__
*-&call_and_return_all_conditional_losses
&-"call_and_return_conditional_losses* 
* 
* 
* 
_Y
VARIABLE_VALUEdense_36/kernel6layer_with_weights-2/kernel/.ATTRIBUTES/VARIABLE_VALUE*
[U
VARIABLE_VALUEdense_36/bias4layer_with_weights-2/bias/.ATTRIBUTES/VARIABLE_VALUE*

.0
/1*

.0
/1*
* 
�
Xnon_trainable_variables

Ylayers
Zmetrics
[layer_regularization_losses
\layer_metrics
0	variables
1trainable_variables
2regularization_losses
4__call__
*5&call_and_return_all_conditional_losses
&5"call_and_return_conditional_losses*
* 
* 
LF
VARIABLE_VALUE	Adam/iter)optimizer/iter/.ATTRIBUTES/VARIABLE_VALUE*
PJ
VARIABLE_VALUEAdam/beta_1+optimizer/beta_1/.ATTRIBUTES/VARIABLE_VALUE*
PJ
VARIABLE_VALUEAdam/beta_2+optimizer/beta_2/.ATTRIBUTES/VARIABLE_VALUE*
NH
VARIABLE_VALUE
Adam/decay*optimizer/decay/.ATTRIBUTES/VARIABLE_VALUE*
^X
VARIABLE_VALUEAdam/learning_rate2optimizer/learning_rate/.ATTRIBUTES/VARIABLE_VALUE*
* 
.
0
1
2
3
4
5*

]0
^1*
* 
* 
* 
* 
R
__initializer
`_create_resource
a_initialize
b_destroy_resource* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
8
	ctotal
	dcount
e	variables
f	keras_api*
H
	gtotal
	hcount
i
_fn_kwargs
j	variables
k	keras_api*
* 
* 
* 
* 
SM
VARIABLE_VALUEtotal4keras_api/metrics/0/total/.ATTRIBUTES/VARIABLE_VALUE*
SM
VARIABLE_VALUEcount4keras_api/metrics/0/count/.ATTRIBUTES/VARIABLE_VALUE*

c0
d1*

e	variables*
UO
VARIABLE_VALUEtotal_14keras_api/metrics/1/total/.ATTRIBUTES/VARIABLE_VALUE*
UO
VARIABLE_VALUEcount_14keras_api/metrics/1/count/.ATTRIBUTES/VARIABLE_VALUE*
* 

g0
h1*

j	variables*
��
VARIABLE_VALUEAdam/embedding_6/embeddings/mVlayer_with_weights-0/embeddings/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUE*
�|
VARIABLE_VALUEAdam/conv1d_1/kernel/mRlayer_with_weights-1/kernel/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUE*
~x
VARIABLE_VALUEAdam/conv1d_1/bias/mPlayer_with_weights-1/bias/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUE*
�|
VARIABLE_VALUEAdam/dense_36/kernel/mRlayer_with_weights-2/kernel/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUE*
~x
VARIABLE_VALUEAdam/dense_36/bias/mPlayer_with_weights-2/bias/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUE*
��
VARIABLE_VALUEAdam/embedding_6/embeddings/vVlayer_with_weights-0/embeddings/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUE*
�|
VARIABLE_VALUEAdam/conv1d_1/kernel/vRlayer_with_weights-1/kernel/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUE*
~x
VARIABLE_VALUEAdam/conv1d_1/bias/vPlayer_with_weights-1/bias/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUE*
�|
VARIABLE_VALUEAdam/dense_36/kernel/vRlayer_with_weights-2/kernel/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUE*
~x
VARIABLE_VALUEAdam/dense_36/bias/vPlayer_with_weights-2/bias/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUE*
{
serving_default_input_22Placeholder*'
_output_shapes
:���������*
dtype0*
shape:���������
�
StatefulPartitionedCall_1StatefulPartitionedCallserving_default_input_22
hash_tableConstConst_1Const_2embedding_6/embeddingsconv1d_1/kernelconv1d_1/biasdense_36/kerneldense_36/bias*
Tin
2
		*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������
*'
_read_only_resource_inputs	
	*0
config_proto 

CPU

GPU2*0J 8� *.
f)R'
%__inference_signature_wrapper_1740467
O
saver_filenamePlaceholder*
_output_shapes
: *
dtype0*
shape: 
�	
StatefulPartitionedCall_2StatefulPartitionedCallsaver_filename*embedding_6/embeddings/Read/ReadVariableOp#conv1d_1/kernel/Read/ReadVariableOp!conv1d_1/bias/Read/ReadVariableOp#dense_36/kernel/Read/ReadVariableOp!dense_36/bias/Read/ReadVariableOpAdam/iter/Read/ReadVariableOpAdam/beta_1/Read/ReadVariableOpAdam/beta_2/Read/ReadVariableOpAdam/decay/Read/ReadVariableOp&Adam/learning_rate/Read/ReadVariableOptotal/Read/ReadVariableOpcount/Read/ReadVariableOptotal_1/Read/ReadVariableOpcount_1/Read/ReadVariableOp1Adam/embedding_6/embeddings/m/Read/ReadVariableOp*Adam/conv1d_1/kernel/m/Read/ReadVariableOp(Adam/conv1d_1/bias/m/Read/ReadVariableOp*Adam/dense_36/kernel/m/Read/ReadVariableOp(Adam/dense_36/bias/m/Read/ReadVariableOp1Adam/embedding_6/embeddings/v/Read/ReadVariableOp*Adam/conv1d_1/kernel/v/Read/ReadVariableOp(Adam/conv1d_1/bias/v/Read/ReadVariableOp*Adam/dense_36/kernel/v/Read/ReadVariableOp(Adam/dense_36/bias/v/Read/ReadVariableOpConst_5*%
Tin
2	*
Tout
2*
_collective_manager_ids
 *
_output_shapes
: * 
_read_only_resource_inputs
 *0
config_proto 

CPU

GPU2*0J 8� *)
f$R"
 __inference__traced_save_1740694
�
StatefulPartitionedCall_3StatefulPartitionedCallsaver_filenameembedding_6/embeddingsconv1d_1/kernelconv1d_1/biasdense_36/kerneldense_36/bias	Adam/iterAdam/beta_1Adam/beta_2
Adam/decayAdam/learning_ratetotalcounttotal_1count_1Adam/embedding_6/embeddings/mAdam/conv1d_1/kernel/mAdam/conv1d_1/bias/mAdam/dense_36/kernel/mAdam/dense_36/bias/mAdam/embedding_6/embeddings/vAdam/conv1d_1/kernel/vAdam/conv1d_1/bias/vAdam/dense_36/kernel/vAdam/dense_36/bias/v*$
Tin
2*
Tout
2*
_collective_manager_ids
 *
_output_shapes
: * 
_read_only_resource_inputs
 *0
config_proto 

CPU

GPU2*0J 8� *,
f'R%
#__inference__traced_restore_1740776��	
�

�
E__inference_dense_36_layer_call_and_return_conditional_losses_1739876

inputs1
matmul_readvariableop_resource:	�
-
biasadd_readvariableop_resource:

identity��BiasAdd/ReadVariableOp�MatMul/ReadVariableOpu
MatMul/ReadVariableOpReadVariableOpmatmul_readvariableop_resource*
_output_shapes
:	�
*
dtype0i
MatMulMatMulinputsMatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������
r
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes
:
*
dtype0v
BiasAddBiasAddMatMul:product:0BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������
V
SoftmaxSoftmaxBiasAdd:output:0*
T0*'
_output_shapes
:���������
`
IdentityIdentitySoftmax:softmax:0^NoOp*
T0*'
_output_shapes
:���������
w
NoOpNoOp^BiasAdd/ReadVariableOp^MatMul/ReadVariableOp*"
_acd_function_control_output(*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime*+
_input_shapes
:����������: : 20
BiasAdd/ReadVariableOpBiasAdd/ReadVariableOp2.
MatMul/ReadVariableOpMatMul/ReadVariableOp:P L
(
_output_shapes
:����������
 
_user_specified_nameinputs
�
�
-__inference_embedding_6_layer_call_fn_1740474

inputs	
unknown:	�Nd
identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallinputsunknown*
Tin
2	*
Tout
2*
_collective_manager_ids
 *,
_output_shapes
:����������d*#
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *Q
fLRJ
H__inference_embedding_6_layer_call_and_return_conditional_losses_1739831t
IdentityIdentity StatefulPartitionedCall:output:0^NoOp*
T0*,
_output_shapes
:����������d`
NoOpNoOp^StatefulPartitionedCall*"
_acd_function_control_output(*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime*)
_input_shapes
:����������: 22
StatefulPartitionedCallStatefulPartitionedCall:P L
(
_output_shapes
:����������
 
_user_specified_nameinputs
�
H
,__inference_dropout_15_layer_call_fn_1740524

inputs
identity�
PartitionedCallPartitionedCallinputs*
Tin
2*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:����������* 
_read_only_resource_inputs
 *0
config_proto 

CPU

GPU2*0J 8� *P
fKRI
G__inference_dropout_15_layer_call_and_return_conditional_losses_1739863a
IdentityIdentityPartitionedCall:output:0*
T0*(
_output_shapes
:����������"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*'
_input_shapes
:����������:P L
(
_output_shapes
:����������
 
_user_specified_nameinputs
�
�
*__inference_dense_36_layer_call_fn_1740555

inputs
unknown:	�

	unknown_0:

identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallinputsunknown	unknown_0*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������
*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *N
fIRG
E__inference_dense_36_layer_call_and_return_conditional_losses_1739876o
IdentityIdentity StatefulPartitionedCall:output:0^NoOp*
T0*'
_output_shapes
:���������
`
NoOpNoOp^StatefulPartitionedCall*"
_acd_function_control_output(*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime*+
_input_shapes
:����������: : 22
StatefulPartitionedCallStatefulPartitionedCall:P L
(
_output_shapes
:����������
 
_user_specified_nameinputs
�
�
H__inference_embedding_6_layer_call_and_return_conditional_losses_1739831

inputs	+
embedding_lookup_1739825:	�Nd
identity��embedding_lookup�
embedding_lookupResourceGatherembedding_lookup_1739825inputs*
Tindices0	*+
_class!
loc:@embedding_lookup/1739825*,
_output_shapes
:����������d*
dtype0�
embedding_lookup/IdentityIdentityembedding_lookup:output:0*
T0*+
_class!
loc:@embedding_lookup/1739825*,
_output_shapes
:����������d�
embedding_lookup/Identity_1Identity"embedding_lookup/Identity:output:0*
T0*,
_output_shapes
:����������dx
IdentityIdentity$embedding_lookup/Identity_1:output:0^NoOp*
T0*,
_output_shapes
:����������dY
NoOpNoOp^embedding_lookup*"
_acd_function_control_output(*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime*)
_input_shapes
:����������: 2$
embedding_lookupembedding_lookup:P L
(
_output_shapes
:����������
 
_user_specified_nameinputs
�
e
G__inference_dropout_15_layer_call_and_return_conditional_losses_1740534

inputs

identity_1O
IdentityIdentityinputs*
T0*(
_output_shapes
:����������\

Identity_1IdentityIdentity:output:0*
T0*(
_output_shapes
:����������"!

identity_1Identity_1:output:0*(
_construction_contextkEagerRuntime*'
_input_shapes
:����������:P L
(
_output_shapes
:����������
 
_user_specified_nameinputs
�
�
E__inference_conv1d_1_layer_call_and_return_conditional_losses_1740508

inputsB
+conv1d_expanddims_1_readvariableop_resource:d�.
biasadd_readvariableop_resource:	�
identity��BiasAdd/ReadVariableOp�"Conv1D/ExpandDims_1/ReadVariableOp`
Conv1D/ExpandDims/dimConst*
_output_shapes
: *
dtype0*
valueB :
����������
Conv1D/ExpandDims
ExpandDimsinputsConv1D/ExpandDims/dim:output:0*
T0*0
_output_shapes
:����������d�
"Conv1D/ExpandDims_1/ReadVariableOpReadVariableOp+conv1d_expanddims_1_readvariableop_resource*#
_output_shapes
:d�*
dtype0Y
Conv1D/ExpandDims_1/dimConst*
_output_shapes
: *
dtype0*
value	B : �
Conv1D/ExpandDims_1
ExpandDims*Conv1D/ExpandDims_1/ReadVariableOp:value:0 Conv1D/ExpandDims_1/dim:output:0*
T0*'
_output_shapes
:d��
Conv1DConv2DConv1D/ExpandDims:output:0Conv1D/ExpandDims_1:output:0*
T0*1
_output_shapes
:�����������*
paddingVALID*
strides
�
Conv1D/SqueezeSqueezeConv1D:output:0*
T0*-
_output_shapes
:�����������*
squeeze_dims

���������s
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes	
:�*
dtype0�
BiasAddBiasAddConv1D/Squeeze:output:0BiasAdd/ReadVariableOp:value:0*
T0*-
_output_shapes
:�����������V
ReluReluBiasAdd:output:0*
T0*-
_output_shapes
:�����������g
IdentityIdentityRelu:activations:0^NoOp*
T0*-
_output_shapes
:������������
NoOpNoOp^BiasAdd/ReadVariableOp#^Conv1D/ExpandDims_1/ReadVariableOp*"
_acd_function_control_output(*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime*/
_input_shapes
:����������d: : 20
BiasAdd/ReadVariableOpBiasAdd/ReadVariableOp2H
"Conv1D/ExpandDims_1/ReadVariableOp"Conv1D/ExpandDims_1/ReadVariableOp:T P
,
_output_shapes
:����������d
 
_user_specified_nameinputs
�

�
/__inference_sequential_21_layer_call_fn_1739904
input_22
unknown
	unknown_0	
	unknown_1
	unknown_2	
	unknown_3:	�Nd 
	unknown_4:d�
	unknown_5:	�
	unknown_6:	�

	unknown_7:

identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallinput_22unknown	unknown_0	unknown_1	unknown_2	unknown_3	unknown_4	unknown_5	unknown_6	unknown_7*
Tin
2
		*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������
*'
_read_only_resource_inputs	
	*0
config_proto 

CPU

GPU2*0J 8� *S
fNRL
J__inference_sequential_21_layer_call_and_return_conditional_losses_1739883o
IdentityIdentity StatefulPartitionedCall:output:0^NoOp*
T0*'
_output_shapes
:���������
`
NoOpNoOp^StatefulPartitionedCall*"
_acd_function_control_output(*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime*8
_input_shapes'
%:���������: : : : : : : : : 22
StatefulPartitionedCallStatefulPartitionedCall:Q M
'
_output_shapes
:���������
"
_user_specified_name
input_22:

_output_shapes
: :

_output_shapes
: :

_output_shapes
: 
�
e
G__inference_dropout_15_layer_call_and_return_conditional_losses_1739863

inputs

identity_1O
IdentityIdentityinputs*
T0*(
_output_shapes
:����������\

Identity_1IdentityIdentity:output:0*
T0*(
_output_shapes
:����������"!

identity_1Identity_1:output:0*(
_construction_contextkEagerRuntime*'
_input_shapes
:����������:P L
(
_output_shapes
:����������
 
_user_specified_nameinputs
�o
�
J__inference_sequential_21_layer_call_and_return_conditional_losses_1740158
input_22[
Wtext_vectorization_14_string_lookup_42_hash_table_lookup_lookuptablefindv2_table_handle\
Xtext_vectorization_14_string_lookup_42_hash_table_lookup_lookuptablefindv2_default_value	2
.text_vectorization_14_string_lookup_42_equal_y5
1text_vectorization_14_string_lookup_42_selectv2_t	&
embedding_6_1740142:	�Nd'
conv1d_1_1740145:d�
conv1d_1_1740147:	�#
dense_36_1740152:	�

dense_36_1740154:

identity�� conv1d_1/StatefulPartitionedCall� dense_36/StatefulPartitionedCall�#embedding_6/StatefulPartitionedCall�Jtext_vectorization_14/string_lookup_42/hash_table_Lookup/LookupTableFindV2c
!text_vectorization_14/StringLowerStringLowerinput_22*'
_output_shapes
:����������
(text_vectorization_14/StaticRegexReplaceStaticRegexReplace*text_vectorization_14/StringLower:output:0*'
_output_shapes
:���������*6
pattern+)[!"#$%&()\*\+,-\./:;<=>?@\[\\\]^_`{|}~\']*
rewrite �
text_vectorization_14/SqueezeSqueeze1text_vectorization_14/StaticRegexReplace:output:0*
T0*#
_output_shapes
:���������*
squeeze_dims

���������h
'text_vectorization_14/StringSplit/ConstConst*
_output_shapes
: *
dtype0*
valueB B �
/text_vectorization_14/StringSplit/StringSplitV2StringSplitV2&text_vectorization_14/Squeeze:output:00text_vectorization_14/StringSplit/Const:output:0*<
_output_shapes*
(:���������:���������:�
5text_vectorization_14/StringSplit/strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB"        �
7text_vectorization_14/StringSplit/strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB"       �
7text_vectorization_14/StringSplit/strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB"      �
/text_vectorization_14/StringSplit/strided_sliceStridedSlice9text_vectorization_14/StringSplit/StringSplitV2:indices:0>text_vectorization_14/StringSplit/strided_slice/stack:output:0@text_vectorization_14/StringSplit/strided_slice/stack_1:output:0@text_vectorization_14/StringSplit/strided_slice/stack_2:output:0*
Index0*
T0	*#
_output_shapes
:���������*

begin_mask*
end_mask*
shrink_axis_mask�
7text_vectorization_14/StringSplit/strided_slice_1/stackConst*
_output_shapes
:*
dtype0*
valueB: �
9text_vectorization_14/StringSplit/strided_slice_1/stack_1Const*
_output_shapes
:*
dtype0*
valueB:�
9text_vectorization_14/StringSplit/strided_slice_1/stack_2Const*
_output_shapes
:*
dtype0*
valueB:�
1text_vectorization_14/StringSplit/strided_slice_1StridedSlice7text_vectorization_14/StringSplit/StringSplitV2:shape:0@text_vectorization_14/StringSplit/strided_slice_1/stack:output:0Btext_vectorization_14/StringSplit/strided_slice_1/stack_1:output:0Btext_vectorization_14/StringSplit/strided_slice_1/stack_2:output:0*
Index0*
T0	*
_output_shapes
: *
shrink_axis_mask�
Xtext_vectorization_14/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/CastCast8text_vectorization_14/StringSplit/strided_slice:output:0*

DstT0*

SrcT0	*#
_output_shapes
:����������
Ztext_vectorization_14/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cast_1Cast:text_vectorization_14/StringSplit/strided_slice_1:output:0*

DstT0*

SrcT0	*
_output_shapes
: �
btext_vectorization_14/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/ShapeShape\text_vectorization_14/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cast:y:0*
T0*
_output_shapes
:�
btext_vectorization_14/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/ConstConst*
_output_shapes
:*
dtype0*
valueB: �
atext_vectorization_14/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/ProdProdktext_vectorization_14/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Shape:output:0ktext_vectorization_14/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Const:output:0*
T0*
_output_shapes
: �
ftext_vectorization_14/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Greater/yConst*
_output_shapes
: *
dtype0*
value	B : �
dtext_vectorization_14/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/GreaterGreaterjtext_vectorization_14/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Prod:output:0otext_vectorization_14/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Greater/y:output:0*
T0*
_output_shapes
: �
atext_vectorization_14/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/CastCasthtext_vectorization_14/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Greater:z:0*

DstT0*

SrcT0
*
_output_shapes
: �
dtext_vectorization_14/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Const_1Const*
_output_shapes
:*
dtype0*
valueB: �
`text_vectorization_14/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/MaxMax\text_vectorization_14/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cast:y:0mtext_vectorization_14/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Const_1:output:0*
T0*
_output_shapes
: �
btext_vectorization_14/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/add/yConst*
_output_shapes
: *
dtype0*
value	B :�
`text_vectorization_14/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/addAddV2itext_vectorization_14/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Max:output:0ktext_vectorization_14/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/add/y:output:0*
T0*
_output_shapes
: �
`text_vectorization_14/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/mulMuletext_vectorization_14/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Cast:y:0dtext_vectorization_14/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/add:z:0*
T0*
_output_shapes
: �
dtext_vectorization_14/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/MaximumMaximum^text_vectorization_14/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cast_1:y:0dtext_vectorization_14/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/mul:z:0*
T0*
_output_shapes
: �
dtext_vectorization_14/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/MinimumMinimum^text_vectorization_14/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cast_1:y:0htext_vectorization_14/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Maximum:z:0*
T0*
_output_shapes
: �
dtext_vectorization_14/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Const_2Const*
_output_shapes
: *
dtype0	*
valueB	 �
etext_vectorization_14/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/BincountBincount\text_vectorization_14/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cast:y:0htext_vectorization_14/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Minimum:z:0mtext_vectorization_14/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Const_2:output:0*
T0	*#
_output_shapes
:����������
_text_vectorization_14/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cumsum/axisConst*
_output_shapes
: *
dtype0*
value	B : �
Ztext_vectorization_14/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/CumsumCumsumltext_vectorization_14/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Bincount:bins:0htext_vectorization_14/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cumsum/axis:output:0*
T0	*#
_output_shapes
:����������
ctext_vectorization_14/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/concat/values_0Const*
_output_shapes
:*
dtype0	*
valueB	R �
_text_vectorization_14/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/concat/axisConst*
_output_shapes
: *
dtype0*
value	B : �
Ztext_vectorization_14/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/concatConcatV2ltext_vectorization_14/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/concat/values_0:output:0`text_vectorization_14/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cumsum:out:0htext_vectorization_14/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/concat/axis:output:0*
N*
T0	*#
_output_shapes
:����������
Jtext_vectorization_14/string_lookup_42/hash_table_Lookup/LookupTableFindV2LookupTableFindV2Wtext_vectorization_14_string_lookup_42_hash_table_lookup_lookuptablefindv2_table_handle8text_vectorization_14/StringSplit/StringSplitV2:values:0Xtext_vectorization_14_string_lookup_42_hash_table_lookup_lookuptablefindv2_default_value*	
Tin0*

Tout0	*#
_output_shapes
:����������
,text_vectorization_14/string_lookup_42/EqualEqual8text_vectorization_14/StringSplit/StringSplitV2:values:0.text_vectorization_14_string_lookup_42_equal_y*
T0*#
_output_shapes
:����������
/text_vectorization_14/string_lookup_42/SelectV2SelectV20text_vectorization_14/string_lookup_42/Equal:z:01text_vectorization_14_string_lookup_42_selectv2_tStext_vectorization_14/string_lookup_42/hash_table_Lookup/LookupTableFindV2:values:0*
T0	*#
_output_shapes
:����������
/text_vectorization_14/string_lookup_42/IdentityIdentity8text_vectorization_14/string_lookup_42/SelectV2:output:0*
T0	*#
_output_shapes
:���������t
2text_vectorization_14/RaggedToTensor/default_valueConst*
_output_shapes
: *
dtype0	*
value	B	 R �
*text_vectorization_14/RaggedToTensor/ConstConst*
_output_shapes
:*
dtype0	*%
valueB	"���������       �
9text_vectorization_14/RaggedToTensor/RaggedTensorToTensorRaggedTensorToTensor3text_vectorization_14/RaggedToTensor/Const:output:08text_vectorization_14/string_lookup_42/Identity:output:0;text_vectorization_14/RaggedToTensor/default_value:output:0:text_vectorization_14/StringSplit/strided_slice_1:output:08text_vectorization_14/StringSplit/strided_slice:output:0*
T0	*
Tindex0	*
Tshape0	*(
_output_shapes
:����������*
num_row_partition_tensors*7
row_partition_types 
FIRST_DIM_SIZEVALUE_ROWIDS�
#embedding_6/StatefulPartitionedCallStatefulPartitionedCallBtext_vectorization_14/RaggedToTensor/RaggedTensorToTensor:result:0embedding_6_1740142*
Tin
2	*
Tout
2*
_collective_manager_ids
 *,
_output_shapes
:����������d*#
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *Q
fLRJ
H__inference_embedding_6_layer_call_and_return_conditional_losses_1739831�
 conv1d_1/StatefulPartitionedCallStatefulPartitionedCall,embedding_6/StatefulPartitionedCall:output:0conv1d_1_1740145conv1d_1_1740147*
Tin
2*
Tout
2*
_collective_manager_ids
 *-
_output_shapes
:�����������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *N
fIRG
E__inference_conv1d_1_layer_call_and_return_conditional_losses_1739851�
&global_max_pooling1d_1/PartitionedCallPartitionedCall)conv1d_1/StatefulPartitionedCall:output:0*
Tin
2*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:����������* 
_read_only_resource_inputs
 *0
config_proto 

CPU

GPU2*0J 8� *\
fWRU
S__inference_global_max_pooling1d_1_layer_call_and_return_conditional_losses_1739764�
dropout_15/PartitionedCallPartitionedCall/global_max_pooling1d_1/PartitionedCall:output:0*
Tin
2*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:����������* 
_read_only_resource_inputs
 *0
config_proto 

CPU

GPU2*0J 8� *P
fKRI
G__inference_dropout_15_layer_call_and_return_conditional_losses_1739863�
 dense_36/StatefulPartitionedCallStatefulPartitionedCall#dropout_15/PartitionedCall:output:0dense_36_1740152dense_36_1740154*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������
*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *N
fIRG
E__inference_dense_36_layer_call_and_return_conditional_losses_1739876x
IdentityIdentity)dense_36/StatefulPartitionedCall:output:0^NoOp*
T0*'
_output_shapes
:���������
�
NoOpNoOp!^conv1d_1/StatefulPartitionedCall!^dense_36/StatefulPartitionedCall$^embedding_6/StatefulPartitionedCallK^text_vectorization_14/string_lookup_42/hash_table_Lookup/LookupTableFindV2*"
_acd_function_control_output(*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime*8
_input_shapes'
%:���������: : : : : : : : : 2D
 conv1d_1/StatefulPartitionedCall conv1d_1/StatefulPartitionedCall2D
 dense_36/StatefulPartitionedCall dense_36/StatefulPartitionedCall2J
#embedding_6/StatefulPartitionedCall#embedding_6/StatefulPartitionedCall2�
Jtext_vectorization_14/string_lookup_42/hash_table_Lookup/LookupTableFindV2Jtext_vectorization_14/string_lookup_42/hash_table_Lookup/LookupTableFindV2:Q M
'
_output_shapes
:���������
"
_user_specified_name
input_22:

_output_shapes
: :

_output_shapes
: :

_output_shapes
: 
�
T
8__inference_global_max_pooling1d_1_layer_call_fn_1740513

inputs
identity�
PartitionedCallPartitionedCallinputs*
Tin
2*
Tout
2*
_collective_manager_ids
 *0
_output_shapes
:������������������* 
_read_only_resource_inputs
 *0
config_proto 

CPU

GPU2*0J 8� *\
fWRU
S__inference_global_max_pooling1d_1_layer_call_and_return_conditional_losses_1739764i
IdentityIdentityPartitionedCall:output:0*
T0*0
_output_shapes
:������������������"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*<
_input_shapes+
):'���������������������������:e a
=
_output_shapes+
):'���������������������������
 
_user_specified_nameinputs
�

�
/__inference_sequential_21_layer_call_fn_1740091
input_22
unknown
	unknown_0	
	unknown_1
	unknown_2	
	unknown_3:	�Nd 
	unknown_4:d�
	unknown_5:	�
	unknown_6:	�

	unknown_7:

identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallinput_22unknown	unknown_0	unknown_1	unknown_2	unknown_3	unknown_4	unknown_5	unknown_6	unknown_7*
Tin
2
		*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������
*'
_read_only_resource_inputs	
	*0
config_proto 

CPU

GPU2*0J 8� *S
fNRL
J__inference_sequential_21_layer_call_and_return_conditional_losses_1740047o
IdentityIdentity StatefulPartitionedCall:output:0^NoOp*
T0*'
_output_shapes
:���������
`
NoOpNoOp^StatefulPartitionedCall*"
_acd_function_control_output(*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime*8
_input_shapes'
%:���������: : : : : : : : : 22
StatefulPartitionedCallStatefulPartitionedCall:Q M
'
_output_shapes
:���������
"
_user_specified_name
input_22:

_output_shapes
: :

_output_shapes
: :

_output_shapes
: 
�
e
,__inference_dropout_15_layer_call_fn_1740529

inputs
identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallinputs*
Tin
2*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:����������* 
_read_only_resource_inputs
 *0
config_proto 

CPU

GPU2*0J 8� *P
fKRI
G__inference_dropout_15_layer_call_and_return_conditional_losses_1739934p
IdentityIdentity StatefulPartitionedCall:output:0^NoOp*
T0*(
_output_shapes
:����������`
NoOpNoOp^StatefulPartitionedCall*"
_acd_function_control_output(*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime*'
_input_shapes
:����������22
StatefulPartitionedCallStatefulPartitionedCall:P L
(
_output_shapes
:����������
 
_user_specified_nameinputs
�a
�
#__inference__traced_restore_1740776
file_prefix:
'assignvariableop_embedding_6_embeddings:	�Nd9
"assignvariableop_1_conv1d_1_kernel:d�/
 assignvariableop_2_conv1d_1_bias:	�5
"assignvariableop_3_dense_36_kernel:	�
.
 assignvariableop_4_dense_36_bias:
&
assignvariableop_5_adam_iter:	 (
assignvariableop_6_adam_beta_1: (
assignvariableop_7_adam_beta_2: '
assignvariableop_8_adam_decay: /
%assignvariableop_9_adam_learning_rate: #
assignvariableop_10_total: #
assignvariableop_11_count: %
assignvariableop_12_total_1: %
assignvariableop_13_count_1: D
1assignvariableop_14_adam_embedding_6_embeddings_m:	�NdA
*assignvariableop_15_adam_conv1d_1_kernel_m:d�7
(assignvariableop_16_adam_conv1d_1_bias_m:	�=
*assignvariableop_17_adam_dense_36_kernel_m:	�
6
(assignvariableop_18_adam_dense_36_bias_m:
D
1assignvariableop_19_adam_embedding_6_embeddings_v:	�NdA
*assignvariableop_20_adam_conv1d_1_kernel_v:d�7
(assignvariableop_21_adam_conv1d_1_bias_v:	�=
*assignvariableop_22_adam_dense_36_kernel_v:	�
6
(assignvariableop_23_adam_dense_36_bias_v:

identity_25��AssignVariableOp�AssignVariableOp_1�AssignVariableOp_10�AssignVariableOp_11�AssignVariableOp_12�AssignVariableOp_13�AssignVariableOp_14�AssignVariableOp_15�AssignVariableOp_16�AssignVariableOp_17�AssignVariableOp_18�AssignVariableOp_19�AssignVariableOp_2�AssignVariableOp_20�AssignVariableOp_21�AssignVariableOp_22�AssignVariableOp_23�AssignVariableOp_3�AssignVariableOp_4�AssignVariableOp_5�AssignVariableOp_6�AssignVariableOp_7�AssignVariableOp_8�AssignVariableOp_9�
RestoreV2/tensor_namesConst"/device:CPU:0*
_output_shapes
:*
dtype0*�
value�B�B:layer_with_weights-0/embeddings/.ATTRIBUTES/VARIABLE_VALUEB6layer_with_weights-1/kernel/.ATTRIBUTES/VARIABLE_VALUEB4layer_with_weights-1/bias/.ATTRIBUTES/VARIABLE_VALUEB6layer_with_weights-2/kernel/.ATTRIBUTES/VARIABLE_VALUEB4layer_with_weights-2/bias/.ATTRIBUTES/VARIABLE_VALUEB)optimizer/iter/.ATTRIBUTES/VARIABLE_VALUEB+optimizer/beta_1/.ATTRIBUTES/VARIABLE_VALUEB+optimizer/beta_2/.ATTRIBUTES/VARIABLE_VALUEB*optimizer/decay/.ATTRIBUTES/VARIABLE_VALUEB2optimizer/learning_rate/.ATTRIBUTES/VARIABLE_VALUEB4keras_api/metrics/0/total/.ATTRIBUTES/VARIABLE_VALUEB4keras_api/metrics/0/count/.ATTRIBUTES/VARIABLE_VALUEB4keras_api/metrics/1/total/.ATTRIBUTES/VARIABLE_VALUEB4keras_api/metrics/1/count/.ATTRIBUTES/VARIABLE_VALUEBVlayer_with_weights-0/embeddings/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBRlayer_with_weights-1/kernel/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBPlayer_with_weights-1/bias/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBRlayer_with_weights-2/kernel/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBPlayer_with_weights-2/bias/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBVlayer_with_weights-0/embeddings/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBRlayer_with_weights-1/kernel/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBPlayer_with_weights-1/bias/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBRlayer_with_weights-2/kernel/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBPlayer_with_weights-2/bias/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEB_CHECKPOINTABLE_OBJECT_GRAPH�
RestoreV2/shape_and_slicesConst"/device:CPU:0*
_output_shapes
:*
dtype0*E
value<B:B B B B B B B B B B B B B B B B B B B B B B B B B �
	RestoreV2	RestoreV2file_prefixRestoreV2/tensor_names:output:0#RestoreV2/shape_and_slices:output:0"/device:CPU:0*x
_output_shapesf
d:::::::::::::::::::::::::*'
dtypes
2	[
IdentityIdentityRestoreV2:tensors:0"/device:CPU:0*
T0*
_output_shapes
:�
AssignVariableOpAssignVariableOp'assignvariableop_embedding_6_embeddingsIdentity:output:0"/device:CPU:0*
_output_shapes
 *
dtype0]

Identity_1IdentityRestoreV2:tensors:1"/device:CPU:0*
T0*
_output_shapes
:�
AssignVariableOp_1AssignVariableOp"assignvariableop_1_conv1d_1_kernelIdentity_1:output:0"/device:CPU:0*
_output_shapes
 *
dtype0]

Identity_2IdentityRestoreV2:tensors:2"/device:CPU:0*
T0*
_output_shapes
:�
AssignVariableOp_2AssignVariableOp assignvariableop_2_conv1d_1_biasIdentity_2:output:0"/device:CPU:0*
_output_shapes
 *
dtype0]

Identity_3IdentityRestoreV2:tensors:3"/device:CPU:0*
T0*
_output_shapes
:�
AssignVariableOp_3AssignVariableOp"assignvariableop_3_dense_36_kernelIdentity_3:output:0"/device:CPU:0*
_output_shapes
 *
dtype0]

Identity_4IdentityRestoreV2:tensors:4"/device:CPU:0*
T0*
_output_shapes
:�
AssignVariableOp_4AssignVariableOp assignvariableop_4_dense_36_biasIdentity_4:output:0"/device:CPU:0*
_output_shapes
 *
dtype0]

Identity_5IdentityRestoreV2:tensors:5"/device:CPU:0*
T0	*
_output_shapes
:�
AssignVariableOp_5AssignVariableOpassignvariableop_5_adam_iterIdentity_5:output:0"/device:CPU:0*
_output_shapes
 *
dtype0	]

Identity_6IdentityRestoreV2:tensors:6"/device:CPU:0*
T0*
_output_shapes
:�
AssignVariableOp_6AssignVariableOpassignvariableop_6_adam_beta_1Identity_6:output:0"/device:CPU:0*
_output_shapes
 *
dtype0]

Identity_7IdentityRestoreV2:tensors:7"/device:CPU:0*
T0*
_output_shapes
:�
AssignVariableOp_7AssignVariableOpassignvariableop_7_adam_beta_2Identity_7:output:0"/device:CPU:0*
_output_shapes
 *
dtype0]

Identity_8IdentityRestoreV2:tensors:8"/device:CPU:0*
T0*
_output_shapes
:�
AssignVariableOp_8AssignVariableOpassignvariableop_8_adam_decayIdentity_8:output:0"/device:CPU:0*
_output_shapes
 *
dtype0]

Identity_9IdentityRestoreV2:tensors:9"/device:CPU:0*
T0*
_output_shapes
:�
AssignVariableOp_9AssignVariableOp%assignvariableop_9_adam_learning_rateIdentity_9:output:0"/device:CPU:0*
_output_shapes
 *
dtype0_
Identity_10IdentityRestoreV2:tensors:10"/device:CPU:0*
T0*
_output_shapes
:�
AssignVariableOp_10AssignVariableOpassignvariableop_10_totalIdentity_10:output:0"/device:CPU:0*
_output_shapes
 *
dtype0_
Identity_11IdentityRestoreV2:tensors:11"/device:CPU:0*
T0*
_output_shapes
:�
AssignVariableOp_11AssignVariableOpassignvariableop_11_countIdentity_11:output:0"/device:CPU:0*
_output_shapes
 *
dtype0_
Identity_12IdentityRestoreV2:tensors:12"/device:CPU:0*
T0*
_output_shapes
:�
AssignVariableOp_12AssignVariableOpassignvariableop_12_total_1Identity_12:output:0"/device:CPU:0*
_output_shapes
 *
dtype0_
Identity_13IdentityRestoreV2:tensors:13"/device:CPU:0*
T0*
_output_shapes
:�
AssignVariableOp_13AssignVariableOpassignvariableop_13_count_1Identity_13:output:0"/device:CPU:0*
_output_shapes
 *
dtype0_
Identity_14IdentityRestoreV2:tensors:14"/device:CPU:0*
T0*
_output_shapes
:�
AssignVariableOp_14AssignVariableOp1assignvariableop_14_adam_embedding_6_embeddings_mIdentity_14:output:0"/device:CPU:0*
_output_shapes
 *
dtype0_
Identity_15IdentityRestoreV2:tensors:15"/device:CPU:0*
T0*
_output_shapes
:�
AssignVariableOp_15AssignVariableOp*assignvariableop_15_adam_conv1d_1_kernel_mIdentity_15:output:0"/device:CPU:0*
_output_shapes
 *
dtype0_
Identity_16IdentityRestoreV2:tensors:16"/device:CPU:0*
T0*
_output_shapes
:�
AssignVariableOp_16AssignVariableOp(assignvariableop_16_adam_conv1d_1_bias_mIdentity_16:output:0"/device:CPU:0*
_output_shapes
 *
dtype0_
Identity_17IdentityRestoreV2:tensors:17"/device:CPU:0*
T0*
_output_shapes
:�
AssignVariableOp_17AssignVariableOp*assignvariableop_17_adam_dense_36_kernel_mIdentity_17:output:0"/device:CPU:0*
_output_shapes
 *
dtype0_
Identity_18IdentityRestoreV2:tensors:18"/device:CPU:0*
T0*
_output_shapes
:�
AssignVariableOp_18AssignVariableOp(assignvariableop_18_adam_dense_36_bias_mIdentity_18:output:0"/device:CPU:0*
_output_shapes
 *
dtype0_
Identity_19IdentityRestoreV2:tensors:19"/device:CPU:0*
T0*
_output_shapes
:�
AssignVariableOp_19AssignVariableOp1assignvariableop_19_adam_embedding_6_embeddings_vIdentity_19:output:0"/device:CPU:0*
_output_shapes
 *
dtype0_
Identity_20IdentityRestoreV2:tensors:20"/device:CPU:0*
T0*
_output_shapes
:�
AssignVariableOp_20AssignVariableOp*assignvariableop_20_adam_conv1d_1_kernel_vIdentity_20:output:0"/device:CPU:0*
_output_shapes
 *
dtype0_
Identity_21IdentityRestoreV2:tensors:21"/device:CPU:0*
T0*
_output_shapes
:�
AssignVariableOp_21AssignVariableOp(assignvariableop_21_adam_conv1d_1_bias_vIdentity_21:output:0"/device:CPU:0*
_output_shapes
 *
dtype0_
Identity_22IdentityRestoreV2:tensors:22"/device:CPU:0*
T0*
_output_shapes
:�
AssignVariableOp_22AssignVariableOp*assignvariableop_22_adam_dense_36_kernel_vIdentity_22:output:0"/device:CPU:0*
_output_shapes
 *
dtype0_
Identity_23IdentityRestoreV2:tensors:23"/device:CPU:0*
T0*
_output_shapes
:�
AssignVariableOp_23AssignVariableOp(assignvariableop_23_adam_dense_36_bias_vIdentity_23:output:0"/device:CPU:0*
_output_shapes
 *
dtype01
NoOpNoOp"/device:CPU:0*
_output_shapes
 �
Identity_24Identityfile_prefix^AssignVariableOp^AssignVariableOp_1^AssignVariableOp_10^AssignVariableOp_11^AssignVariableOp_12^AssignVariableOp_13^AssignVariableOp_14^AssignVariableOp_15^AssignVariableOp_16^AssignVariableOp_17^AssignVariableOp_18^AssignVariableOp_19^AssignVariableOp_2^AssignVariableOp_20^AssignVariableOp_21^AssignVariableOp_22^AssignVariableOp_23^AssignVariableOp_3^AssignVariableOp_4^AssignVariableOp_5^AssignVariableOp_6^AssignVariableOp_7^AssignVariableOp_8^AssignVariableOp_9^NoOp"/device:CPU:0*
T0*
_output_shapes
: W
Identity_25IdentityIdentity_24:output:0^NoOp_1*
T0*
_output_shapes
: �
NoOp_1NoOp^AssignVariableOp^AssignVariableOp_1^AssignVariableOp_10^AssignVariableOp_11^AssignVariableOp_12^AssignVariableOp_13^AssignVariableOp_14^AssignVariableOp_15^AssignVariableOp_16^AssignVariableOp_17^AssignVariableOp_18^AssignVariableOp_19^AssignVariableOp_2^AssignVariableOp_20^AssignVariableOp_21^AssignVariableOp_22^AssignVariableOp_23^AssignVariableOp_3^AssignVariableOp_4^AssignVariableOp_5^AssignVariableOp_6^AssignVariableOp_7^AssignVariableOp_8^AssignVariableOp_9*"
_acd_function_control_output(*
_output_shapes
 "#
identity_25Identity_25:output:0*E
_input_shapes4
2: : : : : : : : : : : : : : : : : : : : : : : : : 2$
AssignVariableOpAssignVariableOp2(
AssignVariableOp_1AssignVariableOp_12*
AssignVariableOp_10AssignVariableOp_102*
AssignVariableOp_11AssignVariableOp_112*
AssignVariableOp_12AssignVariableOp_122*
AssignVariableOp_13AssignVariableOp_132*
AssignVariableOp_14AssignVariableOp_142*
AssignVariableOp_15AssignVariableOp_152*
AssignVariableOp_16AssignVariableOp_162*
AssignVariableOp_17AssignVariableOp_172*
AssignVariableOp_18AssignVariableOp_182*
AssignVariableOp_19AssignVariableOp_192(
AssignVariableOp_2AssignVariableOp_22*
AssignVariableOp_20AssignVariableOp_202*
AssignVariableOp_21AssignVariableOp_212*
AssignVariableOp_22AssignVariableOp_222*
AssignVariableOp_23AssignVariableOp_232(
AssignVariableOp_3AssignVariableOp_32(
AssignVariableOp_4AssignVariableOp_42(
AssignVariableOp_5AssignVariableOp_52(
AssignVariableOp_6AssignVariableOp_62(
AssignVariableOp_7AssignVariableOp_72(
AssignVariableOp_8AssignVariableOp_82(
AssignVariableOp_9AssignVariableOp_9:C ?

_output_shapes
: 
%
_user_specified_namefile_prefix
˃
�
J__inference_sequential_21_layer_call_and_return_conditional_losses_1740442

inputs[
Wtext_vectorization_14_string_lookup_42_hash_table_lookup_lookuptablefindv2_table_handle\
Xtext_vectorization_14_string_lookup_42_hash_table_lookup_lookuptablefindv2_default_value	2
.text_vectorization_14_string_lookup_42_equal_y5
1text_vectorization_14_string_lookup_42_selectv2_t	7
$embedding_6_embedding_lookup_1740407:	�NdK
4conv1d_1_conv1d_expanddims_1_readvariableop_resource:d�7
(conv1d_1_biasadd_readvariableop_resource:	�:
'dense_36_matmul_readvariableop_resource:	�
6
(dense_36_biasadd_readvariableop_resource:

identity��conv1d_1/BiasAdd/ReadVariableOp�+conv1d_1/Conv1D/ExpandDims_1/ReadVariableOp�dense_36/BiasAdd/ReadVariableOp�dense_36/MatMul/ReadVariableOp�embedding_6/embedding_lookup�Jtext_vectorization_14/string_lookup_42/hash_table_Lookup/LookupTableFindV2a
!text_vectorization_14/StringLowerStringLowerinputs*'
_output_shapes
:����������
(text_vectorization_14/StaticRegexReplaceStaticRegexReplace*text_vectorization_14/StringLower:output:0*'
_output_shapes
:���������*6
pattern+)[!"#$%&()\*\+,-\./:;<=>?@\[\\\]^_`{|}~\']*
rewrite �
text_vectorization_14/SqueezeSqueeze1text_vectorization_14/StaticRegexReplace:output:0*
T0*#
_output_shapes
:���������*
squeeze_dims

���������h
'text_vectorization_14/StringSplit/ConstConst*
_output_shapes
: *
dtype0*
valueB B �
/text_vectorization_14/StringSplit/StringSplitV2StringSplitV2&text_vectorization_14/Squeeze:output:00text_vectorization_14/StringSplit/Const:output:0*<
_output_shapes*
(:���������:���������:�
5text_vectorization_14/StringSplit/strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB"        �
7text_vectorization_14/StringSplit/strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB"       �
7text_vectorization_14/StringSplit/strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB"      �
/text_vectorization_14/StringSplit/strided_sliceStridedSlice9text_vectorization_14/StringSplit/StringSplitV2:indices:0>text_vectorization_14/StringSplit/strided_slice/stack:output:0@text_vectorization_14/StringSplit/strided_slice/stack_1:output:0@text_vectorization_14/StringSplit/strided_slice/stack_2:output:0*
Index0*
T0	*#
_output_shapes
:���������*

begin_mask*
end_mask*
shrink_axis_mask�
7text_vectorization_14/StringSplit/strided_slice_1/stackConst*
_output_shapes
:*
dtype0*
valueB: �
9text_vectorization_14/StringSplit/strided_slice_1/stack_1Const*
_output_shapes
:*
dtype0*
valueB:�
9text_vectorization_14/StringSplit/strided_slice_1/stack_2Const*
_output_shapes
:*
dtype0*
valueB:�
1text_vectorization_14/StringSplit/strided_slice_1StridedSlice7text_vectorization_14/StringSplit/StringSplitV2:shape:0@text_vectorization_14/StringSplit/strided_slice_1/stack:output:0Btext_vectorization_14/StringSplit/strided_slice_1/stack_1:output:0Btext_vectorization_14/StringSplit/strided_slice_1/stack_2:output:0*
Index0*
T0	*
_output_shapes
: *
shrink_axis_mask�
Xtext_vectorization_14/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/CastCast8text_vectorization_14/StringSplit/strided_slice:output:0*

DstT0*

SrcT0	*#
_output_shapes
:����������
Ztext_vectorization_14/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cast_1Cast:text_vectorization_14/StringSplit/strided_slice_1:output:0*

DstT0*

SrcT0	*
_output_shapes
: �
btext_vectorization_14/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/ShapeShape\text_vectorization_14/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cast:y:0*
T0*
_output_shapes
:�
btext_vectorization_14/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/ConstConst*
_output_shapes
:*
dtype0*
valueB: �
atext_vectorization_14/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/ProdProdktext_vectorization_14/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Shape:output:0ktext_vectorization_14/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Const:output:0*
T0*
_output_shapes
: �
ftext_vectorization_14/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Greater/yConst*
_output_shapes
: *
dtype0*
value	B : �
dtext_vectorization_14/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/GreaterGreaterjtext_vectorization_14/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Prod:output:0otext_vectorization_14/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Greater/y:output:0*
T0*
_output_shapes
: �
atext_vectorization_14/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/CastCasthtext_vectorization_14/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Greater:z:0*

DstT0*

SrcT0
*
_output_shapes
: �
dtext_vectorization_14/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Const_1Const*
_output_shapes
:*
dtype0*
valueB: �
`text_vectorization_14/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/MaxMax\text_vectorization_14/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cast:y:0mtext_vectorization_14/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Const_1:output:0*
T0*
_output_shapes
: �
btext_vectorization_14/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/add/yConst*
_output_shapes
: *
dtype0*
value	B :�
`text_vectorization_14/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/addAddV2itext_vectorization_14/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Max:output:0ktext_vectorization_14/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/add/y:output:0*
T0*
_output_shapes
: �
`text_vectorization_14/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/mulMuletext_vectorization_14/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Cast:y:0dtext_vectorization_14/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/add:z:0*
T0*
_output_shapes
: �
dtext_vectorization_14/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/MaximumMaximum^text_vectorization_14/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cast_1:y:0dtext_vectorization_14/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/mul:z:0*
T0*
_output_shapes
: �
dtext_vectorization_14/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/MinimumMinimum^text_vectorization_14/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cast_1:y:0htext_vectorization_14/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Maximum:z:0*
T0*
_output_shapes
: �
dtext_vectorization_14/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Const_2Const*
_output_shapes
: *
dtype0	*
valueB	 �
etext_vectorization_14/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/BincountBincount\text_vectorization_14/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cast:y:0htext_vectorization_14/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Minimum:z:0mtext_vectorization_14/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Const_2:output:0*
T0	*#
_output_shapes
:����������
_text_vectorization_14/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cumsum/axisConst*
_output_shapes
: *
dtype0*
value	B : �
Ztext_vectorization_14/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/CumsumCumsumltext_vectorization_14/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Bincount:bins:0htext_vectorization_14/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cumsum/axis:output:0*
T0	*#
_output_shapes
:����������
ctext_vectorization_14/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/concat/values_0Const*
_output_shapes
:*
dtype0	*
valueB	R �
_text_vectorization_14/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/concat/axisConst*
_output_shapes
: *
dtype0*
value	B : �
Ztext_vectorization_14/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/concatConcatV2ltext_vectorization_14/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/concat/values_0:output:0`text_vectorization_14/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cumsum:out:0htext_vectorization_14/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/concat/axis:output:0*
N*
T0	*#
_output_shapes
:����������
Jtext_vectorization_14/string_lookup_42/hash_table_Lookup/LookupTableFindV2LookupTableFindV2Wtext_vectorization_14_string_lookup_42_hash_table_lookup_lookuptablefindv2_table_handle8text_vectorization_14/StringSplit/StringSplitV2:values:0Xtext_vectorization_14_string_lookup_42_hash_table_lookup_lookuptablefindv2_default_value*	
Tin0*

Tout0	*#
_output_shapes
:����������
,text_vectorization_14/string_lookup_42/EqualEqual8text_vectorization_14/StringSplit/StringSplitV2:values:0.text_vectorization_14_string_lookup_42_equal_y*
T0*#
_output_shapes
:����������
/text_vectorization_14/string_lookup_42/SelectV2SelectV20text_vectorization_14/string_lookup_42/Equal:z:01text_vectorization_14_string_lookup_42_selectv2_tStext_vectorization_14/string_lookup_42/hash_table_Lookup/LookupTableFindV2:values:0*
T0	*#
_output_shapes
:����������
/text_vectorization_14/string_lookup_42/IdentityIdentity8text_vectorization_14/string_lookup_42/SelectV2:output:0*
T0	*#
_output_shapes
:���������t
2text_vectorization_14/RaggedToTensor/default_valueConst*
_output_shapes
: *
dtype0	*
value	B	 R �
*text_vectorization_14/RaggedToTensor/ConstConst*
_output_shapes
:*
dtype0	*%
valueB	"���������       �
9text_vectorization_14/RaggedToTensor/RaggedTensorToTensorRaggedTensorToTensor3text_vectorization_14/RaggedToTensor/Const:output:08text_vectorization_14/string_lookup_42/Identity:output:0;text_vectorization_14/RaggedToTensor/default_value:output:0:text_vectorization_14/StringSplit/strided_slice_1:output:08text_vectorization_14/StringSplit/strided_slice:output:0*
T0	*
Tindex0	*
Tshape0	*(
_output_shapes
:����������*
num_row_partition_tensors*7
row_partition_types 
FIRST_DIM_SIZEVALUE_ROWIDS�
embedding_6/embedding_lookupResourceGather$embedding_6_embedding_lookup_1740407Btext_vectorization_14/RaggedToTensor/RaggedTensorToTensor:result:0*
Tindices0	*7
_class-
+)loc:@embedding_6/embedding_lookup/1740407*,
_output_shapes
:����������d*
dtype0�
%embedding_6/embedding_lookup/IdentityIdentity%embedding_6/embedding_lookup:output:0*
T0*7
_class-
+)loc:@embedding_6/embedding_lookup/1740407*,
_output_shapes
:����������d�
'embedding_6/embedding_lookup/Identity_1Identity.embedding_6/embedding_lookup/Identity:output:0*
T0*,
_output_shapes
:����������di
conv1d_1/Conv1D/ExpandDims/dimConst*
_output_shapes
: *
dtype0*
valueB :
����������
conv1d_1/Conv1D/ExpandDims
ExpandDims0embedding_6/embedding_lookup/Identity_1:output:0'conv1d_1/Conv1D/ExpandDims/dim:output:0*
T0*0
_output_shapes
:����������d�
+conv1d_1/Conv1D/ExpandDims_1/ReadVariableOpReadVariableOp4conv1d_1_conv1d_expanddims_1_readvariableop_resource*#
_output_shapes
:d�*
dtype0b
 conv1d_1/Conv1D/ExpandDims_1/dimConst*
_output_shapes
: *
dtype0*
value	B : �
conv1d_1/Conv1D/ExpandDims_1
ExpandDims3conv1d_1/Conv1D/ExpandDims_1/ReadVariableOp:value:0)conv1d_1/Conv1D/ExpandDims_1/dim:output:0*
T0*'
_output_shapes
:d��
conv1d_1/Conv1DConv2D#conv1d_1/Conv1D/ExpandDims:output:0%conv1d_1/Conv1D/ExpandDims_1:output:0*
T0*1
_output_shapes
:�����������*
paddingVALID*
strides
�
conv1d_1/Conv1D/SqueezeSqueezeconv1d_1/Conv1D:output:0*
T0*-
_output_shapes
:�����������*
squeeze_dims

����������
conv1d_1/BiasAdd/ReadVariableOpReadVariableOp(conv1d_1_biasadd_readvariableop_resource*
_output_shapes	
:�*
dtype0�
conv1d_1/BiasAddBiasAdd conv1d_1/Conv1D/Squeeze:output:0'conv1d_1/BiasAdd/ReadVariableOp:value:0*
T0*-
_output_shapes
:�����������h
conv1d_1/ReluReluconv1d_1/BiasAdd:output:0*
T0*-
_output_shapes
:�����������n
,global_max_pooling1d_1/Max/reduction_indicesConst*
_output_shapes
: *
dtype0*
value	B :�
global_max_pooling1d_1/MaxMaxconv1d_1/Relu:activations:05global_max_pooling1d_1/Max/reduction_indices:output:0*
T0*(
_output_shapes
:����������]
dropout_15/dropout/ConstConst*
_output_shapes
: *
dtype0*
valueB
 *   @�
dropout_15/dropout/MulMul#global_max_pooling1d_1/Max:output:0!dropout_15/dropout/Const:output:0*
T0*(
_output_shapes
:����������k
dropout_15/dropout/ShapeShape#global_max_pooling1d_1/Max:output:0*
T0*
_output_shapes
:�
/dropout_15/dropout/random_uniform/RandomUniformRandomUniform!dropout_15/dropout/Shape:output:0*
T0*(
_output_shapes
:����������*
dtype0f
!dropout_15/dropout/GreaterEqual/yConst*
_output_shapes
: *
dtype0*
valueB
 *   ?�
dropout_15/dropout/GreaterEqualGreaterEqual8dropout_15/dropout/random_uniform/RandomUniform:output:0*dropout_15/dropout/GreaterEqual/y:output:0*
T0*(
_output_shapes
:�����������
dropout_15/dropout/CastCast#dropout_15/dropout/GreaterEqual:z:0*

DstT0*

SrcT0
*(
_output_shapes
:�����������
dropout_15/dropout/Mul_1Muldropout_15/dropout/Mul:z:0dropout_15/dropout/Cast:y:0*
T0*(
_output_shapes
:�����������
dense_36/MatMul/ReadVariableOpReadVariableOp'dense_36_matmul_readvariableop_resource*
_output_shapes
:	�
*
dtype0�
dense_36/MatMulMatMuldropout_15/dropout/Mul_1:z:0&dense_36/MatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������
�
dense_36/BiasAdd/ReadVariableOpReadVariableOp(dense_36_biasadd_readvariableop_resource*
_output_shapes
:
*
dtype0�
dense_36/BiasAddBiasAdddense_36/MatMul:product:0'dense_36/BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������
h
dense_36/SoftmaxSoftmaxdense_36/BiasAdd:output:0*
T0*'
_output_shapes
:���������
i
IdentityIdentitydense_36/Softmax:softmax:0^NoOp*
T0*'
_output_shapes
:���������
�
NoOpNoOp ^conv1d_1/BiasAdd/ReadVariableOp,^conv1d_1/Conv1D/ExpandDims_1/ReadVariableOp ^dense_36/BiasAdd/ReadVariableOp^dense_36/MatMul/ReadVariableOp^embedding_6/embedding_lookupK^text_vectorization_14/string_lookup_42/hash_table_Lookup/LookupTableFindV2*"
_acd_function_control_output(*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime*8
_input_shapes'
%:���������: : : : : : : : : 2B
conv1d_1/BiasAdd/ReadVariableOpconv1d_1/BiasAdd/ReadVariableOp2Z
+conv1d_1/Conv1D/ExpandDims_1/ReadVariableOp+conv1d_1/Conv1D/ExpandDims_1/ReadVariableOp2B
dense_36/BiasAdd/ReadVariableOpdense_36/BiasAdd/ReadVariableOp2@
dense_36/MatMul/ReadVariableOpdense_36/MatMul/ReadVariableOp2<
embedding_6/embedding_lookupembedding_6/embedding_lookup2�
Jtext_vectorization_14/string_lookup_42/hash_table_Lookup/LookupTableFindV2Jtext_vectorization_14/string_lookup_42/hash_table_Lookup/LookupTableFindV2:O K
'
_output_shapes
:���������
 
_user_specified_nameinputs:

_output_shapes
: :

_output_shapes
: :

_output_shapes
: 
�
�
E__inference_conv1d_1_layer_call_and_return_conditional_losses_1739851

inputsB
+conv1d_expanddims_1_readvariableop_resource:d�.
biasadd_readvariableop_resource:	�
identity��BiasAdd/ReadVariableOp�"Conv1D/ExpandDims_1/ReadVariableOp`
Conv1D/ExpandDims/dimConst*
_output_shapes
: *
dtype0*
valueB :
����������
Conv1D/ExpandDims
ExpandDimsinputsConv1D/ExpandDims/dim:output:0*
T0*0
_output_shapes
:����������d�
"Conv1D/ExpandDims_1/ReadVariableOpReadVariableOp+conv1d_expanddims_1_readvariableop_resource*#
_output_shapes
:d�*
dtype0Y
Conv1D/ExpandDims_1/dimConst*
_output_shapes
: *
dtype0*
value	B : �
Conv1D/ExpandDims_1
ExpandDims*Conv1D/ExpandDims_1/ReadVariableOp:value:0 Conv1D/ExpandDims_1/dim:output:0*
T0*'
_output_shapes
:d��
Conv1DConv2DConv1D/ExpandDims:output:0Conv1D/ExpandDims_1:output:0*
T0*1
_output_shapes
:�����������*
paddingVALID*
strides
�
Conv1D/SqueezeSqueezeConv1D:output:0*
T0*-
_output_shapes
:�����������*
squeeze_dims

���������s
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes	
:�*
dtype0�
BiasAddBiasAddConv1D/Squeeze:output:0BiasAdd/ReadVariableOp:value:0*
T0*-
_output_shapes
:�����������V
ReluReluBiasAdd:output:0*
T0*-
_output_shapes
:�����������g
IdentityIdentityRelu:activations:0^NoOp*
T0*-
_output_shapes
:������������
NoOpNoOp^BiasAdd/ReadVariableOp#^Conv1D/ExpandDims_1/ReadVariableOp*"
_acd_function_control_output(*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime*/
_input_shapes
:����������d: : 20
BiasAdd/ReadVariableOpBiasAdd/ReadVariableOp2H
"Conv1D/ExpandDims_1/ReadVariableOp"Conv1D/ExpandDims_1/ReadVariableOp:T P
,
_output_shapes
:����������d
 
_user_specified_nameinputs
�	
f
G__inference_dropout_15_layer_call_and_return_conditional_losses_1740546

inputs
identity�R
dropout/ConstConst*
_output_shapes
: *
dtype0*
valueB
 *   @e
dropout/MulMulinputsdropout/Const:output:0*
T0*(
_output_shapes
:����������C
dropout/ShapeShapeinputs*
T0*
_output_shapes
:�
$dropout/random_uniform/RandomUniformRandomUniformdropout/Shape:output:0*
T0*(
_output_shapes
:����������*
dtype0[
dropout/GreaterEqual/yConst*
_output_shapes
: *
dtype0*
valueB
 *   ?�
dropout/GreaterEqualGreaterEqual-dropout/random_uniform/RandomUniform:output:0dropout/GreaterEqual/y:output:0*
T0*(
_output_shapes
:����������p
dropout/CastCastdropout/GreaterEqual:z:0*

DstT0*

SrcT0
*(
_output_shapes
:����������j
dropout/Mul_1Muldropout/Mul:z:0dropout/Cast:y:0*
T0*(
_output_shapes
:����������Z
IdentityIdentitydropout/Mul_1:z:0*
T0*(
_output_shapes
:����������"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*'
_input_shapes
:����������:P L
(
_output_shapes
:����������
 
_user_specified_nameinputs
�
�
__inference_<lambda>_1740592:
6key_value_init1706868_lookuptableimportv2_table_handle2
.key_value_init1706868_lookuptableimportv2_keys4
0key_value_init1706868_lookuptableimportv2_values	
identity��)key_value_init1706868/LookupTableImportV2�
)key_value_init1706868/LookupTableImportV2LookupTableImportV26key_value_init1706868_lookuptableimportv2_table_handle.key_value_init1706868_lookuptableimportv2_keys0key_value_init1706868_lookuptableimportv2_values*	
Tin0*

Tout0	*
_output_shapes
 J
ConstConst*
_output_shapes
: *
dtype0*
valueB
 *  �?L
IdentityIdentityConst:output:0^NoOp*
T0*
_output_shapes
: r
NoOpNoOp*^key_value_init1706868/LookupTableImportV2*"
_acd_function_control_output(*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime*#
_input_shapes
: :�N:�N2V
)key_value_init1706868/LookupTableImportV2)key_value_init1706868/LookupTableImportV2:!

_output_shapes	
:�N:!

_output_shapes	
:�N
��
�
"__inference__wrapped_model_1739754
input_22i
esequential_21_text_vectorization_14_string_lookup_42_hash_table_lookup_lookuptablefindv2_table_handlej
fsequential_21_text_vectorization_14_string_lookup_42_hash_table_lookup_lookuptablefindv2_default_value	@
<sequential_21_text_vectorization_14_string_lookup_42_equal_yC
?sequential_21_text_vectorization_14_string_lookup_42_selectv2_t	E
2sequential_21_embedding_6_embedding_lookup_1739726:	�NdY
Bsequential_21_conv1d_1_conv1d_expanddims_1_readvariableop_resource:d�E
6sequential_21_conv1d_1_biasadd_readvariableop_resource:	�H
5sequential_21_dense_36_matmul_readvariableop_resource:	�
D
6sequential_21_dense_36_biasadd_readvariableop_resource:

identity��-sequential_21/conv1d_1/BiasAdd/ReadVariableOp�9sequential_21/conv1d_1/Conv1D/ExpandDims_1/ReadVariableOp�-sequential_21/dense_36/BiasAdd/ReadVariableOp�,sequential_21/dense_36/MatMul/ReadVariableOp�*sequential_21/embedding_6/embedding_lookup�Xsequential_21/text_vectorization_14/string_lookup_42/hash_table_Lookup/LookupTableFindV2q
/sequential_21/text_vectorization_14/StringLowerStringLowerinput_22*'
_output_shapes
:����������
6sequential_21/text_vectorization_14/StaticRegexReplaceStaticRegexReplace8sequential_21/text_vectorization_14/StringLower:output:0*'
_output_shapes
:���������*6
pattern+)[!"#$%&()\*\+,-\./:;<=>?@\[\\\]^_`{|}~\']*
rewrite �
+sequential_21/text_vectorization_14/SqueezeSqueeze?sequential_21/text_vectorization_14/StaticRegexReplace:output:0*
T0*#
_output_shapes
:���������*
squeeze_dims

���������v
5sequential_21/text_vectorization_14/StringSplit/ConstConst*
_output_shapes
: *
dtype0*
valueB B �
=sequential_21/text_vectorization_14/StringSplit/StringSplitV2StringSplitV24sequential_21/text_vectorization_14/Squeeze:output:0>sequential_21/text_vectorization_14/StringSplit/Const:output:0*<
_output_shapes*
(:���������:���������:�
Csequential_21/text_vectorization_14/StringSplit/strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB"        �
Esequential_21/text_vectorization_14/StringSplit/strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB"       �
Esequential_21/text_vectorization_14/StringSplit/strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB"      �
=sequential_21/text_vectorization_14/StringSplit/strided_sliceStridedSliceGsequential_21/text_vectorization_14/StringSplit/StringSplitV2:indices:0Lsequential_21/text_vectorization_14/StringSplit/strided_slice/stack:output:0Nsequential_21/text_vectorization_14/StringSplit/strided_slice/stack_1:output:0Nsequential_21/text_vectorization_14/StringSplit/strided_slice/stack_2:output:0*
Index0*
T0	*#
_output_shapes
:���������*

begin_mask*
end_mask*
shrink_axis_mask�
Esequential_21/text_vectorization_14/StringSplit/strided_slice_1/stackConst*
_output_shapes
:*
dtype0*
valueB: �
Gsequential_21/text_vectorization_14/StringSplit/strided_slice_1/stack_1Const*
_output_shapes
:*
dtype0*
valueB:�
Gsequential_21/text_vectorization_14/StringSplit/strided_slice_1/stack_2Const*
_output_shapes
:*
dtype0*
valueB:�
?sequential_21/text_vectorization_14/StringSplit/strided_slice_1StridedSliceEsequential_21/text_vectorization_14/StringSplit/StringSplitV2:shape:0Nsequential_21/text_vectorization_14/StringSplit/strided_slice_1/stack:output:0Psequential_21/text_vectorization_14/StringSplit/strided_slice_1/stack_1:output:0Psequential_21/text_vectorization_14/StringSplit/strided_slice_1/stack_2:output:0*
Index0*
T0	*
_output_shapes
: *
shrink_axis_mask�
fsequential_21/text_vectorization_14/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/CastCastFsequential_21/text_vectorization_14/StringSplit/strided_slice:output:0*

DstT0*

SrcT0	*#
_output_shapes
:����������
hsequential_21/text_vectorization_14/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cast_1CastHsequential_21/text_vectorization_14/StringSplit/strided_slice_1:output:0*

DstT0*

SrcT0	*
_output_shapes
: �
psequential_21/text_vectorization_14/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/ShapeShapejsequential_21/text_vectorization_14/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cast:y:0*
T0*
_output_shapes
:�
psequential_21/text_vectorization_14/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/ConstConst*
_output_shapes
:*
dtype0*
valueB: �
osequential_21/text_vectorization_14/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/ProdProdysequential_21/text_vectorization_14/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Shape:output:0ysequential_21/text_vectorization_14/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Const:output:0*
T0*
_output_shapes
: �
tsequential_21/text_vectorization_14/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Greater/yConst*
_output_shapes
: *
dtype0*
value	B : �
rsequential_21/text_vectorization_14/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/GreaterGreaterxsequential_21/text_vectorization_14/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Prod:output:0}sequential_21/text_vectorization_14/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Greater/y:output:0*
T0*
_output_shapes
: �
osequential_21/text_vectorization_14/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/CastCastvsequential_21/text_vectorization_14/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Greater:z:0*

DstT0*

SrcT0
*
_output_shapes
: �
rsequential_21/text_vectorization_14/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Const_1Const*
_output_shapes
:*
dtype0*
valueB: �
nsequential_21/text_vectorization_14/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/MaxMaxjsequential_21/text_vectorization_14/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cast:y:0{sequential_21/text_vectorization_14/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Const_1:output:0*
T0*
_output_shapes
: �
psequential_21/text_vectorization_14/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/add/yConst*
_output_shapes
: *
dtype0*
value	B :�
nsequential_21/text_vectorization_14/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/addAddV2wsequential_21/text_vectorization_14/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Max:output:0ysequential_21/text_vectorization_14/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/add/y:output:0*
T0*
_output_shapes
: �
nsequential_21/text_vectorization_14/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/mulMulssequential_21/text_vectorization_14/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Cast:y:0rsequential_21/text_vectorization_14/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/add:z:0*
T0*
_output_shapes
: �
rsequential_21/text_vectorization_14/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/MaximumMaximumlsequential_21/text_vectorization_14/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cast_1:y:0rsequential_21/text_vectorization_14/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/mul:z:0*
T0*
_output_shapes
: �
rsequential_21/text_vectorization_14/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/MinimumMinimumlsequential_21/text_vectorization_14/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cast_1:y:0vsequential_21/text_vectorization_14/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Maximum:z:0*
T0*
_output_shapes
: �
rsequential_21/text_vectorization_14/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Const_2Const*
_output_shapes
: *
dtype0	*
valueB	 �
ssequential_21/text_vectorization_14/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/BincountBincountjsequential_21/text_vectorization_14/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cast:y:0vsequential_21/text_vectorization_14/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Minimum:z:0{sequential_21/text_vectorization_14/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Const_2:output:0*
T0	*#
_output_shapes
:����������
msequential_21/text_vectorization_14/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cumsum/axisConst*
_output_shapes
: *
dtype0*
value	B : �
hsequential_21/text_vectorization_14/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/CumsumCumsumzsequential_21/text_vectorization_14/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Bincount:bins:0vsequential_21/text_vectorization_14/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cumsum/axis:output:0*
T0	*#
_output_shapes
:����������
qsequential_21/text_vectorization_14/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/concat/values_0Const*
_output_shapes
:*
dtype0	*
valueB	R �
msequential_21/text_vectorization_14/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/concat/axisConst*
_output_shapes
: *
dtype0*
value	B : �
hsequential_21/text_vectorization_14/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/concatConcatV2zsequential_21/text_vectorization_14/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/concat/values_0:output:0nsequential_21/text_vectorization_14/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cumsum:out:0vsequential_21/text_vectorization_14/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/concat/axis:output:0*
N*
T0	*#
_output_shapes
:����������
Xsequential_21/text_vectorization_14/string_lookup_42/hash_table_Lookup/LookupTableFindV2LookupTableFindV2esequential_21_text_vectorization_14_string_lookup_42_hash_table_lookup_lookuptablefindv2_table_handleFsequential_21/text_vectorization_14/StringSplit/StringSplitV2:values:0fsequential_21_text_vectorization_14_string_lookup_42_hash_table_lookup_lookuptablefindv2_default_value*	
Tin0*

Tout0	*#
_output_shapes
:����������
:sequential_21/text_vectorization_14/string_lookup_42/EqualEqualFsequential_21/text_vectorization_14/StringSplit/StringSplitV2:values:0<sequential_21_text_vectorization_14_string_lookup_42_equal_y*
T0*#
_output_shapes
:����������
=sequential_21/text_vectorization_14/string_lookup_42/SelectV2SelectV2>sequential_21/text_vectorization_14/string_lookup_42/Equal:z:0?sequential_21_text_vectorization_14_string_lookup_42_selectv2_tasequential_21/text_vectorization_14/string_lookup_42/hash_table_Lookup/LookupTableFindV2:values:0*
T0	*#
_output_shapes
:����������
=sequential_21/text_vectorization_14/string_lookup_42/IdentityIdentityFsequential_21/text_vectorization_14/string_lookup_42/SelectV2:output:0*
T0	*#
_output_shapes
:����������
@sequential_21/text_vectorization_14/RaggedToTensor/default_valueConst*
_output_shapes
: *
dtype0	*
value	B	 R �
8sequential_21/text_vectorization_14/RaggedToTensor/ConstConst*
_output_shapes
:*
dtype0	*%
valueB	"���������       �
Gsequential_21/text_vectorization_14/RaggedToTensor/RaggedTensorToTensorRaggedTensorToTensorAsequential_21/text_vectorization_14/RaggedToTensor/Const:output:0Fsequential_21/text_vectorization_14/string_lookup_42/Identity:output:0Isequential_21/text_vectorization_14/RaggedToTensor/default_value:output:0Hsequential_21/text_vectorization_14/StringSplit/strided_slice_1:output:0Fsequential_21/text_vectorization_14/StringSplit/strided_slice:output:0*
T0	*
Tindex0	*
Tshape0	*(
_output_shapes
:����������*
num_row_partition_tensors*7
row_partition_types 
FIRST_DIM_SIZEVALUE_ROWIDS�
*sequential_21/embedding_6/embedding_lookupResourceGather2sequential_21_embedding_6_embedding_lookup_1739726Psequential_21/text_vectorization_14/RaggedToTensor/RaggedTensorToTensor:result:0*
Tindices0	*E
_class;
97loc:@sequential_21/embedding_6/embedding_lookup/1739726*,
_output_shapes
:����������d*
dtype0�
3sequential_21/embedding_6/embedding_lookup/IdentityIdentity3sequential_21/embedding_6/embedding_lookup:output:0*
T0*E
_class;
97loc:@sequential_21/embedding_6/embedding_lookup/1739726*,
_output_shapes
:����������d�
5sequential_21/embedding_6/embedding_lookup/Identity_1Identity<sequential_21/embedding_6/embedding_lookup/Identity:output:0*
T0*,
_output_shapes
:����������dw
,sequential_21/conv1d_1/Conv1D/ExpandDims/dimConst*
_output_shapes
: *
dtype0*
valueB :
����������
(sequential_21/conv1d_1/Conv1D/ExpandDims
ExpandDims>sequential_21/embedding_6/embedding_lookup/Identity_1:output:05sequential_21/conv1d_1/Conv1D/ExpandDims/dim:output:0*
T0*0
_output_shapes
:����������d�
9sequential_21/conv1d_1/Conv1D/ExpandDims_1/ReadVariableOpReadVariableOpBsequential_21_conv1d_1_conv1d_expanddims_1_readvariableop_resource*#
_output_shapes
:d�*
dtype0p
.sequential_21/conv1d_1/Conv1D/ExpandDims_1/dimConst*
_output_shapes
: *
dtype0*
value	B : �
*sequential_21/conv1d_1/Conv1D/ExpandDims_1
ExpandDimsAsequential_21/conv1d_1/Conv1D/ExpandDims_1/ReadVariableOp:value:07sequential_21/conv1d_1/Conv1D/ExpandDims_1/dim:output:0*
T0*'
_output_shapes
:d��
sequential_21/conv1d_1/Conv1DConv2D1sequential_21/conv1d_1/Conv1D/ExpandDims:output:03sequential_21/conv1d_1/Conv1D/ExpandDims_1:output:0*
T0*1
_output_shapes
:�����������*
paddingVALID*
strides
�
%sequential_21/conv1d_1/Conv1D/SqueezeSqueeze&sequential_21/conv1d_1/Conv1D:output:0*
T0*-
_output_shapes
:�����������*
squeeze_dims

����������
-sequential_21/conv1d_1/BiasAdd/ReadVariableOpReadVariableOp6sequential_21_conv1d_1_biasadd_readvariableop_resource*
_output_shapes	
:�*
dtype0�
sequential_21/conv1d_1/BiasAddBiasAdd.sequential_21/conv1d_1/Conv1D/Squeeze:output:05sequential_21/conv1d_1/BiasAdd/ReadVariableOp:value:0*
T0*-
_output_shapes
:������������
sequential_21/conv1d_1/ReluRelu'sequential_21/conv1d_1/BiasAdd:output:0*
T0*-
_output_shapes
:�����������|
:sequential_21/global_max_pooling1d_1/Max/reduction_indicesConst*
_output_shapes
: *
dtype0*
value	B :�
(sequential_21/global_max_pooling1d_1/MaxMax)sequential_21/conv1d_1/Relu:activations:0Csequential_21/global_max_pooling1d_1/Max/reduction_indices:output:0*
T0*(
_output_shapes
:�����������
!sequential_21/dropout_15/IdentityIdentity1sequential_21/global_max_pooling1d_1/Max:output:0*
T0*(
_output_shapes
:�����������
,sequential_21/dense_36/MatMul/ReadVariableOpReadVariableOp5sequential_21_dense_36_matmul_readvariableop_resource*
_output_shapes
:	�
*
dtype0�
sequential_21/dense_36/MatMulMatMul*sequential_21/dropout_15/Identity:output:04sequential_21/dense_36/MatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������
�
-sequential_21/dense_36/BiasAdd/ReadVariableOpReadVariableOp6sequential_21_dense_36_biasadd_readvariableop_resource*
_output_shapes
:
*
dtype0�
sequential_21/dense_36/BiasAddBiasAdd'sequential_21/dense_36/MatMul:product:05sequential_21/dense_36/BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������
�
sequential_21/dense_36/SoftmaxSoftmax'sequential_21/dense_36/BiasAdd:output:0*
T0*'
_output_shapes
:���������
w
IdentityIdentity(sequential_21/dense_36/Softmax:softmax:0^NoOp*
T0*'
_output_shapes
:���������
�
NoOpNoOp.^sequential_21/conv1d_1/BiasAdd/ReadVariableOp:^sequential_21/conv1d_1/Conv1D/ExpandDims_1/ReadVariableOp.^sequential_21/dense_36/BiasAdd/ReadVariableOp-^sequential_21/dense_36/MatMul/ReadVariableOp+^sequential_21/embedding_6/embedding_lookupY^sequential_21/text_vectorization_14/string_lookup_42/hash_table_Lookup/LookupTableFindV2*"
_acd_function_control_output(*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime*8
_input_shapes'
%:���������: : : : : : : : : 2^
-sequential_21/conv1d_1/BiasAdd/ReadVariableOp-sequential_21/conv1d_1/BiasAdd/ReadVariableOp2v
9sequential_21/conv1d_1/Conv1D/ExpandDims_1/ReadVariableOp9sequential_21/conv1d_1/Conv1D/ExpandDims_1/ReadVariableOp2^
-sequential_21/dense_36/BiasAdd/ReadVariableOp-sequential_21/dense_36/BiasAdd/ReadVariableOp2\
,sequential_21/dense_36/MatMul/ReadVariableOp,sequential_21/dense_36/MatMul/ReadVariableOp2X
*sequential_21/embedding_6/embedding_lookup*sequential_21/embedding_6/embedding_lookup2�
Xsequential_21/text_vectorization_14/string_lookup_42/hash_table_Lookup/LookupTableFindV2Xsequential_21/text_vectorization_14/string_lookup_42/hash_table_Lookup/LookupTableFindV2:Q M
'
_output_shapes
:���������
"
_user_specified_name
input_22:

_output_shapes
: :

_output_shapes
: :

_output_shapes
: 
�
�
 __inference__initializer_1740579:
6key_value_init1706868_lookuptableimportv2_table_handle2
.key_value_init1706868_lookuptableimportv2_keys4
0key_value_init1706868_lookuptableimportv2_values	
identity��)key_value_init1706868/LookupTableImportV2�
)key_value_init1706868/LookupTableImportV2LookupTableImportV26key_value_init1706868_lookuptableimportv2_table_handle.key_value_init1706868_lookuptableimportv2_keys0key_value_init1706868_lookuptableimportv2_values*	
Tin0*

Tout0	*
_output_shapes
 G
ConstConst*
_output_shapes
: *
dtype0*
value	B :L
IdentityIdentityConst:output:0^NoOp*
T0*
_output_shapes
: r
NoOpNoOp*^key_value_init1706868/LookupTableImportV2*"
_acd_function_control_output(*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime*#
_input_shapes
: :�N:�N2V
)key_value_init1706868/LookupTableImportV2)key_value_init1706868/LookupTableImportV2:!

_output_shapes	
:�N:!

_output_shapes	
:�N
�o
�
J__inference_sequential_21_layer_call_and_return_conditional_losses_1739883

inputs[
Wtext_vectorization_14_string_lookup_42_hash_table_lookup_lookuptablefindv2_table_handle\
Xtext_vectorization_14_string_lookup_42_hash_table_lookup_lookuptablefindv2_default_value	2
.text_vectorization_14_string_lookup_42_equal_y5
1text_vectorization_14_string_lookup_42_selectv2_t	&
embedding_6_1739832:	�Nd'
conv1d_1_1739852:d�
conv1d_1_1739854:	�#
dense_36_1739877:	�

dense_36_1739879:

identity�� conv1d_1/StatefulPartitionedCall� dense_36/StatefulPartitionedCall�#embedding_6/StatefulPartitionedCall�Jtext_vectorization_14/string_lookup_42/hash_table_Lookup/LookupTableFindV2a
!text_vectorization_14/StringLowerStringLowerinputs*'
_output_shapes
:����������
(text_vectorization_14/StaticRegexReplaceStaticRegexReplace*text_vectorization_14/StringLower:output:0*'
_output_shapes
:���������*6
pattern+)[!"#$%&()\*\+,-\./:;<=>?@\[\\\]^_`{|}~\']*
rewrite �
text_vectorization_14/SqueezeSqueeze1text_vectorization_14/StaticRegexReplace:output:0*
T0*#
_output_shapes
:���������*
squeeze_dims

���������h
'text_vectorization_14/StringSplit/ConstConst*
_output_shapes
: *
dtype0*
valueB B �
/text_vectorization_14/StringSplit/StringSplitV2StringSplitV2&text_vectorization_14/Squeeze:output:00text_vectorization_14/StringSplit/Const:output:0*<
_output_shapes*
(:���������:���������:�
5text_vectorization_14/StringSplit/strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB"        �
7text_vectorization_14/StringSplit/strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB"       �
7text_vectorization_14/StringSplit/strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB"      �
/text_vectorization_14/StringSplit/strided_sliceStridedSlice9text_vectorization_14/StringSplit/StringSplitV2:indices:0>text_vectorization_14/StringSplit/strided_slice/stack:output:0@text_vectorization_14/StringSplit/strided_slice/stack_1:output:0@text_vectorization_14/StringSplit/strided_slice/stack_2:output:0*
Index0*
T0	*#
_output_shapes
:���������*

begin_mask*
end_mask*
shrink_axis_mask�
7text_vectorization_14/StringSplit/strided_slice_1/stackConst*
_output_shapes
:*
dtype0*
valueB: �
9text_vectorization_14/StringSplit/strided_slice_1/stack_1Const*
_output_shapes
:*
dtype0*
valueB:�
9text_vectorization_14/StringSplit/strided_slice_1/stack_2Const*
_output_shapes
:*
dtype0*
valueB:�
1text_vectorization_14/StringSplit/strided_slice_1StridedSlice7text_vectorization_14/StringSplit/StringSplitV2:shape:0@text_vectorization_14/StringSplit/strided_slice_1/stack:output:0Btext_vectorization_14/StringSplit/strided_slice_1/stack_1:output:0Btext_vectorization_14/StringSplit/strided_slice_1/stack_2:output:0*
Index0*
T0	*
_output_shapes
: *
shrink_axis_mask�
Xtext_vectorization_14/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/CastCast8text_vectorization_14/StringSplit/strided_slice:output:0*

DstT0*

SrcT0	*#
_output_shapes
:����������
Ztext_vectorization_14/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cast_1Cast:text_vectorization_14/StringSplit/strided_slice_1:output:0*

DstT0*

SrcT0	*
_output_shapes
: �
btext_vectorization_14/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/ShapeShape\text_vectorization_14/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cast:y:0*
T0*
_output_shapes
:�
btext_vectorization_14/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/ConstConst*
_output_shapes
:*
dtype0*
valueB: �
atext_vectorization_14/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/ProdProdktext_vectorization_14/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Shape:output:0ktext_vectorization_14/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Const:output:0*
T0*
_output_shapes
: �
ftext_vectorization_14/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Greater/yConst*
_output_shapes
: *
dtype0*
value	B : �
dtext_vectorization_14/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/GreaterGreaterjtext_vectorization_14/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Prod:output:0otext_vectorization_14/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Greater/y:output:0*
T0*
_output_shapes
: �
atext_vectorization_14/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/CastCasthtext_vectorization_14/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Greater:z:0*

DstT0*

SrcT0
*
_output_shapes
: �
dtext_vectorization_14/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Const_1Const*
_output_shapes
:*
dtype0*
valueB: �
`text_vectorization_14/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/MaxMax\text_vectorization_14/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cast:y:0mtext_vectorization_14/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Const_1:output:0*
T0*
_output_shapes
: �
btext_vectorization_14/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/add/yConst*
_output_shapes
: *
dtype0*
value	B :�
`text_vectorization_14/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/addAddV2itext_vectorization_14/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Max:output:0ktext_vectorization_14/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/add/y:output:0*
T0*
_output_shapes
: �
`text_vectorization_14/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/mulMuletext_vectorization_14/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Cast:y:0dtext_vectorization_14/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/add:z:0*
T0*
_output_shapes
: �
dtext_vectorization_14/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/MaximumMaximum^text_vectorization_14/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cast_1:y:0dtext_vectorization_14/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/mul:z:0*
T0*
_output_shapes
: �
dtext_vectorization_14/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/MinimumMinimum^text_vectorization_14/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cast_1:y:0htext_vectorization_14/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Maximum:z:0*
T0*
_output_shapes
: �
dtext_vectorization_14/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Const_2Const*
_output_shapes
: *
dtype0	*
valueB	 �
etext_vectorization_14/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/BincountBincount\text_vectorization_14/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cast:y:0htext_vectorization_14/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Minimum:z:0mtext_vectorization_14/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Const_2:output:0*
T0	*#
_output_shapes
:����������
_text_vectorization_14/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cumsum/axisConst*
_output_shapes
: *
dtype0*
value	B : �
Ztext_vectorization_14/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/CumsumCumsumltext_vectorization_14/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Bincount:bins:0htext_vectorization_14/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cumsum/axis:output:0*
T0	*#
_output_shapes
:����������
ctext_vectorization_14/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/concat/values_0Const*
_output_shapes
:*
dtype0	*
valueB	R �
_text_vectorization_14/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/concat/axisConst*
_output_shapes
: *
dtype0*
value	B : �
Ztext_vectorization_14/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/concatConcatV2ltext_vectorization_14/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/concat/values_0:output:0`text_vectorization_14/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cumsum:out:0htext_vectorization_14/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/concat/axis:output:0*
N*
T0	*#
_output_shapes
:����������
Jtext_vectorization_14/string_lookup_42/hash_table_Lookup/LookupTableFindV2LookupTableFindV2Wtext_vectorization_14_string_lookup_42_hash_table_lookup_lookuptablefindv2_table_handle8text_vectorization_14/StringSplit/StringSplitV2:values:0Xtext_vectorization_14_string_lookup_42_hash_table_lookup_lookuptablefindv2_default_value*	
Tin0*

Tout0	*#
_output_shapes
:����������
,text_vectorization_14/string_lookup_42/EqualEqual8text_vectorization_14/StringSplit/StringSplitV2:values:0.text_vectorization_14_string_lookup_42_equal_y*
T0*#
_output_shapes
:����������
/text_vectorization_14/string_lookup_42/SelectV2SelectV20text_vectorization_14/string_lookup_42/Equal:z:01text_vectorization_14_string_lookup_42_selectv2_tStext_vectorization_14/string_lookup_42/hash_table_Lookup/LookupTableFindV2:values:0*
T0	*#
_output_shapes
:����������
/text_vectorization_14/string_lookup_42/IdentityIdentity8text_vectorization_14/string_lookup_42/SelectV2:output:0*
T0	*#
_output_shapes
:���������t
2text_vectorization_14/RaggedToTensor/default_valueConst*
_output_shapes
: *
dtype0	*
value	B	 R �
*text_vectorization_14/RaggedToTensor/ConstConst*
_output_shapes
:*
dtype0	*%
valueB	"���������       �
9text_vectorization_14/RaggedToTensor/RaggedTensorToTensorRaggedTensorToTensor3text_vectorization_14/RaggedToTensor/Const:output:08text_vectorization_14/string_lookup_42/Identity:output:0;text_vectorization_14/RaggedToTensor/default_value:output:0:text_vectorization_14/StringSplit/strided_slice_1:output:08text_vectorization_14/StringSplit/strided_slice:output:0*
T0	*
Tindex0	*
Tshape0	*(
_output_shapes
:����������*
num_row_partition_tensors*7
row_partition_types 
FIRST_DIM_SIZEVALUE_ROWIDS�
#embedding_6/StatefulPartitionedCallStatefulPartitionedCallBtext_vectorization_14/RaggedToTensor/RaggedTensorToTensor:result:0embedding_6_1739832*
Tin
2	*
Tout
2*
_collective_manager_ids
 *,
_output_shapes
:����������d*#
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *Q
fLRJ
H__inference_embedding_6_layer_call_and_return_conditional_losses_1739831�
 conv1d_1/StatefulPartitionedCallStatefulPartitionedCall,embedding_6/StatefulPartitionedCall:output:0conv1d_1_1739852conv1d_1_1739854*
Tin
2*
Tout
2*
_collective_manager_ids
 *-
_output_shapes
:�����������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *N
fIRG
E__inference_conv1d_1_layer_call_and_return_conditional_losses_1739851�
&global_max_pooling1d_1/PartitionedCallPartitionedCall)conv1d_1/StatefulPartitionedCall:output:0*
Tin
2*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:����������* 
_read_only_resource_inputs
 *0
config_proto 

CPU

GPU2*0J 8� *\
fWRU
S__inference_global_max_pooling1d_1_layer_call_and_return_conditional_losses_1739764�
dropout_15/PartitionedCallPartitionedCall/global_max_pooling1d_1/PartitionedCall:output:0*
Tin
2*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:����������* 
_read_only_resource_inputs
 *0
config_proto 

CPU

GPU2*0J 8� *P
fKRI
G__inference_dropout_15_layer_call_and_return_conditional_losses_1739863�
 dense_36/StatefulPartitionedCallStatefulPartitionedCall#dropout_15/PartitionedCall:output:0dense_36_1739877dense_36_1739879*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������
*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *N
fIRG
E__inference_dense_36_layer_call_and_return_conditional_losses_1739876x
IdentityIdentity)dense_36/StatefulPartitionedCall:output:0^NoOp*
T0*'
_output_shapes
:���������
�
NoOpNoOp!^conv1d_1/StatefulPartitionedCall!^dense_36/StatefulPartitionedCall$^embedding_6/StatefulPartitionedCallK^text_vectorization_14/string_lookup_42/hash_table_Lookup/LookupTableFindV2*"
_acd_function_control_output(*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime*8
_input_shapes'
%:���������: : : : : : : : : 2D
 conv1d_1/StatefulPartitionedCall conv1d_1/StatefulPartitionedCall2D
 dense_36/StatefulPartitionedCall dense_36/StatefulPartitionedCall2J
#embedding_6/StatefulPartitionedCall#embedding_6/StatefulPartitionedCall2�
Jtext_vectorization_14/string_lookup_42/hash_table_Lookup/LookupTableFindV2Jtext_vectorization_14/string_lookup_42/hash_table_Lookup/LookupTableFindV2:O K
'
_output_shapes
:���������
 
_user_specified_nameinputs:

_output_shapes
: :

_output_shapes
: :

_output_shapes
: 
�
o
S__inference_global_max_pooling1d_1_layer_call_and_return_conditional_losses_1740519

inputs
identityW
Max/reduction_indicesConst*
_output_shapes
: *
dtype0*
value	B :m
MaxMaxinputsMax/reduction_indices:output:0*
T0*0
_output_shapes
:������������������]
IdentityIdentityMax:output:0*
T0*0
_output_shapes
:������������������"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*<
_input_shapes+
):'���������������������������:e a
=
_output_shapes+
):'���������������������������
 
_user_specified_nameinputs
�	
�
%__inference_signature_wrapper_1740467
input_22
unknown
	unknown_0	
	unknown_1
	unknown_2	
	unknown_3:	�Nd 
	unknown_4:d�
	unknown_5:	�
	unknown_6:	�

	unknown_7:

identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallinput_22unknown	unknown_0	unknown_1	unknown_2	unknown_3	unknown_4	unknown_5	unknown_6	unknown_7*
Tin
2
		*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������
*'
_read_only_resource_inputs	
	*0
config_proto 

CPU

GPU2*0J 8� *+
f&R$
"__inference__wrapped_model_1739754o
IdentityIdentity StatefulPartitionedCall:output:0^NoOp*
T0*'
_output_shapes
:���������
`
NoOpNoOp^StatefulPartitionedCall*"
_acd_function_control_output(*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime*8
_input_shapes'
%:���������: : : : : : : : : 22
StatefulPartitionedCallStatefulPartitionedCall:Q M
'
_output_shapes
:���������
"
_user_specified_name
input_22:

_output_shapes
: :

_output_shapes
: :

_output_shapes
: 
�

�
E__inference_dense_36_layer_call_and_return_conditional_losses_1740566

inputs1
matmul_readvariableop_resource:	�
-
biasadd_readvariableop_resource:

identity��BiasAdd/ReadVariableOp�MatMul/ReadVariableOpu
MatMul/ReadVariableOpReadVariableOpmatmul_readvariableop_resource*
_output_shapes
:	�
*
dtype0i
MatMulMatMulinputsMatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������
r
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes
:
*
dtype0v
BiasAddBiasAddMatMul:product:0BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������
V
SoftmaxSoftmaxBiasAdd:output:0*
T0*'
_output_shapes
:���������
`
IdentityIdentitySoftmax:softmax:0^NoOp*
T0*'
_output_shapes
:���������
w
NoOpNoOp^BiasAdd/ReadVariableOp^MatMul/ReadVariableOp*"
_acd_function_control_output(*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime*+
_input_shapes
:����������: : 20
BiasAdd/ReadVariableOpBiasAdd/ReadVariableOp2.
MatMul/ReadVariableOpMatMul/ReadVariableOp:P L
(
_output_shapes
:����������
 
_user_specified_nameinputs
�
�
*__inference_conv1d_1_layer_call_fn_1740492

inputs
unknown:d�
	unknown_0:	�
identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallinputsunknown	unknown_0*
Tin
2*
Tout
2*
_collective_manager_ids
 *-
_output_shapes
:�����������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *N
fIRG
E__inference_conv1d_1_layer_call_and_return_conditional_losses_1739851u
IdentityIdentity StatefulPartitionedCall:output:0^NoOp*
T0*-
_output_shapes
:�����������`
NoOpNoOp^StatefulPartitionedCall*"
_acd_function_control_output(*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime*/
_input_shapes
:����������d: : 22
StatefulPartitionedCallStatefulPartitionedCall:T P
,
_output_shapes
:����������d
 
_user_specified_nameinputs
�
.
__inference__destroyer_1740584
identityG
ConstConst*
_output_shapes
: *
dtype0*
value	B :E
IdentityIdentityConst:output:0*
T0*
_output_shapes
: "
identityIdentity:output:0*(
_construction_contextkEagerRuntime*
_input_shapes 
�
o
S__inference_global_max_pooling1d_1_layer_call_and_return_conditional_losses_1739764

inputs
identityW
Max/reduction_indicesConst*
_output_shapes
: *
dtype0*
value	B :m
MaxMaxinputsMax/reduction_indices:output:0*
T0*0
_output_shapes
:������������������]
IdentityIdentityMax:output:0*
T0*0
_output_shapes
:������������������"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*<
_input_shapes+
):'���������������������������:e a
=
_output_shapes+
):'���������������������������
 
_user_specified_nameinputs
�	
f
G__inference_dropout_15_layer_call_and_return_conditional_losses_1739934

inputs
identity�R
dropout/ConstConst*
_output_shapes
: *
dtype0*
valueB
 *   @e
dropout/MulMulinputsdropout/Const:output:0*
T0*(
_output_shapes
:����������C
dropout/ShapeShapeinputs*
T0*
_output_shapes
:�
$dropout/random_uniform/RandomUniformRandomUniformdropout/Shape:output:0*
T0*(
_output_shapes
:����������*
dtype0[
dropout/GreaterEqual/yConst*
_output_shapes
: *
dtype0*
valueB
 *   ?�
dropout/GreaterEqualGreaterEqual-dropout/random_uniform/RandomUniform:output:0dropout/GreaterEqual/y:output:0*
T0*(
_output_shapes
:����������p
dropout/CastCastdropout/GreaterEqual:z:0*

DstT0*

SrcT0
*(
_output_shapes
:����������j
dropout/Mul_1Muldropout/Mul:z:0dropout/Cast:y:0*
T0*(
_output_shapes
:����������Z
IdentityIdentitydropout/Mul_1:z:0*
T0*(
_output_shapes
:����������"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*'
_input_shapes
:����������:P L
(
_output_shapes
:����������
 
_user_specified_nameinputs
�
<
__inference__creator_1740571
identity��
hash_tableo

hash_tableHashTableV2*
_output_shapes
: *
	key_dtype0*
shared_name	1706869*
value_dtype0	W
IdentityIdentityhash_table:table_handle:0^NoOp*
T0*
_output_shapes
: S
NoOpNoOp^hash_table*"
_acd_function_control_output(*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime*
_input_shapes 2

hash_table
hash_table
�7
�

 __inference__traced_save_1740694
file_prefix5
1savev2_embedding_6_embeddings_read_readvariableop.
*savev2_conv1d_1_kernel_read_readvariableop,
(savev2_conv1d_1_bias_read_readvariableop.
*savev2_dense_36_kernel_read_readvariableop,
(savev2_dense_36_bias_read_readvariableop(
$savev2_adam_iter_read_readvariableop	*
&savev2_adam_beta_1_read_readvariableop*
&savev2_adam_beta_2_read_readvariableop)
%savev2_adam_decay_read_readvariableop1
-savev2_adam_learning_rate_read_readvariableop$
 savev2_total_read_readvariableop$
 savev2_count_read_readvariableop&
"savev2_total_1_read_readvariableop&
"savev2_count_1_read_readvariableop<
8savev2_adam_embedding_6_embeddings_m_read_readvariableop5
1savev2_adam_conv1d_1_kernel_m_read_readvariableop3
/savev2_adam_conv1d_1_bias_m_read_readvariableop5
1savev2_adam_dense_36_kernel_m_read_readvariableop3
/savev2_adam_dense_36_bias_m_read_readvariableop<
8savev2_adam_embedding_6_embeddings_v_read_readvariableop5
1savev2_adam_conv1d_1_kernel_v_read_readvariableop3
/savev2_adam_conv1d_1_bias_v_read_readvariableop5
1savev2_adam_dense_36_kernel_v_read_readvariableop3
/savev2_adam_dense_36_bias_v_read_readvariableop
savev2_const_5

identity_1��MergeV2Checkpointsw
StaticRegexFullMatchStaticRegexFullMatchfile_prefix"/device:CPU:**
_output_shapes
: *
pattern
^s3://.*Z
ConstConst"/device:CPU:**
_output_shapes
: *
dtype0*
valueB B.parta
Const_1Const"/device:CPU:**
_output_shapes
: *
dtype0*
valueB B
_temp/part�
SelectSelectStaticRegexFullMatch:output:0Const:output:0Const_1:output:0"/device:CPU:**
T0*
_output_shapes
: f

StringJoin
StringJoinfile_prefixSelect:output:0"/device:CPU:**
N*
_output_shapes
: L

num_shardsConst*
_output_shapes
: *
dtype0*
value	B :f
ShardedFilename/shardConst"/device:CPU:0*
_output_shapes
: *
dtype0*
value	B : �
ShardedFilenameShardedFilenameStringJoin:output:0ShardedFilename/shard:output:0num_shards:output:0"/device:CPU:0*
_output_shapes
: �
SaveV2/tensor_namesConst"/device:CPU:0*
_output_shapes
:*
dtype0*�
value�B�B:layer_with_weights-0/embeddings/.ATTRIBUTES/VARIABLE_VALUEB6layer_with_weights-1/kernel/.ATTRIBUTES/VARIABLE_VALUEB4layer_with_weights-1/bias/.ATTRIBUTES/VARIABLE_VALUEB6layer_with_weights-2/kernel/.ATTRIBUTES/VARIABLE_VALUEB4layer_with_weights-2/bias/.ATTRIBUTES/VARIABLE_VALUEB)optimizer/iter/.ATTRIBUTES/VARIABLE_VALUEB+optimizer/beta_1/.ATTRIBUTES/VARIABLE_VALUEB+optimizer/beta_2/.ATTRIBUTES/VARIABLE_VALUEB*optimizer/decay/.ATTRIBUTES/VARIABLE_VALUEB2optimizer/learning_rate/.ATTRIBUTES/VARIABLE_VALUEB4keras_api/metrics/0/total/.ATTRIBUTES/VARIABLE_VALUEB4keras_api/metrics/0/count/.ATTRIBUTES/VARIABLE_VALUEB4keras_api/metrics/1/total/.ATTRIBUTES/VARIABLE_VALUEB4keras_api/metrics/1/count/.ATTRIBUTES/VARIABLE_VALUEBVlayer_with_weights-0/embeddings/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBRlayer_with_weights-1/kernel/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBPlayer_with_weights-1/bias/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBRlayer_with_weights-2/kernel/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBPlayer_with_weights-2/bias/.OPTIMIZER_SLOT/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBVlayer_with_weights-0/embeddings/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBRlayer_with_weights-1/kernel/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBPlayer_with_weights-1/bias/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBRlayer_with_weights-2/kernel/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBPlayer_with_weights-2/bias/.OPTIMIZER_SLOT/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEB_CHECKPOINTABLE_OBJECT_GRAPH�
SaveV2/shape_and_slicesConst"/device:CPU:0*
_output_shapes
:*
dtype0*E
value<B:B B B B B B B B B B B B B B B B B B B B B B B B B �

SaveV2SaveV2ShardedFilename:filename:0SaveV2/tensor_names:output:0 SaveV2/shape_and_slices:output:01savev2_embedding_6_embeddings_read_readvariableop*savev2_conv1d_1_kernel_read_readvariableop(savev2_conv1d_1_bias_read_readvariableop*savev2_dense_36_kernel_read_readvariableop(savev2_dense_36_bias_read_readvariableop$savev2_adam_iter_read_readvariableop&savev2_adam_beta_1_read_readvariableop&savev2_adam_beta_2_read_readvariableop%savev2_adam_decay_read_readvariableop-savev2_adam_learning_rate_read_readvariableop savev2_total_read_readvariableop savev2_count_read_readvariableop"savev2_total_1_read_readvariableop"savev2_count_1_read_readvariableop8savev2_adam_embedding_6_embeddings_m_read_readvariableop1savev2_adam_conv1d_1_kernel_m_read_readvariableop/savev2_adam_conv1d_1_bias_m_read_readvariableop1savev2_adam_dense_36_kernel_m_read_readvariableop/savev2_adam_dense_36_bias_m_read_readvariableop8savev2_adam_embedding_6_embeddings_v_read_readvariableop1savev2_adam_conv1d_1_kernel_v_read_readvariableop/savev2_adam_conv1d_1_bias_v_read_readvariableop1savev2_adam_dense_36_kernel_v_read_readvariableop/savev2_adam_dense_36_bias_v_read_readvariableopsavev2_const_5"/device:CPU:0*
_output_shapes
 *'
dtypes
2	�
&MergeV2Checkpoints/checkpoint_prefixesPackShardedFilename:filename:0^SaveV2"/device:CPU:0*
N*
T0*
_output_shapes
:�
MergeV2CheckpointsMergeV2Checkpoints/MergeV2Checkpoints/checkpoint_prefixes:output:0file_prefix"/device:CPU:0*
_output_shapes
 f
IdentityIdentityfile_prefix^MergeV2Checkpoints"/device:CPU:0*
T0*
_output_shapes
: Q

Identity_1IdentityIdentity:output:0^NoOp*
T0*
_output_shapes
: [
NoOpNoOp^MergeV2Checkpoints*"
_acd_function_control_output(*
_output_shapes
 "!

identity_1Identity_1:output:0*�
_input_shapes�
�: :	�Nd:d�:�:	�
:
: : : : : : : : : :	�Nd:d�:�:	�
:
:	�Nd:d�:�:	�
:
: 2(
MergeV2CheckpointsMergeV2Checkpoints:C ?

_output_shapes
: 
%
_user_specified_namefile_prefix:%!

_output_shapes
:	�Nd:)%
#
_output_shapes
:d�:!

_output_shapes	
:�:%!

_output_shapes
:	�
: 

_output_shapes
:
:

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :	

_output_shapes
: :


_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :%!

_output_shapes
:	�Nd:)%
#
_output_shapes
:d�:!

_output_shapes	
:�:%!

_output_shapes
:	�
: 

_output_shapes
:
:%!

_output_shapes
:	�Nd:)%
#
_output_shapes
:d�:!

_output_shapes	
:�:%!

_output_shapes
:	�
: 

_output_shapes
:
:

_output_shapes
: 
�

�
/__inference_sequential_21_layer_call_fn_1740254

inputs
unknown
	unknown_0	
	unknown_1
	unknown_2	
	unknown_3:	�Nd 
	unknown_4:d�
	unknown_5:	�
	unknown_6:	�

	unknown_7:

identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallinputsunknown	unknown_0	unknown_1	unknown_2	unknown_3	unknown_4	unknown_5	unknown_6	unknown_7*
Tin
2
		*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������
*'
_read_only_resource_inputs	
	*0
config_proto 

CPU

GPU2*0J 8� *S
fNRL
J__inference_sequential_21_layer_call_and_return_conditional_losses_1739883o
IdentityIdentity StatefulPartitionedCall:output:0^NoOp*
T0*'
_output_shapes
:���������
`
NoOpNoOp^StatefulPartitionedCall*"
_acd_function_control_output(*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime*8
_input_shapes'
%:���������: : : : : : : : : 22
StatefulPartitionedCallStatefulPartitionedCall:O K
'
_output_shapes
:���������
 
_user_specified_nameinputs:

_output_shapes
: :

_output_shapes
: :

_output_shapes
: 
�{
�
J__inference_sequential_21_layer_call_and_return_conditional_losses_1740356

inputs[
Wtext_vectorization_14_string_lookup_42_hash_table_lookup_lookuptablefindv2_table_handle\
Xtext_vectorization_14_string_lookup_42_hash_table_lookup_lookuptablefindv2_default_value	2
.text_vectorization_14_string_lookup_42_equal_y5
1text_vectorization_14_string_lookup_42_selectv2_t	7
$embedding_6_embedding_lookup_1740328:	�NdK
4conv1d_1_conv1d_expanddims_1_readvariableop_resource:d�7
(conv1d_1_biasadd_readvariableop_resource:	�:
'dense_36_matmul_readvariableop_resource:	�
6
(dense_36_biasadd_readvariableop_resource:

identity��conv1d_1/BiasAdd/ReadVariableOp�+conv1d_1/Conv1D/ExpandDims_1/ReadVariableOp�dense_36/BiasAdd/ReadVariableOp�dense_36/MatMul/ReadVariableOp�embedding_6/embedding_lookup�Jtext_vectorization_14/string_lookup_42/hash_table_Lookup/LookupTableFindV2a
!text_vectorization_14/StringLowerStringLowerinputs*'
_output_shapes
:����������
(text_vectorization_14/StaticRegexReplaceStaticRegexReplace*text_vectorization_14/StringLower:output:0*'
_output_shapes
:���������*6
pattern+)[!"#$%&()\*\+,-\./:;<=>?@\[\\\]^_`{|}~\']*
rewrite �
text_vectorization_14/SqueezeSqueeze1text_vectorization_14/StaticRegexReplace:output:0*
T0*#
_output_shapes
:���������*
squeeze_dims

���������h
'text_vectorization_14/StringSplit/ConstConst*
_output_shapes
: *
dtype0*
valueB B �
/text_vectorization_14/StringSplit/StringSplitV2StringSplitV2&text_vectorization_14/Squeeze:output:00text_vectorization_14/StringSplit/Const:output:0*<
_output_shapes*
(:���������:���������:�
5text_vectorization_14/StringSplit/strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB"        �
7text_vectorization_14/StringSplit/strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB"       �
7text_vectorization_14/StringSplit/strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB"      �
/text_vectorization_14/StringSplit/strided_sliceStridedSlice9text_vectorization_14/StringSplit/StringSplitV2:indices:0>text_vectorization_14/StringSplit/strided_slice/stack:output:0@text_vectorization_14/StringSplit/strided_slice/stack_1:output:0@text_vectorization_14/StringSplit/strided_slice/stack_2:output:0*
Index0*
T0	*#
_output_shapes
:���������*

begin_mask*
end_mask*
shrink_axis_mask�
7text_vectorization_14/StringSplit/strided_slice_1/stackConst*
_output_shapes
:*
dtype0*
valueB: �
9text_vectorization_14/StringSplit/strided_slice_1/stack_1Const*
_output_shapes
:*
dtype0*
valueB:�
9text_vectorization_14/StringSplit/strided_slice_1/stack_2Const*
_output_shapes
:*
dtype0*
valueB:�
1text_vectorization_14/StringSplit/strided_slice_1StridedSlice7text_vectorization_14/StringSplit/StringSplitV2:shape:0@text_vectorization_14/StringSplit/strided_slice_1/stack:output:0Btext_vectorization_14/StringSplit/strided_slice_1/stack_1:output:0Btext_vectorization_14/StringSplit/strided_slice_1/stack_2:output:0*
Index0*
T0	*
_output_shapes
: *
shrink_axis_mask�
Xtext_vectorization_14/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/CastCast8text_vectorization_14/StringSplit/strided_slice:output:0*

DstT0*

SrcT0	*#
_output_shapes
:����������
Ztext_vectorization_14/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cast_1Cast:text_vectorization_14/StringSplit/strided_slice_1:output:0*

DstT0*

SrcT0	*
_output_shapes
: �
btext_vectorization_14/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/ShapeShape\text_vectorization_14/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cast:y:0*
T0*
_output_shapes
:�
btext_vectorization_14/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/ConstConst*
_output_shapes
:*
dtype0*
valueB: �
atext_vectorization_14/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/ProdProdktext_vectorization_14/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Shape:output:0ktext_vectorization_14/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Const:output:0*
T0*
_output_shapes
: �
ftext_vectorization_14/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Greater/yConst*
_output_shapes
: *
dtype0*
value	B : �
dtext_vectorization_14/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/GreaterGreaterjtext_vectorization_14/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Prod:output:0otext_vectorization_14/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Greater/y:output:0*
T0*
_output_shapes
: �
atext_vectorization_14/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/CastCasthtext_vectorization_14/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Greater:z:0*

DstT0*

SrcT0
*
_output_shapes
: �
dtext_vectorization_14/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Const_1Const*
_output_shapes
:*
dtype0*
valueB: �
`text_vectorization_14/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/MaxMax\text_vectorization_14/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cast:y:0mtext_vectorization_14/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Const_1:output:0*
T0*
_output_shapes
: �
btext_vectorization_14/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/add/yConst*
_output_shapes
: *
dtype0*
value	B :�
`text_vectorization_14/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/addAddV2itext_vectorization_14/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Max:output:0ktext_vectorization_14/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/add/y:output:0*
T0*
_output_shapes
: �
`text_vectorization_14/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/mulMuletext_vectorization_14/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Cast:y:0dtext_vectorization_14/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/add:z:0*
T0*
_output_shapes
: �
dtext_vectorization_14/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/MaximumMaximum^text_vectorization_14/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cast_1:y:0dtext_vectorization_14/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/mul:z:0*
T0*
_output_shapes
: �
dtext_vectorization_14/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/MinimumMinimum^text_vectorization_14/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cast_1:y:0htext_vectorization_14/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Maximum:z:0*
T0*
_output_shapes
: �
dtext_vectorization_14/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Const_2Const*
_output_shapes
: *
dtype0	*
valueB	 �
etext_vectorization_14/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/BincountBincount\text_vectorization_14/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cast:y:0htext_vectorization_14/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Minimum:z:0mtext_vectorization_14/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Const_2:output:0*
T0	*#
_output_shapes
:����������
_text_vectorization_14/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cumsum/axisConst*
_output_shapes
: *
dtype0*
value	B : �
Ztext_vectorization_14/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/CumsumCumsumltext_vectorization_14/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Bincount:bins:0htext_vectorization_14/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cumsum/axis:output:0*
T0	*#
_output_shapes
:����������
ctext_vectorization_14/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/concat/values_0Const*
_output_shapes
:*
dtype0	*
valueB	R �
_text_vectorization_14/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/concat/axisConst*
_output_shapes
: *
dtype0*
value	B : �
Ztext_vectorization_14/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/concatConcatV2ltext_vectorization_14/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/concat/values_0:output:0`text_vectorization_14/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cumsum:out:0htext_vectorization_14/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/concat/axis:output:0*
N*
T0	*#
_output_shapes
:����������
Jtext_vectorization_14/string_lookup_42/hash_table_Lookup/LookupTableFindV2LookupTableFindV2Wtext_vectorization_14_string_lookup_42_hash_table_lookup_lookuptablefindv2_table_handle8text_vectorization_14/StringSplit/StringSplitV2:values:0Xtext_vectorization_14_string_lookup_42_hash_table_lookup_lookuptablefindv2_default_value*	
Tin0*

Tout0	*#
_output_shapes
:����������
,text_vectorization_14/string_lookup_42/EqualEqual8text_vectorization_14/StringSplit/StringSplitV2:values:0.text_vectorization_14_string_lookup_42_equal_y*
T0*#
_output_shapes
:����������
/text_vectorization_14/string_lookup_42/SelectV2SelectV20text_vectorization_14/string_lookup_42/Equal:z:01text_vectorization_14_string_lookup_42_selectv2_tStext_vectorization_14/string_lookup_42/hash_table_Lookup/LookupTableFindV2:values:0*
T0	*#
_output_shapes
:����������
/text_vectorization_14/string_lookup_42/IdentityIdentity8text_vectorization_14/string_lookup_42/SelectV2:output:0*
T0	*#
_output_shapes
:���������t
2text_vectorization_14/RaggedToTensor/default_valueConst*
_output_shapes
: *
dtype0	*
value	B	 R �
*text_vectorization_14/RaggedToTensor/ConstConst*
_output_shapes
:*
dtype0	*%
valueB	"���������       �
9text_vectorization_14/RaggedToTensor/RaggedTensorToTensorRaggedTensorToTensor3text_vectorization_14/RaggedToTensor/Const:output:08text_vectorization_14/string_lookup_42/Identity:output:0;text_vectorization_14/RaggedToTensor/default_value:output:0:text_vectorization_14/StringSplit/strided_slice_1:output:08text_vectorization_14/StringSplit/strided_slice:output:0*
T0	*
Tindex0	*
Tshape0	*(
_output_shapes
:����������*
num_row_partition_tensors*7
row_partition_types 
FIRST_DIM_SIZEVALUE_ROWIDS�
embedding_6/embedding_lookupResourceGather$embedding_6_embedding_lookup_1740328Btext_vectorization_14/RaggedToTensor/RaggedTensorToTensor:result:0*
Tindices0	*7
_class-
+)loc:@embedding_6/embedding_lookup/1740328*,
_output_shapes
:����������d*
dtype0�
%embedding_6/embedding_lookup/IdentityIdentity%embedding_6/embedding_lookup:output:0*
T0*7
_class-
+)loc:@embedding_6/embedding_lookup/1740328*,
_output_shapes
:����������d�
'embedding_6/embedding_lookup/Identity_1Identity.embedding_6/embedding_lookup/Identity:output:0*
T0*,
_output_shapes
:����������di
conv1d_1/Conv1D/ExpandDims/dimConst*
_output_shapes
: *
dtype0*
valueB :
����������
conv1d_1/Conv1D/ExpandDims
ExpandDims0embedding_6/embedding_lookup/Identity_1:output:0'conv1d_1/Conv1D/ExpandDims/dim:output:0*
T0*0
_output_shapes
:����������d�
+conv1d_1/Conv1D/ExpandDims_1/ReadVariableOpReadVariableOp4conv1d_1_conv1d_expanddims_1_readvariableop_resource*#
_output_shapes
:d�*
dtype0b
 conv1d_1/Conv1D/ExpandDims_1/dimConst*
_output_shapes
: *
dtype0*
value	B : �
conv1d_1/Conv1D/ExpandDims_1
ExpandDims3conv1d_1/Conv1D/ExpandDims_1/ReadVariableOp:value:0)conv1d_1/Conv1D/ExpandDims_1/dim:output:0*
T0*'
_output_shapes
:d��
conv1d_1/Conv1DConv2D#conv1d_1/Conv1D/ExpandDims:output:0%conv1d_1/Conv1D/ExpandDims_1:output:0*
T0*1
_output_shapes
:�����������*
paddingVALID*
strides
�
conv1d_1/Conv1D/SqueezeSqueezeconv1d_1/Conv1D:output:0*
T0*-
_output_shapes
:�����������*
squeeze_dims

����������
conv1d_1/BiasAdd/ReadVariableOpReadVariableOp(conv1d_1_biasadd_readvariableop_resource*
_output_shapes	
:�*
dtype0�
conv1d_1/BiasAddBiasAdd conv1d_1/Conv1D/Squeeze:output:0'conv1d_1/BiasAdd/ReadVariableOp:value:0*
T0*-
_output_shapes
:�����������h
conv1d_1/ReluReluconv1d_1/BiasAdd:output:0*
T0*-
_output_shapes
:�����������n
,global_max_pooling1d_1/Max/reduction_indicesConst*
_output_shapes
: *
dtype0*
value	B :�
global_max_pooling1d_1/MaxMaxconv1d_1/Relu:activations:05global_max_pooling1d_1/Max/reduction_indices:output:0*
T0*(
_output_shapes
:����������w
dropout_15/IdentityIdentity#global_max_pooling1d_1/Max:output:0*
T0*(
_output_shapes
:�����������
dense_36/MatMul/ReadVariableOpReadVariableOp'dense_36_matmul_readvariableop_resource*
_output_shapes
:	�
*
dtype0�
dense_36/MatMulMatMuldropout_15/Identity:output:0&dense_36/MatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������
�
dense_36/BiasAdd/ReadVariableOpReadVariableOp(dense_36_biasadd_readvariableop_resource*
_output_shapes
:
*
dtype0�
dense_36/BiasAddBiasAdddense_36/MatMul:product:0'dense_36/BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������
h
dense_36/SoftmaxSoftmaxdense_36/BiasAdd:output:0*
T0*'
_output_shapes
:���������
i
IdentityIdentitydense_36/Softmax:softmax:0^NoOp*
T0*'
_output_shapes
:���������
�
NoOpNoOp ^conv1d_1/BiasAdd/ReadVariableOp,^conv1d_1/Conv1D/ExpandDims_1/ReadVariableOp ^dense_36/BiasAdd/ReadVariableOp^dense_36/MatMul/ReadVariableOp^embedding_6/embedding_lookupK^text_vectorization_14/string_lookup_42/hash_table_Lookup/LookupTableFindV2*"
_acd_function_control_output(*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime*8
_input_shapes'
%:���������: : : : : : : : : 2B
conv1d_1/BiasAdd/ReadVariableOpconv1d_1/BiasAdd/ReadVariableOp2Z
+conv1d_1/Conv1D/ExpandDims_1/ReadVariableOp+conv1d_1/Conv1D/ExpandDims_1/ReadVariableOp2B
dense_36/BiasAdd/ReadVariableOpdense_36/BiasAdd/ReadVariableOp2@
dense_36/MatMul/ReadVariableOpdense_36/MatMul/ReadVariableOp2<
embedding_6/embedding_lookupembedding_6/embedding_lookup2�
Jtext_vectorization_14/string_lookup_42/hash_table_Lookup/LookupTableFindV2Jtext_vectorization_14/string_lookup_42/hash_table_Lookup/LookupTableFindV2:O K
'
_output_shapes
:���������
 
_user_specified_nameinputs:

_output_shapes
: :

_output_shapes
: :

_output_shapes
: 
�
�
H__inference_embedding_6_layer_call_and_return_conditional_losses_1740483

inputs	+
embedding_lookup_1740477:	�Nd
identity��embedding_lookup�
embedding_lookupResourceGatherembedding_lookup_1740477inputs*
Tindices0	*+
_class!
loc:@embedding_lookup/1740477*,
_output_shapes
:����������d*
dtype0�
embedding_lookup/IdentityIdentityembedding_lookup:output:0*
T0*+
_class!
loc:@embedding_lookup/1740477*,
_output_shapes
:����������d�
embedding_lookup/Identity_1Identity"embedding_lookup/Identity:output:0*
T0*,
_output_shapes
:����������dx
IdentityIdentity$embedding_lookup/Identity_1:output:0^NoOp*
T0*,
_output_shapes
:����������dY
NoOpNoOp^embedding_lookup*"
_acd_function_control_output(*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime*)
_input_shapes
:����������: 2$
embedding_lookupembedding_lookup:P L
(
_output_shapes
:����������
 
_user_specified_nameinputs
�p
�
J__inference_sequential_21_layer_call_and_return_conditional_losses_1740225
input_22[
Wtext_vectorization_14_string_lookup_42_hash_table_lookup_lookuptablefindv2_table_handle\
Xtext_vectorization_14_string_lookup_42_hash_table_lookup_lookuptablefindv2_default_value	2
.text_vectorization_14_string_lookup_42_equal_y5
1text_vectorization_14_string_lookup_42_selectv2_t	&
embedding_6_1740209:	�Nd'
conv1d_1_1740212:d�
conv1d_1_1740214:	�#
dense_36_1740219:	�

dense_36_1740221:

identity�� conv1d_1/StatefulPartitionedCall� dense_36/StatefulPartitionedCall�"dropout_15/StatefulPartitionedCall�#embedding_6/StatefulPartitionedCall�Jtext_vectorization_14/string_lookup_42/hash_table_Lookup/LookupTableFindV2c
!text_vectorization_14/StringLowerStringLowerinput_22*'
_output_shapes
:����������
(text_vectorization_14/StaticRegexReplaceStaticRegexReplace*text_vectorization_14/StringLower:output:0*'
_output_shapes
:���������*6
pattern+)[!"#$%&()\*\+,-\./:;<=>?@\[\\\]^_`{|}~\']*
rewrite �
text_vectorization_14/SqueezeSqueeze1text_vectorization_14/StaticRegexReplace:output:0*
T0*#
_output_shapes
:���������*
squeeze_dims

���������h
'text_vectorization_14/StringSplit/ConstConst*
_output_shapes
: *
dtype0*
valueB B �
/text_vectorization_14/StringSplit/StringSplitV2StringSplitV2&text_vectorization_14/Squeeze:output:00text_vectorization_14/StringSplit/Const:output:0*<
_output_shapes*
(:���������:���������:�
5text_vectorization_14/StringSplit/strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB"        �
7text_vectorization_14/StringSplit/strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB"       �
7text_vectorization_14/StringSplit/strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB"      �
/text_vectorization_14/StringSplit/strided_sliceStridedSlice9text_vectorization_14/StringSplit/StringSplitV2:indices:0>text_vectorization_14/StringSplit/strided_slice/stack:output:0@text_vectorization_14/StringSplit/strided_slice/stack_1:output:0@text_vectorization_14/StringSplit/strided_slice/stack_2:output:0*
Index0*
T0	*#
_output_shapes
:���������*

begin_mask*
end_mask*
shrink_axis_mask�
7text_vectorization_14/StringSplit/strided_slice_1/stackConst*
_output_shapes
:*
dtype0*
valueB: �
9text_vectorization_14/StringSplit/strided_slice_1/stack_1Const*
_output_shapes
:*
dtype0*
valueB:�
9text_vectorization_14/StringSplit/strided_slice_1/stack_2Const*
_output_shapes
:*
dtype0*
valueB:�
1text_vectorization_14/StringSplit/strided_slice_1StridedSlice7text_vectorization_14/StringSplit/StringSplitV2:shape:0@text_vectorization_14/StringSplit/strided_slice_1/stack:output:0Btext_vectorization_14/StringSplit/strided_slice_1/stack_1:output:0Btext_vectorization_14/StringSplit/strided_slice_1/stack_2:output:0*
Index0*
T0	*
_output_shapes
: *
shrink_axis_mask�
Xtext_vectorization_14/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/CastCast8text_vectorization_14/StringSplit/strided_slice:output:0*

DstT0*

SrcT0	*#
_output_shapes
:����������
Ztext_vectorization_14/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cast_1Cast:text_vectorization_14/StringSplit/strided_slice_1:output:0*

DstT0*

SrcT0	*
_output_shapes
: �
btext_vectorization_14/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/ShapeShape\text_vectorization_14/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cast:y:0*
T0*
_output_shapes
:�
btext_vectorization_14/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/ConstConst*
_output_shapes
:*
dtype0*
valueB: �
atext_vectorization_14/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/ProdProdktext_vectorization_14/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Shape:output:0ktext_vectorization_14/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Const:output:0*
T0*
_output_shapes
: �
ftext_vectorization_14/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Greater/yConst*
_output_shapes
: *
dtype0*
value	B : �
dtext_vectorization_14/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/GreaterGreaterjtext_vectorization_14/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Prod:output:0otext_vectorization_14/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Greater/y:output:0*
T0*
_output_shapes
: �
atext_vectorization_14/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/CastCasthtext_vectorization_14/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Greater:z:0*

DstT0*

SrcT0
*
_output_shapes
: �
dtext_vectorization_14/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Const_1Const*
_output_shapes
:*
dtype0*
valueB: �
`text_vectorization_14/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/MaxMax\text_vectorization_14/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cast:y:0mtext_vectorization_14/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Const_1:output:0*
T0*
_output_shapes
: �
btext_vectorization_14/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/add/yConst*
_output_shapes
: *
dtype0*
value	B :�
`text_vectorization_14/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/addAddV2itext_vectorization_14/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Max:output:0ktext_vectorization_14/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/add/y:output:0*
T0*
_output_shapes
: �
`text_vectorization_14/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/mulMuletext_vectorization_14/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Cast:y:0dtext_vectorization_14/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/add:z:0*
T0*
_output_shapes
: �
dtext_vectorization_14/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/MaximumMaximum^text_vectorization_14/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cast_1:y:0dtext_vectorization_14/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/mul:z:0*
T0*
_output_shapes
: �
dtext_vectorization_14/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/MinimumMinimum^text_vectorization_14/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cast_1:y:0htext_vectorization_14/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Maximum:z:0*
T0*
_output_shapes
: �
dtext_vectorization_14/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Const_2Const*
_output_shapes
: *
dtype0	*
valueB	 �
etext_vectorization_14/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/BincountBincount\text_vectorization_14/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cast:y:0htext_vectorization_14/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Minimum:z:0mtext_vectorization_14/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Const_2:output:0*
T0	*#
_output_shapes
:����������
_text_vectorization_14/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cumsum/axisConst*
_output_shapes
: *
dtype0*
value	B : �
Ztext_vectorization_14/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/CumsumCumsumltext_vectorization_14/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Bincount:bins:0htext_vectorization_14/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cumsum/axis:output:0*
T0	*#
_output_shapes
:����������
ctext_vectorization_14/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/concat/values_0Const*
_output_shapes
:*
dtype0	*
valueB	R �
_text_vectorization_14/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/concat/axisConst*
_output_shapes
: *
dtype0*
value	B : �
Ztext_vectorization_14/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/concatConcatV2ltext_vectorization_14/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/concat/values_0:output:0`text_vectorization_14/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cumsum:out:0htext_vectorization_14/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/concat/axis:output:0*
N*
T0	*#
_output_shapes
:����������
Jtext_vectorization_14/string_lookup_42/hash_table_Lookup/LookupTableFindV2LookupTableFindV2Wtext_vectorization_14_string_lookup_42_hash_table_lookup_lookuptablefindv2_table_handle8text_vectorization_14/StringSplit/StringSplitV2:values:0Xtext_vectorization_14_string_lookup_42_hash_table_lookup_lookuptablefindv2_default_value*	
Tin0*

Tout0	*#
_output_shapes
:����������
,text_vectorization_14/string_lookup_42/EqualEqual8text_vectorization_14/StringSplit/StringSplitV2:values:0.text_vectorization_14_string_lookup_42_equal_y*
T0*#
_output_shapes
:����������
/text_vectorization_14/string_lookup_42/SelectV2SelectV20text_vectorization_14/string_lookup_42/Equal:z:01text_vectorization_14_string_lookup_42_selectv2_tStext_vectorization_14/string_lookup_42/hash_table_Lookup/LookupTableFindV2:values:0*
T0	*#
_output_shapes
:����������
/text_vectorization_14/string_lookup_42/IdentityIdentity8text_vectorization_14/string_lookup_42/SelectV2:output:0*
T0	*#
_output_shapes
:���������t
2text_vectorization_14/RaggedToTensor/default_valueConst*
_output_shapes
: *
dtype0	*
value	B	 R �
*text_vectorization_14/RaggedToTensor/ConstConst*
_output_shapes
:*
dtype0	*%
valueB	"���������       �
9text_vectorization_14/RaggedToTensor/RaggedTensorToTensorRaggedTensorToTensor3text_vectorization_14/RaggedToTensor/Const:output:08text_vectorization_14/string_lookup_42/Identity:output:0;text_vectorization_14/RaggedToTensor/default_value:output:0:text_vectorization_14/StringSplit/strided_slice_1:output:08text_vectorization_14/StringSplit/strided_slice:output:0*
T0	*
Tindex0	*
Tshape0	*(
_output_shapes
:����������*
num_row_partition_tensors*7
row_partition_types 
FIRST_DIM_SIZEVALUE_ROWIDS�
#embedding_6/StatefulPartitionedCallStatefulPartitionedCallBtext_vectorization_14/RaggedToTensor/RaggedTensorToTensor:result:0embedding_6_1740209*
Tin
2	*
Tout
2*
_collective_manager_ids
 *,
_output_shapes
:����������d*#
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *Q
fLRJ
H__inference_embedding_6_layer_call_and_return_conditional_losses_1739831�
 conv1d_1/StatefulPartitionedCallStatefulPartitionedCall,embedding_6/StatefulPartitionedCall:output:0conv1d_1_1740212conv1d_1_1740214*
Tin
2*
Tout
2*
_collective_manager_ids
 *-
_output_shapes
:�����������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *N
fIRG
E__inference_conv1d_1_layer_call_and_return_conditional_losses_1739851�
&global_max_pooling1d_1/PartitionedCallPartitionedCall)conv1d_1/StatefulPartitionedCall:output:0*
Tin
2*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:����������* 
_read_only_resource_inputs
 *0
config_proto 

CPU

GPU2*0J 8� *\
fWRU
S__inference_global_max_pooling1d_1_layer_call_and_return_conditional_losses_1739764�
"dropout_15/StatefulPartitionedCallStatefulPartitionedCall/global_max_pooling1d_1/PartitionedCall:output:0*
Tin
2*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:����������* 
_read_only_resource_inputs
 *0
config_proto 

CPU

GPU2*0J 8� *P
fKRI
G__inference_dropout_15_layer_call_and_return_conditional_losses_1739934�
 dense_36/StatefulPartitionedCallStatefulPartitionedCall+dropout_15/StatefulPartitionedCall:output:0dense_36_1740219dense_36_1740221*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������
*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *N
fIRG
E__inference_dense_36_layer_call_and_return_conditional_losses_1739876x
IdentityIdentity)dense_36/StatefulPartitionedCall:output:0^NoOp*
T0*'
_output_shapes
:���������
�
NoOpNoOp!^conv1d_1/StatefulPartitionedCall!^dense_36/StatefulPartitionedCall#^dropout_15/StatefulPartitionedCall$^embedding_6/StatefulPartitionedCallK^text_vectorization_14/string_lookup_42/hash_table_Lookup/LookupTableFindV2*"
_acd_function_control_output(*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime*8
_input_shapes'
%:���������: : : : : : : : : 2D
 conv1d_1/StatefulPartitionedCall conv1d_1/StatefulPartitionedCall2D
 dense_36/StatefulPartitionedCall dense_36/StatefulPartitionedCall2H
"dropout_15/StatefulPartitionedCall"dropout_15/StatefulPartitionedCall2J
#embedding_6/StatefulPartitionedCall#embedding_6/StatefulPartitionedCall2�
Jtext_vectorization_14/string_lookup_42/hash_table_Lookup/LookupTableFindV2Jtext_vectorization_14/string_lookup_42/hash_table_Lookup/LookupTableFindV2:Q M
'
_output_shapes
:���������
"
_user_specified_name
input_22:

_output_shapes
: :

_output_shapes
: :

_output_shapes
: 
�

�
/__inference_sequential_21_layer_call_fn_1740277

inputs
unknown
	unknown_0	
	unknown_1
	unknown_2	
	unknown_3:	�Nd 
	unknown_4:d�
	unknown_5:	�
	unknown_6:	�

	unknown_7:

identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallinputsunknown	unknown_0	unknown_1	unknown_2	unknown_3	unknown_4	unknown_5	unknown_6	unknown_7*
Tin
2
		*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������
*'
_read_only_resource_inputs	
	*0
config_proto 

CPU

GPU2*0J 8� *S
fNRL
J__inference_sequential_21_layer_call_and_return_conditional_losses_1740047o
IdentityIdentity StatefulPartitionedCall:output:0^NoOp*
T0*'
_output_shapes
:���������
`
NoOpNoOp^StatefulPartitionedCall*"
_acd_function_control_output(*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime*8
_input_shapes'
%:���������: : : : : : : : : 22
StatefulPartitionedCallStatefulPartitionedCall:O K
'
_output_shapes
:���������
 
_user_specified_nameinputs:

_output_shapes
: :

_output_shapes
: :

_output_shapes
: 
�p
�
J__inference_sequential_21_layer_call_and_return_conditional_losses_1740047

inputs[
Wtext_vectorization_14_string_lookup_42_hash_table_lookup_lookuptablefindv2_table_handle\
Xtext_vectorization_14_string_lookup_42_hash_table_lookup_lookuptablefindv2_default_value	2
.text_vectorization_14_string_lookup_42_equal_y5
1text_vectorization_14_string_lookup_42_selectv2_t	&
embedding_6_1740031:	�Nd'
conv1d_1_1740034:d�
conv1d_1_1740036:	�#
dense_36_1740041:	�

dense_36_1740043:

identity�� conv1d_1/StatefulPartitionedCall� dense_36/StatefulPartitionedCall�"dropout_15/StatefulPartitionedCall�#embedding_6/StatefulPartitionedCall�Jtext_vectorization_14/string_lookup_42/hash_table_Lookup/LookupTableFindV2a
!text_vectorization_14/StringLowerStringLowerinputs*'
_output_shapes
:����������
(text_vectorization_14/StaticRegexReplaceStaticRegexReplace*text_vectorization_14/StringLower:output:0*'
_output_shapes
:���������*6
pattern+)[!"#$%&()\*\+,-\./:;<=>?@\[\\\]^_`{|}~\']*
rewrite �
text_vectorization_14/SqueezeSqueeze1text_vectorization_14/StaticRegexReplace:output:0*
T0*#
_output_shapes
:���������*
squeeze_dims

���������h
'text_vectorization_14/StringSplit/ConstConst*
_output_shapes
: *
dtype0*
valueB B �
/text_vectorization_14/StringSplit/StringSplitV2StringSplitV2&text_vectorization_14/Squeeze:output:00text_vectorization_14/StringSplit/Const:output:0*<
_output_shapes*
(:���������:���������:�
5text_vectorization_14/StringSplit/strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB"        �
7text_vectorization_14/StringSplit/strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB"       �
7text_vectorization_14/StringSplit/strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB"      �
/text_vectorization_14/StringSplit/strided_sliceStridedSlice9text_vectorization_14/StringSplit/StringSplitV2:indices:0>text_vectorization_14/StringSplit/strided_slice/stack:output:0@text_vectorization_14/StringSplit/strided_slice/stack_1:output:0@text_vectorization_14/StringSplit/strided_slice/stack_2:output:0*
Index0*
T0	*#
_output_shapes
:���������*

begin_mask*
end_mask*
shrink_axis_mask�
7text_vectorization_14/StringSplit/strided_slice_1/stackConst*
_output_shapes
:*
dtype0*
valueB: �
9text_vectorization_14/StringSplit/strided_slice_1/stack_1Const*
_output_shapes
:*
dtype0*
valueB:�
9text_vectorization_14/StringSplit/strided_slice_1/stack_2Const*
_output_shapes
:*
dtype0*
valueB:�
1text_vectorization_14/StringSplit/strided_slice_1StridedSlice7text_vectorization_14/StringSplit/StringSplitV2:shape:0@text_vectorization_14/StringSplit/strided_slice_1/stack:output:0Btext_vectorization_14/StringSplit/strided_slice_1/stack_1:output:0Btext_vectorization_14/StringSplit/strided_slice_1/stack_2:output:0*
Index0*
T0	*
_output_shapes
: *
shrink_axis_mask�
Xtext_vectorization_14/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/CastCast8text_vectorization_14/StringSplit/strided_slice:output:0*

DstT0*

SrcT0	*#
_output_shapes
:����������
Ztext_vectorization_14/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cast_1Cast:text_vectorization_14/StringSplit/strided_slice_1:output:0*

DstT0*

SrcT0	*
_output_shapes
: �
btext_vectorization_14/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/ShapeShape\text_vectorization_14/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cast:y:0*
T0*
_output_shapes
:�
btext_vectorization_14/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/ConstConst*
_output_shapes
:*
dtype0*
valueB: �
atext_vectorization_14/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/ProdProdktext_vectorization_14/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Shape:output:0ktext_vectorization_14/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Const:output:0*
T0*
_output_shapes
: �
ftext_vectorization_14/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Greater/yConst*
_output_shapes
: *
dtype0*
value	B : �
dtext_vectorization_14/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/GreaterGreaterjtext_vectorization_14/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Prod:output:0otext_vectorization_14/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Greater/y:output:0*
T0*
_output_shapes
: �
atext_vectorization_14/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/CastCasthtext_vectorization_14/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Greater:z:0*

DstT0*

SrcT0
*
_output_shapes
: �
dtext_vectorization_14/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Const_1Const*
_output_shapes
:*
dtype0*
valueB: �
`text_vectorization_14/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/MaxMax\text_vectorization_14/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cast:y:0mtext_vectorization_14/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Const_1:output:0*
T0*
_output_shapes
: �
btext_vectorization_14/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/add/yConst*
_output_shapes
: *
dtype0*
value	B :�
`text_vectorization_14/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/addAddV2itext_vectorization_14/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Max:output:0ktext_vectorization_14/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/add/y:output:0*
T0*
_output_shapes
: �
`text_vectorization_14/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/mulMuletext_vectorization_14/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Cast:y:0dtext_vectorization_14/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/add:z:0*
T0*
_output_shapes
: �
dtext_vectorization_14/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/MaximumMaximum^text_vectorization_14/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cast_1:y:0dtext_vectorization_14/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/mul:z:0*
T0*
_output_shapes
: �
dtext_vectorization_14/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/MinimumMinimum^text_vectorization_14/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cast_1:y:0htext_vectorization_14/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Maximum:z:0*
T0*
_output_shapes
: �
dtext_vectorization_14/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Const_2Const*
_output_shapes
: *
dtype0	*
valueB	 �
etext_vectorization_14/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/BincountBincount\text_vectorization_14/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cast:y:0htext_vectorization_14/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Minimum:z:0mtext_vectorization_14/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Const_2:output:0*
T0	*#
_output_shapes
:����������
_text_vectorization_14/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cumsum/axisConst*
_output_shapes
: *
dtype0*
value	B : �
Ztext_vectorization_14/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/CumsumCumsumltext_vectorization_14/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Bincount:bins:0htext_vectorization_14/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cumsum/axis:output:0*
T0	*#
_output_shapes
:����������
ctext_vectorization_14/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/concat/values_0Const*
_output_shapes
:*
dtype0	*
valueB	R �
_text_vectorization_14/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/concat/axisConst*
_output_shapes
: *
dtype0*
value	B : �
Ztext_vectorization_14/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/concatConcatV2ltext_vectorization_14/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/concat/values_0:output:0`text_vectorization_14/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cumsum:out:0htext_vectorization_14/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/concat/axis:output:0*
N*
T0	*#
_output_shapes
:����������
Jtext_vectorization_14/string_lookup_42/hash_table_Lookup/LookupTableFindV2LookupTableFindV2Wtext_vectorization_14_string_lookup_42_hash_table_lookup_lookuptablefindv2_table_handle8text_vectorization_14/StringSplit/StringSplitV2:values:0Xtext_vectorization_14_string_lookup_42_hash_table_lookup_lookuptablefindv2_default_value*	
Tin0*

Tout0	*#
_output_shapes
:����������
,text_vectorization_14/string_lookup_42/EqualEqual8text_vectorization_14/StringSplit/StringSplitV2:values:0.text_vectorization_14_string_lookup_42_equal_y*
T0*#
_output_shapes
:����������
/text_vectorization_14/string_lookup_42/SelectV2SelectV20text_vectorization_14/string_lookup_42/Equal:z:01text_vectorization_14_string_lookup_42_selectv2_tStext_vectorization_14/string_lookup_42/hash_table_Lookup/LookupTableFindV2:values:0*
T0	*#
_output_shapes
:����������
/text_vectorization_14/string_lookup_42/IdentityIdentity8text_vectorization_14/string_lookup_42/SelectV2:output:0*
T0	*#
_output_shapes
:���������t
2text_vectorization_14/RaggedToTensor/default_valueConst*
_output_shapes
: *
dtype0	*
value	B	 R �
*text_vectorization_14/RaggedToTensor/ConstConst*
_output_shapes
:*
dtype0	*%
valueB	"���������       �
9text_vectorization_14/RaggedToTensor/RaggedTensorToTensorRaggedTensorToTensor3text_vectorization_14/RaggedToTensor/Const:output:08text_vectorization_14/string_lookup_42/Identity:output:0;text_vectorization_14/RaggedToTensor/default_value:output:0:text_vectorization_14/StringSplit/strided_slice_1:output:08text_vectorization_14/StringSplit/strided_slice:output:0*
T0	*
Tindex0	*
Tshape0	*(
_output_shapes
:����������*
num_row_partition_tensors*7
row_partition_types 
FIRST_DIM_SIZEVALUE_ROWIDS�
#embedding_6/StatefulPartitionedCallStatefulPartitionedCallBtext_vectorization_14/RaggedToTensor/RaggedTensorToTensor:result:0embedding_6_1740031*
Tin
2	*
Tout
2*
_collective_manager_ids
 *,
_output_shapes
:����������d*#
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *Q
fLRJ
H__inference_embedding_6_layer_call_and_return_conditional_losses_1739831�
 conv1d_1/StatefulPartitionedCallStatefulPartitionedCall,embedding_6/StatefulPartitionedCall:output:0conv1d_1_1740034conv1d_1_1740036*
Tin
2*
Tout
2*
_collective_manager_ids
 *-
_output_shapes
:�����������*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *N
fIRG
E__inference_conv1d_1_layer_call_and_return_conditional_losses_1739851�
&global_max_pooling1d_1/PartitionedCallPartitionedCall)conv1d_1/StatefulPartitionedCall:output:0*
Tin
2*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:����������* 
_read_only_resource_inputs
 *0
config_proto 

CPU

GPU2*0J 8� *\
fWRU
S__inference_global_max_pooling1d_1_layer_call_and_return_conditional_losses_1739764�
"dropout_15/StatefulPartitionedCallStatefulPartitionedCall/global_max_pooling1d_1/PartitionedCall:output:0*
Tin
2*
Tout
2*
_collective_manager_ids
 *(
_output_shapes
:����������* 
_read_only_resource_inputs
 *0
config_proto 

CPU

GPU2*0J 8� *P
fKRI
G__inference_dropout_15_layer_call_and_return_conditional_losses_1739934�
 dense_36/StatefulPartitionedCallStatefulPartitionedCall+dropout_15/StatefulPartitionedCall:output:0dense_36_1740041dense_36_1740043*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������
*$
_read_only_resource_inputs
*0
config_proto 

CPU

GPU2*0J 8� *N
fIRG
E__inference_dense_36_layer_call_and_return_conditional_losses_1739876x
IdentityIdentity)dense_36/StatefulPartitionedCall:output:0^NoOp*
T0*'
_output_shapes
:���������
�
NoOpNoOp!^conv1d_1/StatefulPartitionedCall!^dense_36/StatefulPartitionedCall#^dropout_15/StatefulPartitionedCall$^embedding_6/StatefulPartitionedCallK^text_vectorization_14/string_lookup_42/hash_table_Lookup/LookupTableFindV2*"
_acd_function_control_output(*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime*8
_input_shapes'
%:���������: : : : : : : : : 2D
 conv1d_1/StatefulPartitionedCall conv1d_1/StatefulPartitionedCall2D
 dense_36/StatefulPartitionedCall dense_36/StatefulPartitionedCall2H
"dropout_15/StatefulPartitionedCall"dropout_15/StatefulPartitionedCall2J
#embedding_6/StatefulPartitionedCall#embedding_6/StatefulPartitionedCall2�
Jtext_vectorization_14/string_lookup_42/hash_table_Lookup/LookupTableFindV2Jtext_vectorization_14/string_lookup_42/hash_table_Lookup/LookupTableFindV2:O K
'
_output_shapes
:���������
 
_user_specified_nameinputs:

_output_shapes
: :

_output_shapes
: :

_output_shapes
: "�L
saver_filename:0StatefulPartitionedCall_2:0StatefulPartitionedCall_38"
saved_model_main_op

NoOp*>
__saved_model_init_op%#
__saved_model_init_op

NoOp*�
serving_default�
=
input_221
serving_default_input_22:0���������>
dense_362
StatefulPartitionedCall_1:0���������
tensorflow/serving/predict:�u
�
layer-0
layer_with_weights-0
layer-1
layer_with_weights-1
layer-2
layer-3
layer-4
layer_with_weights-2
layer-5
	optimizer
	variables
	trainable_variables

regularization_losses
	keras_api
__call__
*&call_and_return_all_conditional_losses
_default_save_signature

signatures"
_tf_keras_sequential
;
_lookup_layer
	keras_api"
_tf_keras_layer
�

embeddings
	variables
trainable_variables
regularization_losses
	keras_api
__call__
*&call_and_return_all_conditional_losses"
_tf_keras_layer
�

kernel
bias
	variables
trainable_variables
regularization_losses
	keras_api
__call__
* &call_and_return_all_conditional_losses"
_tf_keras_layer
�
!	variables
"trainable_variables
#regularization_losses
$	keras_api
%__call__
*&&call_and_return_all_conditional_losses"
_tf_keras_layer
�
'	variables
(trainable_variables
)regularization_losses
*	keras_api
+_random_generator
,__call__
*-&call_and_return_all_conditional_losses"
_tf_keras_layer
�

.kernel
/bias
0	variables
1trainable_variables
2regularization_losses
3	keras_api
4__call__
*5&call_and_return_all_conditional_losses"
_tf_keras_layer
�
6iter

7beta_1

8beta_2
	9decay
:learning_ratemlmmmn.mo/mpvqvrvs.vt/vu"
	optimizer
C
0
1
2
.3
/4"
trackable_list_wrapper
C
0
1
2
.3
/4"
trackable_list_wrapper
 "
trackable_list_wrapper
�
;non_trainable_variables

<layers
=metrics
>layer_regularization_losses
?layer_metrics
	variables
	trainable_variables

regularization_losses
__call__
_default_save_signature
*&call_and_return_all_conditional_losses
&"call_and_return_conditional_losses"
_generic_user_object
�2�
/__inference_sequential_21_layer_call_fn_1739904
/__inference_sequential_21_layer_call_fn_1740254
/__inference_sequential_21_layer_call_fn_1740277
/__inference_sequential_21_layer_call_fn_1740091�
���
FullArgSpec1
args)�&
jself
jinputs

jtraining
jmask
varargs
 
varkw
 
defaults�
p 

 

kwonlyargs� 
kwonlydefaults� 
annotations� *
 
�2�
J__inference_sequential_21_layer_call_and_return_conditional_losses_1740356
J__inference_sequential_21_layer_call_and_return_conditional_losses_1740442
J__inference_sequential_21_layer_call_and_return_conditional_losses_1740158
J__inference_sequential_21_layer_call_and_return_conditional_losses_1740225�
���
FullArgSpec1
args)�&
jself
jinputs

jtraining
jmask
varargs
 
varkw
 
defaults�
p 

 

kwonlyargs� 
kwonlydefaults� 
annotations� *
 
�B�
"__inference__wrapped_model_1739754input_22"�
���
FullArgSpec
args� 
varargsjargs
varkwjkwargs
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
,
@serving_default"
signature_map
P
Ainput_vocabulary
Blookup_table
C	keras_api"
_tf_keras_layer
"
_generic_user_object
):'	�Nd2embedding_6/embeddings
'
0"
trackable_list_wrapper
'
0"
trackable_list_wrapper
 "
trackable_list_wrapper
�
Dnon_trainable_variables

Elayers
Fmetrics
Glayer_regularization_losses
Hlayer_metrics
	variables
trainable_variables
regularization_losses
__call__
*&call_and_return_all_conditional_losses
&"call_and_return_conditional_losses"
_generic_user_object
�2�
-__inference_embedding_6_layer_call_fn_1740474�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2�
H__inference_embedding_6_layer_call_and_return_conditional_losses_1740483�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
&:$d�2conv1d_1/kernel
:�2conv1d_1/bias
.
0
1"
trackable_list_wrapper
.
0
1"
trackable_list_wrapper
 "
trackable_list_wrapper
�
Inon_trainable_variables

Jlayers
Kmetrics
Llayer_regularization_losses
Mlayer_metrics
	variables
trainable_variables
regularization_losses
__call__
* &call_and_return_all_conditional_losses
& "call_and_return_conditional_losses"
_generic_user_object
�2�
*__inference_conv1d_1_layer_call_fn_1740492�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2�
E__inference_conv1d_1_layer_call_and_return_conditional_losses_1740508�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
�
Nnon_trainable_variables

Olayers
Pmetrics
Qlayer_regularization_losses
Rlayer_metrics
!	variables
"trainable_variables
#regularization_losses
%__call__
*&&call_and_return_all_conditional_losses
&&"call_and_return_conditional_losses"
_generic_user_object
�2�
8__inference_global_max_pooling1d_1_layer_call_fn_1740513�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2�
S__inference_global_max_pooling1d_1_layer_call_and_return_conditional_losses_1740519�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
�
Snon_trainable_variables

Tlayers
Umetrics
Vlayer_regularization_losses
Wlayer_metrics
'	variables
(trainable_variables
)regularization_losses
,__call__
*-&call_and_return_all_conditional_losses
&-"call_and_return_conditional_losses"
_generic_user_object
"
_generic_user_object
�2�
,__inference_dropout_15_layer_call_fn_1740524
,__inference_dropout_15_layer_call_fn_1740529�
���
FullArgSpec)
args!�
jself
jinputs

jtraining
varargs
 
varkw
 
defaults�
p 

kwonlyargs� 
kwonlydefaults� 
annotations� *
 
�2�
G__inference_dropout_15_layer_call_and_return_conditional_losses_1740534
G__inference_dropout_15_layer_call_and_return_conditional_losses_1740546�
���
FullArgSpec)
args!�
jself
jinputs

jtraining
varargs
 
varkw
 
defaults�
p 

kwonlyargs� 
kwonlydefaults� 
annotations� *
 
": 	�
2dense_36/kernel
:
2dense_36/bias
.
.0
/1"
trackable_list_wrapper
.
.0
/1"
trackable_list_wrapper
 "
trackable_list_wrapper
�
Xnon_trainable_variables

Ylayers
Zmetrics
[layer_regularization_losses
\layer_metrics
0	variables
1trainable_variables
2regularization_losses
4__call__
*5&call_and_return_all_conditional_losses
&5"call_and_return_conditional_losses"
_generic_user_object
�2�
*__inference_dense_36_layer_call_fn_1740555�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2�
E__inference_dense_36_layer_call_and_return_conditional_losses_1740566�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
:	 (2	Adam/iter
: (2Adam/beta_1
: (2Adam/beta_2
: (2
Adam/decay
: (2Adam/learning_rate
 "
trackable_list_wrapper
J
0
1
2
3
4
5"
trackable_list_wrapper
.
]0
^1"
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_dict_wrapper
�B�
%__inference_signature_wrapper_1740467input_22"�
���
FullArgSpec
args� 
varargs
 
varkwjkwargs
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
 "
trackable_list_wrapper
j
__initializer
`_create_resource
a_initialize
b_destroy_resourceR jCustom.StaticHashTable
"
_generic_user_object
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_dict_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_dict_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_dict_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_dict_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_dict_wrapper
N
	ctotal
	dcount
e	variables
f	keras_api"
_tf_keras_metric
^
	gtotal
	hcount
i
_fn_kwargs
j	variables
k	keras_api"
_tf_keras_metric
"
_generic_user_object
�2�
__inference__creator_1740571�
���
FullArgSpec
args� 
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *� 
�2�
 __inference__initializer_1740579�
���
FullArgSpec
args� 
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *� 
�2�
__inference__destroyer_1740584�
���
FullArgSpec
args� 
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *� 
:  (2total
:  (2count
.
c0
d1"
trackable_list_wrapper
-
e	variables"
_generic_user_object
:  (2total
:  (2count
 "
trackable_dict_wrapper
.
g0
h1"
trackable_list_wrapper
-
j	variables"
_generic_user_object
.:,	�Nd2Adam/embedding_6/embeddings/m
+:)d�2Adam/conv1d_1/kernel/m
!:�2Adam/conv1d_1/bias/m
':%	�
2Adam/dense_36/kernel/m
 :
2Adam/dense_36/bias/m
.:,	�Nd2Adam/embedding_6/embeddings/v
+:)d�2Adam/conv1d_1/kernel/v
!:�2Adam/conv1d_1/bias/v
':%	�
2Adam/dense_36/kernel/v
 :
2Adam/dense_36/bias/v
	J
Const
J	
Const_1
J	
Const_2
J	
Const_3
J	
Const_48
__inference__creator_1740571�

� 
� "� :
__inference__destroyer_1740584�

� 
� "� A
 __inference__initializer_1740579Byz�

� 
� "� �
"__inference__wrapped_model_1739754s	Bvwx./1�.
'�$
"�
input_22���������
� "3�0
.
dense_36"�
dense_36���������
�
E__inference_conv1d_1_layer_call_and_return_conditional_losses_1740508g4�1
*�'
%�"
inputs����������d
� "+�(
!�
0�����������
� �
*__inference_conv1d_1_layer_call_fn_1740492Z4�1
*�'
%�"
inputs����������d
� "�������������
E__inference_dense_36_layer_call_and_return_conditional_losses_1740566]./0�-
&�#
!�
inputs����������
� "%�"
�
0���������

� ~
*__inference_dense_36_layer_call_fn_1740555P./0�-
&�#
!�
inputs����������
� "����������
�
G__inference_dropout_15_layer_call_and_return_conditional_losses_1740534^4�1
*�'
!�
inputs����������
p 
� "&�#
�
0����������
� �
G__inference_dropout_15_layer_call_and_return_conditional_losses_1740546^4�1
*�'
!�
inputs����������
p
� "&�#
�
0����������
� �
,__inference_dropout_15_layer_call_fn_1740524Q4�1
*�'
!�
inputs����������
p 
� "������������
,__inference_dropout_15_layer_call_fn_1740529Q4�1
*�'
!�
inputs����������
p
� "������������
H__inference_embedding_6_layer_call_and_return_conditional_losses_1740483a0�-
&�#
!�
inputs����������	
� "*�'
 �
0����������d
� �
-__inference_embedding_6_layer_call_fn_1740474T0�-
&�#
!�
inputs����������	
� "�����������d�
S__inference_global_max_pooling1d_1_layer_call_and_return_conditional_losses_1740519wE�B
;�8
6�3
inputs'���������������������������
� ".�+
$�!
0������������������
� �
8__inference_global_max_pooling1d_1_layer_call_fn_1740513jE�B
;�8
6�3
inputs'���������������������������
� "!��������������������
J__inference_sequential_21_layer_call_and_return_conditional_losses_1740158m	Bvwx./9�6
/�,
"�
input_22���������
p 

 
� "%�"
�
0���������

� �
J__inference_sequential_21_layer_call_and_return_conditional_losses_1740225m	Bvwx./9�6
/�,
"�
input_22���������
p

 
� "%�"
�
0���������

� �
J__inference_sequential_21_layer_call_and_return_conditional_losses_1740356k	Bvwx./7�4
-�*
 �
inputs���������
p 

 
� "%�"
�
0���������

� �
J__inference_sequential_21_layer_call_and_return_conditional_losses_1740442k	Bvwx./7�4
-�*
 �
inputs���������
p

 
� "%�"
�
0���������

� �
/__inference_sequential_21_layer_call_fn_1739904`	Bvwx./9�6
/�,
"�
input_22���������
p 

 
� "����������
�
/__inference_sequential_21_layer_call_fn_1740091`	Bvwx./9�6
/�,
"�
input_22���������
p

 
� "����������
�
/__inference_sequential_21_layer_call_fn_1740254^	Bvwx./7�4
-�*
 �
inputs���������
p 

 
� "����������
�
/__inference_sequential_21_layer_call_fn_1740277^	Bvwx./7�4
-�*
 �
inputs���������
p

 
� "����������
�
%__inference_signature_wrapper_1740467	Bvwx./=�:
� 
3�0
.
input_22"�
input_22���������"3�0
.
dense_36"�
dense_36���������
